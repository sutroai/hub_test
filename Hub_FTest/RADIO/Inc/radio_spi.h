/**
******************************************************************************
* @file    radio_spi.h
* @author  Central Labs
* @version V1.0.1
* @date    10-Mar-2016
* @brief   This file contains all the functions prototypes for SPI .
******************************************************************************
* @attention
*
* <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of STMicroelectronics nor the names of its contributors
*      may be used to endorse or promote products derived from this software
*      without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/


/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RADIO_SPI_H
#define __RADIO_SPI_H
#ifdef __cplusplus
extern "C" {
#endif
  
/* Includes ------------------------------------------------------------------*/
#include "cube_hal.h"
#include "radio_gpio.h"

  
/**
 * @addtogroup BSP
 * @{
 */ 
  
/* Exported types ------------------------------------------------------------*/
   
/* Exported constants --------------------------------------------------------*/
  
  
/* Exported macro ------------------------------------------------------------*/
  /* Define for SPIRIT1 board  */  
// #if !defined (USE_SPIRIT1_DEFAULT)
// #define USE_SPIRIT1_DEFAULT
//#endif
  
/* SPIRIT1_Spi_config */ 
/* SPI1 */  
  
#define RADIO_SPI                                 SPI1
#define RADIO_SPI_CLK_ENABLE()                  __SPI1_CLK_ENABLE()
#define RADIO_SPI_CLK_DISABLE()                 __SPI1_CLK_DISABLE()

/* Defines for MISO pin*/
#define RADIO_SPI_MISO_PORT                      GPIOA
#define RADIO_SPI_MISO_PIN                       GPIO_PIN_6 
#if defined(USE_STM32F4XX_NUCLEO)||(USE_STM32L1XX_NUCLEO)
#define RADIO_SPI_MISO_AF                       GPIO_AF5_SPI1
#endif
#if defined(USE_STM32L0XX_NUCLEO)
#define RADIO_SPI_MISO_AF                       GPIO_AF0_SPI1
#endif
#define RADIO_SPI_MISO_CLK_ENABLE()            __GPIOA_CLK_ENABLE()
#define RADIO_SPI_MISO_CLK_DISABLE()           __GPIOA_CLK_DISABLE() 
  
/* Defines for MOSI pin*/  
#define RADIO_SPI_MOSI_PORT                      GPIOA
#define RADIO_SPI_MOSI_PIN                       GPIO_PIN_7 
#if defined(USE_STM32F4XX_NUCLEO)||(USE_STM32L1XX_NUCLEO)
#define RADIO_SPI_MOSI_AF                       GPIO_AF5_SPI1
#endif
#if defined(USE_STM32L0XX_NUCLEO)
#define RADIO_SPI_MOSI_AF                       GPIO_AF0_SPI1
#endif

#define RADIO_SPI_MOSI_CLK_ENABLE()            __GPIOA_CLK_ENABLE()
#define RADIO_SPI_MOSI_CLK_DISABLE()           __GPIOA_CLK_DISABLE()   
  

/* Defines for SCLK pin */  
#define RADIO_SPI_SCK_PORT                      GPIOA
#define RADIO_SPI_SCK_PIN                       GPIO_PIN_5
  #if defined(USE_STM32F4XX_NUCLEO)||(USE_STM32L1XX_NUCLEO)
#define RADIO_SPI_SCK_AF                       GPIO_AF5_SPI1
#endif
#if defined(USE_STM32L0XX_NUCLEO)
#define RADIO_SPI_SCK_AF                       GPIO_AF0_SPI1
#endif
#define RADIO_SPI_SCLK_CLK_ENABLE()            __GPIOA_CLK_ENABLE()
#define RADIO_SPI_SCLK_CLK_DISABLE()           __GPIOA_CLK_DISABLE()

 /* Defines for chip select pin */  
#define RADIO_SPI_CS_PORT                        GPIOA
#define RADIO_SPI_CS_PIN                         GPIO_PIN_4
#define RADIO_SPI_CS_CLK_ENABLE()            __GPIOA_CLK_ENABLE()
#define RADIO_SPI_CS_CLK_DISABLE()           __GPIOA_CLK_DISABLE()
  
#define RadioSpiCSLow()        HAL_GPIO_WritePin(RADIO_SPI_CS_PORT, RADIO_SPI_CS_PIN, GPIO_PIN_RESET)
#define RadioSpiCSHigh()       HAL_GPIO_WritePin(RADIO_SPI_CS_PORT, RADIO_SPI_CS_PIN, GPIO_PIN_SET)

  
#define SPI_ENTER_CRITICAL()          RadioGpioInterruptCmd(RADIO_GPIO_IRQ,0x04,0x04,DISABLE);//kg
#define SPI_EXIT_CRITICAL()           RadioGpioInterruptCmd(RADIO_GPIO_IRQ,0x04,0x04,ENABLE); 

#ifdef __cplusplus
}
#endif
#endif /*__RADIO_SPI_H */

/**
* @}
*/

/**
* @}
*/

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
