#include "api.h"

static void UART_Send_CMD(uint8_t *at)
{
	BSP_UART2_FlushFIFO();
	BSP_UART2_SendStr(at,strlen((char *)at));
	BSP_UART2_SendStr((uint8_t *)"\r\n",2);
}

static void UART_Send_Data(uint8_t *data,uint16_t len)
{
	BSP_UART2_FlushFIFO();
	BSP_UART2_SendStr(data,len);
}

static void UART_Recv_Data(uint8_t *data,uint16_t *len,uint16_t time)
{	
	*len = 0;
	BSP_Timer_Start(SYS_TIMER1,time);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(G_uart2_num)
		{
			*data = BSP_UART2_RecvChar();
			data++;
			(*len)++;
			BSP_Timer_Start(SYS_TIMER1,QUARTER_SECOND);
		}
	}
}

static void UART_Recv_Data_AWS(uint8_t *data,uint16_t reqlen,uint16_t *len,uint16_t time)
{	
	*len = 0;
	BSP_Timer_Start(SYS_TIMER1,1000);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(G_uart2_num)
		{
			*data = BSP_UART2_RecvChar();
			data++;
			(*len)++;
			BSP_Timer_Start(SYS_TIMER1,100);
			if((*len) >= reqlen)	break;
		}
	}
}

void R410M_Module_PWRON(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin   = GPIO_PIN_2;
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);
	HAL_Delay(5000);
}

void R410M_Module_PWROFF(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin   = GPIO_PIN_2;
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_RESET);
	HAL_Delay(5000);
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_2, GPIO_PIN_SET);
	HAL_Delay(500);
}

int R410M_Module_Reset(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CFUN=1");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int R410M_Module_Init(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	BSP_UART2_Init();
	R410M_Module_PWRON();
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"ATE0");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int R410M_GET_CGMM(uint8_t *mid)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CGMM");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	else
	{
		pres = strtok((char *)rbuf,"\n");	
		pres = strtok((char *)'\0',"\n");
		strcpy((char *)mid,pres);
		return 0;
	}
}

int R410M_Get_CPIN(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CPIN?");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"READY"))
	{
		return -1;
	}
	return 0;
}

int R410M_Get_CCID(uint8_t *ccid)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CCID");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	else	
	{
		pres = strtok((char *)rbuf," ");
		pres = strtok((char *)'\0',"\r");
		strcpy((char *)ccid,pres);
		return 0;
	}
}

int R410M_Get_CGATT(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CGATT?");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"1"))
	{
		return -1;
	}
	return 0;
}

int R410M_Get_CEREG(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CEREG?");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"0,1"))
	{
		return -1;
	}
	return 0;
}

int R410M_Get_CSQ(uint8_t *csq)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CSQ");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	else	
	{
		pres = strtok((char *)rbuf," ");
		pres = strtok((char *)'\0',",");
		strcpy((char *)csq,pres);
		return 0;
	}
}

int R410M_Get_COPS(uint16_t *mode,uint8_t *oper,uint16_t *act)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+COPS?");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	else
	{
		pres = strtok((char *)rbuf," ");
		pres = strtok((char *)'\0',",");
		*mode  = atoi(pres);
		pres = strtok((char *)'\0',",");
		pres = strtok((char *)'\0',",");
		strcpy((char *)oper,pres);
		pres = strtok((char *)'\0',"\n");
		*act = atoi(pres);
		return 0;
	}
}

int R410M_Create_Socket(uint16_t *soc)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+USOCR=6");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	else
	{
		pres = strtok((char *)rbuf," ");
		pres = strtok((char *)'\0',"\n");
		*soc = atoi(pres);
		return 0;
	}
}

int R410M_Close_Socket(uint16_t soc)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+USOCL=0");
	UART_Recv_Data(rbuf,&rlen,TEN_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int R410M_Connect_Socket(uint16_t soc,uint8_t *ip,uint16_t port)
{
	uint8_t sbuf[100];
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(sbuf,0x00,sizeof(sbuf));
	memset(rbuf,0x00,sizeof(rbuf));
	sprintf((char *)sbuf,"AT+USOCO=%d,\"%s\",%d",soc,ip,port);
	UART_Send_CMD(sbuf);
	UART_Recv_Data(rbuf,&rlen,TEN_SECOND);
	printf("%s \n",rbuf);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int R410M_Set_USODL(uint16_t soc)
{
	uint8_t sbuf[100];
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(sbuf,0x00,sizeof(sbuf));
	memset(rbuf,0x00,sizeof(rbuf));
	sprintf((char *)sbuf,"AT+USODL=%d",soc);
	UART_Send_CMD(sbuf);
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"CONNECT"))
	{
		return -1;
	}
	return 0;
}

int R410M_Exit_USODL(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_Data((uint8_t *)"+++",3);
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"DISCONNECT"))
	{
		return -1;
	}
	return 0;
}

int R410_DL_Send(uint8_t *data,uint16_t len)
{
	UART_Send_Data(data,len);
	return 0;
}

int R410_DL_Recv(uint8_t *data,uint16_t reqlen,uint16_t *len,uint16_t time)
{
	UART_Recv_Data_AWS(data,reqlen,len,time);
//	if(*len)		return 0;
//	else			return -1;
	return 0;
}

