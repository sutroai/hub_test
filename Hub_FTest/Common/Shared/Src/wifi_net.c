/**
  ******************************************************************************
  * @file    wifi_net.c
  * @author  MCD Application Team
  * @brief   Wifi-specific NET initialization.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#ifdef USE_WIFI

/* Includes ------------------------------------------------------------------*/
#include "api.h"
#include "main.h"
#include "wifi.h"
#include "esp8266.h"
#include "func.h"
#include "iot_flash_config.h"

/* Private defines -----------------------------------------------------------*/
#define  WIFI_CONNECT_MAX_ATTEMPT_COUNT  3

#define WIFI_PRODUCT_INFO_SIZE                      32
#define WIFI_PAYLOAD_SIZE                           1200

/* Private typedef -----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
int net_if_init(void * if_ctxt);
int net_if_deinit(void * if_ctxt);
int net_if_reinit(void * if_ctxt);

/* Functions Definition ------------------------------------------------------*/
int net_if_init(void * if_ctxt)
{
    const char *ssid = (char *)g_wifiSSID;
    const char  *psk = (char *)g_wifiPSK;
    WIFI_Ecn_t security_mode;
    WIFI_Status_t wifiRes;
    int wifiConnectCounter = 0;

    do 
    {
		IWDG_Feed();
        printf("Connecting to AP: %s  Attempt %d/%d...\n",ssid, ++wifiConnectCounter,WIFI_CONNECT_MAX_ATTEMPT_COUNT);
        wifiRes = WIFI_Connect(ssid, psk, security_mode);
        if (wifiRes == WIFI_STATUS_OK) break;
    } 
    while (wifiConnectCounter < WIFI_CONNECT_MAX_ATTEMPT_COUNT);

    HAL_Delay(2000);   
    if (wifiRes == WIFI_STATUS_OK)
    {
        printf("Connected to AP %s\n\n",ssid);
        ESP8266_TCP_MuxSend(0,"{\"wifi\":true}",13);
    }
    else
    {
        printf("Failed to connect to AP %s,need to config network.\n\n",ssid);
        ESP8266_TCP_MuxSend(0,"{\"wifi\":false}",14);
        ESP8266_Set_CIPSERVER(ESP_SERVER_OFF);
		return -1;
    }

    ESP8266_Set_CIPSERVER(ESP_SERVER_OFF);
    ESP8266_Set_CIPMUX(ESP_LINK_SINGlE);
    ESP8266_Set_CWMODE(ESP_MODE_STA);

    ESP8266_Query_AP(g_wifiRSSI);

    return 0;
}


int net_if_deinit(void * if_ctxt)
{
	return 0;
}


int net_if_reinit(void * if_ctxt)
{
  int ret = 0;
  const char *ssid;
  const char  *psk;
  WIFI_Ecn_t security_mode;
  WIFI_Status_t wifiRes;
  int wifiConnectCounter = 0;
  
  // wifiRes = WIFI_Disconnect(); // Disconnect() is not enough for the Inventek module which does not clean the previous state properly.
  wifiRes = WIFI_Init();
  if (wifiRes != WIFI_STATUS_OK)
  {
    msg_error("WIFI_Init() failed.\n");
  }
  
  if (checkWiFiCredentials(&ssid, &psk, (uint8_t *) &security_mode) != HAL_OK)
  {
    ret = -1;
  }
  else
  {
    do 
    {
      printf("\rConnecting to AP: %s  Attempt %d/%d ...",ssid, ++wifiConnectCounter,WIFI_CONNECT_MAX_ATTEMPT_COUNT);
      wifiRes = WIFI_Connect(ssid, psk, security_mode);
      if (wifiRes == WIFI_STATUS_OK) break;
    } 
    while (wifiConnectCounter < WIFI_CONNECT_MAX_ATTEMPT_COUNT);
    
    if (wifiRes == WIFI_STATUS_OK)
    {
      printf("\nRe-connected to AP %s\n",ssid);
    }
    else
    {
      printf("\nFailed to re-connect to AP %s\n",ssid);
      ret = -1;
    }
  }
    
  return ret;
}

#endif /* USE_WIFI */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
