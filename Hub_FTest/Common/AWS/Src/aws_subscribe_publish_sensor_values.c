/**
  ******************************************************************************
  * @file    subscribe_publish_sensor_values.c
  * @author  MCD Application Team
  * @brief   Control of the measurement sampling and MQTT reporting loop.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "iot_flash_config.h"
#include "sensors_data.h"
#include "msg.h"
#include "api.h"

uint8_t cmd_buf[60];
uint16_t cmd_len;
bool cmd_send = false;

void MQTTcallbackHandler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen, IoT_Publish_Message_Params *params, void *pData);
int subscribe_publish_sensor_values(void);

/* Private defines ------------------------------------------------------------*/
#define MQTT_CONNECT_MAX_ATTEMPT_COUNT 3
#define TIMER_COUNT_FOR_SENSOR_PUBLISH 10

/* Private variables ---------------------------------------------------------*/
static char cPTopicName[MAX_SHADOW_TOPIC_LENGTH_BYTES] = "";
static char cSTopicName[MAX_SHADOW_TOPIC_LENGTH_BYTES] = "";
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

static void UART_Recv_Data(uint8_t *data,uint16_t *len,uint16_t time)
{	
	*len = 0;
	
	BSP_Timer_Start(SYS_TIMER1,time);										// 第一个字符的等待时间
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(G_uart1_num)
		{
			*data = BSP_UART1_RecvChar();
			data++;
			(*len)++;
			BSP_Timer_Start(SYS_TIMER1,QUARTER_SECOND);						// 后续字符的等待时间
		}
	}
}

bool app_needs_device_keypair()
{
  return true;
}

/**
* @brief MQTT disconnect callback hander
*
* @param pClient: pointer to the AWS client structure
* @param data: 
* @return no return
*/
static void disconnectCallbackHandler(AWS_IoT_Client *pClient, void *data)
{
  msg_warning("MQTT Disconnect\n");
  IoT_Error_t rc = FAILURE;
  
  if(NULL == data)
  {
    return;
  }

  AWS_IoT_Client *client = (AWS_IoT_Client *)data;

  if(aws_iot_is_autoreconnect_enabled(client))
  {
    msg_info("Auto Reconnect is enabled, Reconnecting attempt will start now\n");
  }
  else
  {
    msg_warning("Auto Reconnect not enabled. Starting manual reconnect...\n");
    rc = aws_iot_mqtt_attempt_reconnect(client);

    if(NETWORK_RECONNECTED == rc)
    {
      msg_warning("Manual Reconnect Successful\n");
    }
    else
    {
      msg_warning("Manual Reconnect Failed - %d\n", rc);
    }
  }
}

/* Exported functions --------------------------------------------------------*/

/**
* @brief MQTT subscriber callback hander
*
* called when data is received from AWS IoT Thing (message broker)
* @param MQTTCallbackParams type parameter
* @return no return
*/
void MQTTcallbackHandler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen, IoT_Publish_Message_Params *params, void *pData)
{
	msg_info("\nMQTT subscribe callback......\n");
	msg_info("%.*s\n", (int)params->payloadLen, (char *)params->payload);
}

/**
* @brief main entry function to AWS IoT code
*
* @param no parameter
* @return AWS_SUCCESS: 0 
          FAILURE: -1
*/
int subscribe_publish_sensor_values(void)
{
  bool infinitePublishFlag = true;
	
//  const char *serverAddress = "a1d8rh9ogf1efh.iot.us-east-2.amazonaws.com";
//  const char *pCaCert = "-----BEGIN CERTIFICATE-----\nMIIE0zCCA7ugAwIBAgIQGNrRniZ96LtKIVjNzGs7SjANBgkqhkiG9w0BAQUFADCB\nyjELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL\nExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJp\nU2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxW\nZXJpU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0\naG9yaXR5IC0gRzUwHhcNMDYxMTA4MDAwMDAwWhcNMzYwNzE2MjM1OTU5WjCByjEL\nMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQLExZW\nZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJpU2ln\nbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxWZXJp\nU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9y\naXR5IC0gRzUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvJAgIKXo1\nnmAMqudLO07cfLw8RRy7K+D+KQL5VwijZIUVJ/XxrcgxiV0i6CqqpkKzj/i5Vbex\nt0uz/o9+B1fs70PbZmIVYc9gDaTY3vjgw2IIPVQT60nKWVSFJuUrjxuf6/WhkcIz\nSdhDY2pSS9KP6HBRTdGJaXvHcPaz3BJ023tdS1bTlr8Vd6Gw9KIl8q8ckmcY5fQG\nBO+QueQA5N06tRn/Arr0PO7gi+s3i+z016zy9vA9r911kTMZHRxAy3QkGSGT2RT+\nrCpSx4/VBEnkjWNHiDxpg8v+R70rfk/Fla4OndTRQ8Bnc+MUCH7lP59zuDMKz10/\nNIeWiu5T6CUVAgMBAAGjgbIwga8wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8E\nBAMCAQYwbQYIKwYBBQUHAQwEYTBfoV2gWzBZMFcwVRYJaW1hZ2UvZ2lmMCEwHzAH\nBgUrDgMCGgQUj+XTGoasjY5rw8+AatRIGCx7GS4wJRYjaHR0cDovL2xvZ28udmVy\naXNpZ24uY29tL3ZzbG9nby5naWYwHQYDVR0OBBYEFH/TZafC3ey78DAJ80M5+gKv\nMzEzMA0GCSqGSIb3DQEBBQUAA4IBAQCTJEowX2LP2BqYLz3q3JktvXf2pXkiOOzE\np6B4Eq1iDkVwZMXnl2YtmAl+X6/WzChl8gGqCBpH3vn5fJJaCGkgDdk+bW48DW7Y\n5gaRQBi5+MHt39tBquCWIMnNZBU4gcmU7qKEKQsTb47bDN0lAtukixlE0kF6BWlK\nWE9gyn6CagsCqiUXObXbf+eEZSqVir2G3l6BFoMtEMze/aiCKm0oHw0LxOXnGiYZ\n4fQRbxC1lfznQgUy286dUV4otp6F01vvpX1FQHKOtw5rDgb7MzVIcbidJ4vEZV8N\nhnacRHr2lVz2XTIIM6RUthg/aFzyQkqFOFSDX9HoLPKsEdao7WNq\n-----END CERTIFICATE-----\n";
//  const char *pClientCert = "-----BEGIN CERTIFICATE-----\nMIIDWTCCAkGgAwIBAgIUUDA64BnpEmd7uWeipMILVG0T2BcwDQYJKoZIhvcNAQEL\nBQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g\nSW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTE4MTEyNDEzMjYy\nMFoXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0\nZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOKrSmsudrnIuNKhpPRw\nK4/jQIMHjmpeYARtzotMDH3yZbuu7uJxHdzWWoCSQLm2+XqoFRsVVwxKD77CTbUJ\n9S4tL3iQGsaV2ySLOkpObfSrhdCgZXzZcF/njL6d0kAULfLdGObZOCeg8l14ZWM7\nVfIJkl1FOIEjQVK+h1GhKVPDMdYh/xSAQbodtyg49gFrxLuW8NjkKT+O9RBCmr9g\nJ8RBA0XHxjU48wiM3r3gtWPWICYISVQjivqRVGoOCoK7DdEBm6q+j0qB4+DNSa5M\nN7F1hmypOw2dxz8NxIrQ7mMB+R46eJ7MXaTkA6HpZDEvFgXEVAifhHD2ySqytjYv\nI0MCAwEAAaNgMF4wHwYDVR0jBBgwFoAUsOqO+vyJ6+EAFg060uoW9aajiWQwHQYD\nVR0OBBYEFI8sBPDJ1Ds+gHP9z1Ehm39nU6fsMAwGA1UdEwEB/wQCMAAwDgYDVR0P\nAQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQBd4YyWQ61GpY04lZgZoW5c3FkZ\ny+rJMCOjyXaAOq/aOZEOg86iVISnOTs8Z/ncMewBHBqEcKptrHCRpfrdKKj14yan\n/sFlXmMKGZ25+PzbOqdarSvMYMOT4925oVU7kKDHmtrAgXzozZ7cnektDso9NFrt\nSJz/J9hymFElGScPee7KM9zT2ejOoTzVnx9Yva1TalkICmiJ1HbAXqvPK6DwfwvR\nct4xmHsbc06EH8HLutw7WBoV6LiToqUTWrZESCBblU4FhzmTFnyMYpYVScOTHtLD\n8I1U4VAsM33WukAJR+fkDGjYEfplneJdOr0Hm39puPc+IlkGJ8rnIuKc1ET2\n-----END CERTIFICATE-----\n";
//  const char *pClientPrivateKey = "-----BEGIN RSA PRIVATE KEY-----\nMIIEpAIBAAKCAQEA4qtKay52uci40qGk9HArj+NAgweOal5gBG3Oi0wMffJlu67u\n4nEd3NZagJJAubb5eqgVGxVXDEoPvsJNtQn1Li0veJAaxpXbJIs6Sk5t9KuF0KBl\nfNlwX+eMvp3SQBQt8t0Y5tk4J6DyXXhlYztV8gmSXUU4gSNBUr6HUaEpU8Mx1iH/\nFIBBuh23KDj2AWvEu5bw2OQpP471EEKav2AnxEEDRcfGNTjzCIzeveC1Y9YgJghJ\nVCOK+pFUag4KgrsN0QGbqr6PSoHj4M1Jrkw3sXWGbKk7DZ3HPw3EitDuYwH5Hjp4\nnsxdpOQDoelkMS8WBcRUCJ+EcPbJKrK2Ni8jQwIDAQABAoIBAQC3R8nk4K0fjC3m\nIs4+2HRg5GBFktaVK7p32lIoUFXJgTbBi7GdZ9m9t5V2YMv1Xv5bL43hWGAKj9gr\nZvFucZ7Ot7qqwdrkuEd6RRnc+43tSMPq7cciLK+w3Inxl6tqJOcCecSo5PuDD6Dp\n83AHzPKxwwuWX2JRCTDIt0azupywHJ7pkDcloJE+kknc2sagsRixYm5Hz844PI9Y\nBYq6vXMu9WfcY/S6RdSqFczHEHa72zv8Ae3s3kBchrnxre0WyV63OYnMT8xuPS4N\ntrbkQLYQLNQ6M+01cjwsR10o+8acGZF1TtpALkWBYSOpFBF6lV0rcYcio71Dwu7l\nYgKg5ngBAoGBAP/keDSYqMufhaKg6C57VUOVAk2kvZ5FzQIQGWPZOYimlHJGnw7S\nQQ6aODjJ9usNN5zFayrPIqSoB+S3rail4feIzvYZ9jMKBhCVqTpJEe/jGDjbVbQC\n8Z3VER594NgBGxpCg826vIO0vb5GcbZW9g2CQbennfbOYDgRu4Tq0U/9AoGBAOLD\nrVfT19CK9qXsjJayCAFLFlbxEWmLo03MvIZbSkOxkxp+yvPW78gQccFUi4BilyHf\nsj/+c7IYr6p6OPfKpw0A+DT/CSsaGV6uNak8dOW3AetelOIMr1KyUVFpQwbO4vC4\nAHxxAZoEXlzONnf8C3gfkw/qEwW2/jTEemZ1hoQ/AoGAbeu+lxlcqNuFlC56jjz8\ni2Ne8etqaKqjPdeckH+nb0PfUJd1i/BVcehdbkeTgbTTxswnFOSVhRWJDn64YlGf\nl610+dJ9J7+Oi2E+qILq6ZkrtoDBd+or0BjOmcKI60DOW6Fm7ODiSdFCEJJCFomU\neIkTocim2nYcbvlvC9GyyCECgYEA1Ws0iaRnSW6AFEk9R9bmkdEJhWn86UjyuRYn\nmIvyqSNHP3h1qmhv4+sWJM5dPMeNVeQi7vecC9IQAnJli1C/SA3RjI87IcW+es2a\n01yC6HAE7nBlzxoBJ+OLhQN8gbvU+pkyW+w/haT26oFp4iLk6HlO/3tdy9MeDKue\nRy/1GiECgYBHX2zNGbtjZdk0cdX4Lwp3/nVYIYsCEu0IueBlPJ5Gv0K6yKQfN9VQ\ns/Cf87fawEGjQZ8dBHJ/tBUEWafkGM7r5jMKo2JC7e4j9ff/RJV+Q3vsv77szKrJ\nux3A9i2C8BJqi4ajMQe2tPHPXjiEjmkrK52y6Egdu8cYYxE7GmsFzQ==\n-----END RSA PRIVATE KEY-----\n";
//  const char *pDeviceName = "Node413";
//  char cPayload[AWS_IOT_MQTT_TX_BUF_LEN];
	
  const char *serverAddress = "a3j68fikom78rq.iot.us-west-2.amazonaws.com";
  const char *pCaCert = "-----BEGIN CERTIFICATE-----\nMIIE0zCCA7ugAwIBAgIQGNrRniZ96LtKIVjNzGs7SjANBgkqhkiG9w0BAQUFADCB\nyjELMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQL\nExZWZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJp\nU2lnbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxW\nZXJpU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0\naG9yaXR5IC0gRzUwHhcNMDYxMTA4MDAwMDAwWhcNMzYwNzE2MjM1OTU5WjCByjEL\nMAkGA1UEBhMCVVMxFzAVBgNVBAoTDlZlcmlTaWduLCBJbmMuMR8wHQYDVQQLExZW\nZXJpU2lnbiBUcnVzdCBOZXR3b3JrMTowOAYDVQQLEzEoYykgMjAwNiBWZXJpU2ln\nbiwgSW5jLiAtIEZvciBhdXRob3JpemVkIHVzZSBvbmx5MUUwQwYDVQQDEzxWZXJp\nU2lnbiBDbGFzcyAzIFB1YmxpYyBQcmltYXJ5IENlcnRpZmljYXRpb24gQXV0aG9y\naXR5IC0gRzUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCvJAgIKXo1\nnmAMqudLO07cfLw8RRy7K+D+KQL5VwijZIUVJ/XxrcgxiV0i6CqqpkKzj/i5Vbex\nt0uz/o9+B1fs70PbZmIVYc9gDaTY3vjgw2IIPVQT60nKWVSFJuUrjxuf6/WhkcIz\nSdhDY2pSS9KP6HBRTdGJaXvHcPaz3BJ023tdS1bTlr8Vd6Gw9KIl8q8ckmcY5fQG\nBO+QueQA5N06tRn/Arr0PO7gi+s3i+z016zy9vA9r911kTMZHRxAy3QkGSGT2RT+\nrCpSx4/VBEnkjWNHiDxpg8v+R70rfk/Fla4OndTRQ8Bnc+MUCH7lP59zuDMKz10/\nNIeWiu5T6CUVAgMBAAGjgbIwga8wDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8E\nBAMCAQYwbQYIKwYBBQUHAQwEYTBfoV2gWzBZMFcwVRYJaW1hZ2UvZ2lmMCEwHzAH\nBgUrDgMCGgQUj+XTGoasjY5rw8+AatRIGCx7GS4wJRYjaHR0cDovL2xvZ28udmVy\naXNpZ24uY29tL3ZzbG9nby5naWYwHQYDVR0OBBYEFH/TZafC3ey78DAJ80M5+gKv\nMzEzMA0GCSqGSIb3DQEBBQUAA4IBAQCTJEowX2LP2BqYLz3q3JktvXf2pXkiOOzE\np6B4Eq1iDkVwZMXnl2YtmAl+X6/WzChl8gGqCBpH3vn5fJJaCGkgDdk+bW48DW7Y\n5gaRQBi5+MHt39tBquCWIMnNZBU4gcmU7qKEKQsTb47bDN0lAtukixlE0kF6BWlK\nWE9gyn6CagsCqiUXObXbf+eEZSqVir2G3l6BFoMtEMze/aiCKm0oHw0LxOXnGiYZ\n4fQRbxC1lfznQgUy286dUV4otp6F01vvpX1FQHKOtw5rDgb7MzVIcbidJ4vEZV8N\nhnacRHr2lVz2XTIIM6RUthg/aFzyQkqFOFSDX9HoLPKsEdao7WNq\n-----END CERTIFICATE-----\n";
  const char *pClientCert = "-----BEGIN CERTIFICATE-----\nMIIDWTCCAkGgAwIBAgIUSjhtgVTAcfm6+YPMOVEBDiLREvwwDQYJKoZIhvcNAQEL\nBQAwTTFLMEkGA1UECwxCQW1hem9uIFdlYiBTZXJ2aWNlcyBPPUFtYXpvbi5jb20g\nSW5jLiBMPVNlYXR0bGUgU1Q9V2FzaGluZ3RvbiBDPVVTMB4XDTE4MTIxODAyMDgx\nNloXDTQ5MTIzMTIzNTk1OVowHjEcMBoGA1UEAwwTQVdTIElvVCBDZXJ0aWZpY2F0\nZTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAO11PkFJXVWqB2zi2RO0\n8jEUVcxry6gvaZaSKJNvOnHelRbZGsBzoqMkyiFB66enDcglZ2TDnkyBCZTR0iy+\nMZMMETGNSMJl+x7PrGhMIEHer3O0SmkHeo+mqLzm9Vnm0kjsEOAoPPvT/ZPgHPL4\nItIJpCiW6Ik2DlveEMcJ7NmQq8P8xKZCYg4+he5G71SgxmN60Q6pofzDe8+pKnP0\nJ5KAdKv9uEprNkDWO+H3qawfsDMya4d1+39dmUxrG2+qZUw2iaXxbzhIHV5lAz3T\nxAdGu3FAX4CLfM5DoxdNYuKukHlp0aqxKH1exP7NRFc8nSFZQ/rsu1OpNSVhaiIa\nH4ECAwEAAaNgMF4wHwYDVR0jBBgwFoAUebEuuqhjriEN/EoxC022hY4BZl4wHQYD\nVR0OBBYEFIg25RJ96yiI/nihDbp+kauRiM9AMAwGA1UdEwEB/wQCMAAwDgYDVR0P\nAQH/BAQDAgeAMA0GCSqGSIb3DQEBCwUAA4IBAQAtTTZ+MYTKvj5omkshja6FEyg6\nU7Y5lAyQRiM13D53eO8NO2aNK1WjlCRXrLbC5CX280/leEb4lqsGWsZ0lVpFgKy2\nxa/nJSKn+hcdJBTPRNEisgaoGQijlOKi1YBOyrqeN63C4O7c0EjAul+UrO25CcbN\nDcTtbrXiZenZ8vvfdIoSPGhBMQJOcixxD/JX8Wsxynts0rnXVHamQQzCU2ANfaPS\nxlTcJWGHdx48pbDBQZL1rO1tBlO91E64gyB1+AQs52H/bsNsKGDJvecWeVoRf96H\n32H9RcDWc2CUJvfR2e2YahuG2/rSxl6c9YTUchyetBUFv6AKUDLUfg2DSlAc\n-----END CERTIFICATE-----\n";
  const char *pClientPrivateKey = "-----BEGIN RSA PRIVATE KEY-----\nMIIEpQIBAAKCAQEA7XU+QUldVaoHbOLZE7TyMRRVzGvLqC9plpIok286cd6VFtka\nwHOioyTKIUHrp6cNyCVnZMOeTIEJlNHSLL4xkwwRMY1IwmX7Hs+saEwgQd6vc7RK\naQd6j6aovOb1WebSSOwQ4Cg8+9P9k+Ac8vgi0gmkKJboiTYOW94Qxwns2ZCrw/zE\npkJiDj6F7kbvVKDGY3rRDqmh/MN7z6kqc/QnkoB0q/24Sms2QNY74feprB+wMzJr\nh3X7f12ZTGsbb6plTDaJpfFvOEgdXmUDPdPEB0a7cUBfgIt8zkOjF01i4q6QeWnR\nqrEofV7E/s1EVzydIVlD+uy7U6k1JWFqIhofgQIDAQABAoIBAQDlAxwljwILKk66\n/xVo4ixqlTgiTRw/emqVIyUq/C0bwRV80anxThTezw56gL8sE/tAuoI7NkandhOt\niabwLAZX0V2Lsr0nb39/ub6HYFQD8ya3qnvRS7sybAN1HDIooGrSlYm5BlW6cjuv\nsId69UFv3IiL9O8T1iSHCWaY78OUFw0xJABigjWNaS6oNRCjvCt3ENx9AmgjKNZu\ncYjkRE6s4ZMtOmK1zoMl03TvY9wuNyUL4L+JleNjVE8zPiY/I7f5NE++7HUZN78q\nKMRb+kqzvc76vSW75U5Dag6Ym3vaSEeD1g19FbPzdOIZ2csN89W/LYWWAPtc7VGB\nluCHTGohAoGBAP1Pt9kThGN6BsWlbh8TGZRJCWw7YWCXE4FUy/vFxsXttSISnr+r\nh9a8cfQ6jCqAs9N+hP6SfvY3ixbH6gXsVQ6UdHEusW92GOmPVoLn05Z9YTl2FW0E\nPbA5rxVIICXtq5Cl60nFkbRp1lsRHUoCxQEAwvaFNAy1NyIohpghpEiVAoGBAO/6\ncvk0S8scJjSFZCN8Ps4IaPBQ/5K6W6NBtxJ7vRzhxAH2qHecgoDl3uLMo3LZNDsc\n6ElDxuhny9MkxnNeRhKpa02RUx27T84qBodAMQd7feQwd/DFqnfauKAbCZw6XApy\nB+TJcl2seZd3a6u5SEvMdVSZKrk05sa52I08aIQ9AoGBAOsTVucx0JNMhApZmpdl\n70AIg6EYfibYLPbBzcdp5Cx8i7MCGqZ/NcGXAh2HU9qBs+BasmtZYk92fjJ29fvn\nNYwhQupiTbHFpBrjtYIoR9iJpHdh2UJhRCpNbejPFV59cND82RyOJ8iIy6+Zc1fi\nuBZVMZj7RrFgXrjpaPa3ibD9AoGAUj72upwGwfTCz0tMN7E0YS6WsI5CGUqPb5Nt\nKAOKm+RwZe/gayYTwBOIYRibiBgTA5NztPBqsmoofBPlg9Kyl2DDKtF8t/rjbhgP\nJrmEVDkpu1Q1Nyq3st4eWpkJoSaLNvU/6VR1Qs+C2LuQBkm5EmlfaQAZmWIdGczG\nABnNBE0CgYEA5GkVIjN9h+yRVMwKE+jLJjYgjAgNLSt9B4YIG/NZPrdd47VaUpfu\nmrMQJJuxbGjMjBTxQpKPB2ReJQ5xQ6XIpBIP5G6MAL2agKWWOi7WJd23OzrpkVvw\nkVa5tWm5jsAvWndeSlpGYQPdR2TqU0CpXlPUJuq9G6FmrsRSAlDjmDc=\n-----END RSA PRIVATE KEY-----\n";
  const char *pDeviceName = "test_hub";
  char cPayload[AWS_IOT_MQTT_TX_BUF_LEN];
	
	
  int connectCounter;
  IoT_Error_t rc = FAILURE;
  AWS_IoT_Client client;
  memset(&client, 0, sizeof(AWS_IoT_Client));
  IoT_Client_Init_Params mqttInitParams = iotClientInitParamsDefault;
  IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;

  
  // $aws/things/Node413/HubF561
//  sprintf(cPTopicName,"$aws/things/Node413/HubF561");
//  sprintf(cSTopicName,"$aws/things/Node413/HubF561");

  sprintf(cPTopicName,"$aws/things/test_hub/hub/F561");
  sprintf(cSTopicName,"$aws/things/test_hub/hub/F561");

  msg_info("AWS IoT SDK Version %d.%d.%d\n", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH);

  //getServerAddress(&serverAddress);
  //getTLSKeys(&pCaCert, &pClientCert, &pClientPrivateKey);
  mqttInitParams.enableAutoReconnect = false; /* We enable this later below */
  mqttInitParams.pHostURL = (char *) serverAddress;
  mqttInitParams.port = AWS_IOT_MQTT_PORT;
  mqttInitParams.pRootCALocation = (char *) pCaCert;
  mqttInitParams.pDeviceCertLocation = (char *) pClientCert;
  mqttInitParams.pDevicePrivateKeyLocation = (char *) pClientPrivateKey;
  mqttInitParams.mqttCommandTimeout_ms = 20000;
  mqttInitParams.tlsHandshakeTimeout_ms = 5000;
  mqttInitParams.isSSLHostnameVerify = true;
  mqttInitParams.disconnectHandler = disconnectCallbackHandler;
  mqttInitParams.disconnectHandlerData = NULL;
  rc = aws_iot_mqtt_init(&client, &mqttInitParams);
  if(AWS_SUCCESS != rc)
  {
    msg_error("aws_iot_mqtt_init returned error : %d\n", rc);
    return -1;
  }

  //getIoTDeviceConfig(&pDeviceName);
  connectParams.keepAliveIntervalInSec = 30;
  connectParams.isCleanSession = true;
  connectParams.MQTTVersion = MQTT_3_1_1;
  connectParams.pClientID = (char *) pDeviceName;
  connectParams.clientIDLen = (uint16_t) strlen(pDeviceName);
  connectParams.isWillMsgPresent = false;

  connectCounter = 0;
  
  do 
  {
    connectCounter++;
    printf("MQTT connection in progress:   Attempt %d/%d ...\n",connectCounter,MQTT_CONNECT_MAX_ATTEMPT_COUNT);
    rc = aws_iot_mqtt_connect(&client, &connectParams);
  } while((rc != AWS_SUCCESS) && (connectCounter < MQTT_CONNECT_MAX_ATTEMPT_COUNT));  
  if(AWS_SUCCESS != rc) 
  {
    msg_error("Error(%d) connecting to %s:%d\n\n", rc, mqttInitParams.pHostURL, mqttInitParams.port);
    return -1;
  }
  else
  {
    printf("Connected to %s:%d\n\n", mqttInitParams.pHostURL, mqttInitParams.port);
  }
  
  
  rc = aws_iot_mqtt_subscribe(&client, cSTopicName, strlen(cSTopicName), QOS0, MQTTcallbackHandler, NULL);
  if(AWS_SUCCESS != rc)
  {
    msg_error("Error subscribing : %d\n", rc);
    return -1;
  } 
  else
  {
    msg_info("Subscribed to topic %s\n", cSTopicName);
  }
  

  IoT_Publish_Message_Params paramsQOS1 = {QOS1, 0, 0, 0, NULL,0};
  
  printf("Waiting for command and message or Push the user button[SW2] to exit the demonstration \n\n");
  while((AWS_SUCCESS == rc) && infinitePublishFlag)
  {
    /* Max time the yield function will wait for read messages */
    rc = aws_iot_mqtt_yield(&client, 10);
	
	if(BSP_KEY_Read() == USER_BUTTON1)
	{
		msg_info("\nPushed button, Terminates the demonstration.\n");
		infinitePublishFlag = false;
		break;
	}
	
	UART_Recv_Data(cmd_buf,&cmd_len,QUARTER_SECOND);
	if(cmd_len && strstr((char *)cmd_buf,"CMD+SysInfo"))
    {
		sprintf(cPayload, "{\n    \"SysInfo#DeviceID\": \"HardVersion|SoftVersion|BootVersion\"\n}");
		cmd_send = true;
    }
	else if(cmd_len && strstr((char *)cmd_buf,"CMD+Battery"))
    {
		sprintf(cPayload, "{\n    \"Battery#DeviceID\": \"value\"\n}");
		cmd_send = true;
    }
	else if(cmd_len && strstr((char *)cmd_buf,"CMD+Data"))
    {
		sprintf(cPayload, "{\n    \"Data#DeviceID\": \"Time|PHValue|FCLValue|TAValue|Conduct|TempValue\"\n}");
		cmd_send = true;
    }
	else if(cmd_len && strstr((char *)cmd_buf,"CMD+DevicePara"))
    {
		sprintf(cPayload, "{\n    \"DevicePara#DeviceID\": \"value1-value2-value3-value4\"\n}");
		cmd_send = true;
    }
	
	if(cmd_send)
	{
		cmd_send = false;
		
		paramsQOS1.payload = (void *) cPayload;
		paramsQOS1.payloadLen = strlen(cPayload) + 1;

		rc = aws_iot_mqtt_publish(&client, cPTopicName, strlen(cPTopicName), &paramsQOS1);
		if (rc == AWS_SUCCESS)
		{
		  printf("\nPublished to topic %s:\n", cPTopicName);
		  printf("%s\n", cPayload);
		}
	}
    
  } /* End of while */
  
  /* Wait for all the messages to be received */
  aws_iot_mqtt_yield(&client, 10);

  rc = aws_iot_mqtt_disconnect(&client);


  return rc;
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
