#include "api.h"

IWDG_HandleTypeDef IWDG_Handler;

void IWDG_Init(uint8_t prer,uint16_t rlr)
{
    IWDG_Handler.Instance=IWDG;
    IWDG_Handler.Init.Prescaler=prer;
    IWDG_Handler.Init.Reload=rlr;
    HAL_IWDG_Init(&IWDG_Handler);
	
    HAL_IWDG_Init(&IWDG_Handler);
}
    

void IWDG_Feed(void)
{   
    HAL_IWDG_Refresh(&IWDG_Handler);
}
