#include "api.h"
#include "func.h"
#include "aws_iot_config.h"
#include "unicast-receiver.h"

extern void Contiki_SysTick_Handler(void);
extern uint32_t  WifiTxWaitDataTimeOut;

uint32_t G_timer_time1 = 0;
uint32_t G_timer_time2 = 0;
uint32_t G_timer_time3 = 0;
uint32_t G_sys_cnt = 0;

uint32_t G_batt_scan_time = 0;
uint32_t G_subg_pair_time = 0;
uint32_t G_subg_out_time = 0;
uint32_t G_devOTA_out_time = 0;
uint32_t G_led_white_time = 0;
uint32_t G_led_white_interval = 0;
uint32_t G_sys_tick = 0;

void SysTick_Handler(void)
{
	HAL_IncTick();
	Contiki_SysTick_Handler();
	if(WifiTxWaitDataTimeOut)
	{
		WifiTxWaitDataTimeOut--;
	}
	
	G_sys_tick = HAL_GetTick();
	if(G_batt_scan_time == G_sys_tick)
	{
		G_batt_scan_time += BATT_INTERVAL_TICK;
		BSP_CHG_Check();
	}
	
	if(G_subg_pair_time == G_sys_tick)
	{
		g_subg_pair = 0;
		BSP_LED_ON(BSP_LED_ID_ALL);
		G_led_white_time = 0;
	}
	
	if(G_subg_out_time == G_sys_tick)
	{
		if(g_resend_num)
		{
			G_subg_out_time = HAL_GetTick() + 5000;
			g_resend_num--;
			g_resend_flag = 1;
		}
		else if(g_resend_loop)
		{
			G_subg_out_time = HAL_GetTick() + 5000;
			g_resend_loop--;
			g_resend_num = 5;
			g_wakeup_flag = 1;
		}
		else
		{
			G_subg_out_time = 0;
		}
	}
	
	if(G_devOTA_out_time == G_sys_tick)
	{
		
	}
	
	if(G_led_white_time == G_sys_tick)
	{
		G_led_white_time = HAL_GetTick() + G_led_white_interval;
		BSP_LED_Toggle(BSP_LED_ID_ALL);
	}
}

void BSP_Timer_Start(uint8_t t_no,uint32_t t_cnt)
{
	uint32_t tickstart = HAL_GetTick();
	
	if(t_no == SYS_TIMER1)
	{
		G_timer_time1 = tickstart + t_cnt;
	}
	else if(t_no == SYS_TIMER2)
	{
		G_timer_time2 = tickstart + t_cnt;
	}
	else if(t_no == SYS_TIMER3)
	{
		G_timer_time3 = tickstart + t_cnt;
	}
}

uint32_t BSP_Timer_Read(uint8_t t_no)
{
	uint32_t ticknow = HAL_GetTick();
	
	if(t_no == SYS_TIMER1)
	{
		if(G_timer_time1 > ticknow)
		{
			return (G_timer_time1 - ticknow);
		}
	}
	else if(t_no == SYS_TIMER2)
	{
		if(G_timer_time2 > ticknow)
		{
			return (G_timer_time2 - ticknow);
		}
	}
	else if(t_no == SYS_TIMER3)
	{
		if(G_timer_time3 > ticknow)
		{
			return (G_timer_time3 - ticknow);
		}
	}
	
	return 0;
}


void BSP_Cnt_Start(void)
{
	uint32_t G_sys_cnt = HAL_GetTick();
}

uint32_t BSP_Cnt_Read(void)
{
    
    return HAL_GetTick() - G_sys_cnt;
}



