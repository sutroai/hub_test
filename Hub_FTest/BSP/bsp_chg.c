#include "api.h"
#include "func.h"

int8_t g_CHG_STAT = 0;
const uint8_t g_CHG_Toast[3][20] = {"noBattery","charging","endOfCharge"};


void BSP_CHG_Init(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	
	GPIO_InitStruct.Pin   = GPIO_PIN_10;					// CHG_EN
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	GPIO_InitStruct.Pin   = GPIO_PIN_0;						// SW7
	GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	GPIO_InitStruct.Pin   = GPIO_PIN_2;						// CHG_OK
	GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	GPIO_InitStruct.Pin   = GPIO_PIN_12;					// AC_OK
	GPIO_InitStruct.Mode  = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);
	G_batt_scan_time = HAL_GetTick() + BATT_INTERVAL_TICK;
}

void BSP_CHG_Check(void)
{
	static uint8_t times = 0;
	static uint8_t firstInsert = 0;
	
	if(HAL_GPIO_ReadPin(GPIOC,GPIO_PIN_0) == GPIO_PIN_RESET)
	{
		if(!firstInsert)
		{
			firstInsert = 1;
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_RESET);
			return;
		}
		
		if(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_2) == GPIO_PIN_RESET && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_12) == GPIO_PIN_RESET)
		{	// In charging
			BSP_LED_ON(BSP_LED_ID_RED);
			g_CHG_STAT = 1;
		}
		else if(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_2) == GPIO_PIN_SET && HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_12) == GPIO_PIN_RESET)
		{	// Complete/Suspend
			BSP_LED_ON(BSP_LED_ID_GREEN);
			g_CHG_STAT = 2;
			if(!times)
			{
				times++;
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);
				G_batt_scan_time += BATT_INTERVAL_TICK*5;
				firstInsert = 0;
			}
		}
	}
	else
	{
		firstInsert = 0;
		times = 0;		
		if(g_CHG_STAT)
		{
			BSP_LED_ON(BSP_LED_ID_ALL);
		}
	    g_CHG_STAT = 0;
		HAL_GPIO_WritePin(GPIOB, GPIO_PIN_10, GPIO_PIN_SET);
	}
}	
