#include "api.h"

UART_HandleTypeDef  UART1_Handler;
volatile static uint8_t G_uart1_buf[UART_BUF_LEN];
uint32_t G_uart1_num = 0;
volatile static uint32_t G_uart1_pos = 0;

UART_HandleTypeDef  UART2_Handler;
volatile static uint8_t G_uart2_buf[UART_BUF_LEN];
uint32_t G_uart2_num = 0;
volatile static uint32_t G_uart2_pos = 0;

UART_HandleTypeDef  UART6_Handler;
volatile static uint8_t G_uart6_buf[UART_BUF_LEN];
uint32_t G_uart6_num = 0;
volatile static uint32_t G_uart6_pos = 0;


#define WIFI_UART_BUFF_SIZE			2048
//TX
volatile uint8_t   WifiTxBuff[WIFI_UART_BUFF_SIZE];
volatile uint32_t  WifiTxBuffCnt;
volatile uint32_t  WifiTxBuffQIn;
volatile uint32_t  WifiTxBuffQOut;
volatile uint32_t  WifiTxBusyFlag;
volatile uint32_t  WifiTxWaitDataTimeOut;
//RX
volatile uint8_t   WifiRxBuff[WIFI_UART_BUFF_SIZE];
volatile uint32_t  WifiRxBuffQCnt;
volatile uint32_t  WifiRxBuffQIn;
volatile uint32_t  WifiRxBuffQOut;
volatile uint32_t  WifiRxWaitDataTimeOut;


int fputc(int ch, FILE *f)
{
	HAL_UART_Transmit(&UART1_Handler, (uint8_t *)&ch, 1, HAL_MAX_DELAY); 
	while(__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_TC)!=SET);
	return ch;
}

void USART1_IRQHandler(void)
{
	uint8_t Res;
	
	if((__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_RXNE)!=RESET))
	{
		HAL_UART_Receive(&UART1_Handler,&Res,1,1000);
		G_uart1_buf[G_uart1_pos] = Res;
		
		G_uart1_num++;
		if(G_uart1_num > UART_BUF_LEN)
		{
			G_uart1_num = UART_BUF_LEN;							
		}
		
		G_uart1_pos++;
		if(G_uart1_pos == UART_BUF_LEN)
		{
			G_uart1_pos = 0;
		}
	}
	HAL_UART_IRQHandler(&UART1_Handler);
} 

void BSP_UART1_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	//Enable GPIO and USART clock
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_USART1_CLK_ENABLE();
	
	//UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = GPIO_PIN_6 | GPIO_PIN_7;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	UART1_Handler.Instance        = USART1;
	UART1_Handler.Init.BaudRate   = 115200;
	UART1_Handler.Init.WordLength = UART_WORDLENGTH_8B;
	UART1_Handler.Init.StopBits   = UART_STOPBITS_1;
	UART1_Handler.Init.Parity     = UART_PARITY_NONE;
	UART1_Handler.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UART1_Handler.Init.Mode       = UART_MODE_TX_RX;
	HAL_UART_Init(&UART1_Handler);
	
	__HAL_UART_ENABLE_IT(&UART1_Handler, UART_IT_RXNE);
	
	HAL_NVIC_EnableIRQ(USART1_IRQn);
	HAL_NVIC_SetPriority(USART1_IRQn,3,0);
}

//void BSP_UART1_SendChar(uint8_t ch)
//{
//	HAL_UART_Transmit(&UART1_Handler, &ch, 1, HAL_MAX_DELAY);
//	while(__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_TC)!=SET);
//}

void BSP_UART1_SendStr(uint8_t *str,uint32_t len)
{
	HAL_UART_Transmit(&UART1_Handler, str, len, HAL_MAX_DELAY);
	while(__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_TC)!=SET);
}

short BSP_UART1_RecvChar(void)
{
	uint8_t ch;
	
	if(G_uart1_num>0)
	{	
		__set_PRIMASK(1);
		// roll buffer
		ch = G_uart1_buf[ (G_uart1_pos+UART_BUF_LEN-G_uart1_num)%UART_BUF_LEN ];
		G_uart1_num--;
		if(G_uart1_num==0)
		{
			G_uart1_pos = 0;
		}
		__set_PRIMASK(0);
		return ch;
	}
	return -1;	
}

void BSP_UART1_FlushFIFO(void)
{
	__set_PRIMASK(1);
	G_uart1_num = 0;
	G_uart1_pos = 0;	
	__set_PRIMASK(0);
}

void USART2_IRQHandler(void)                	
{
	uint8_t Res;
	
	if((__HAL_UART_GET_FLAG(&UART2_Handler,UART_FLAG_RXNE)!=RESET))
	{
		HAL_UART_Receive(&UART2_Handler,&Res,1,1000);
		G_uart2_buf[G_uart2_pos] = Res;
		
		G_uart2_num++;
		if(G_uart2_num > UART_BUF_LEN)
		{
			G_uart2_num = UART_BUF_LEN;							
		}
		
		G_uart2_pos++;
		if(G_uart2_pos == UART_BUF_LEN)
		{
			G_uart2_pos = 0;
		}
	}
	HAL_UART_IRQHandler(&UART2_Handler);
} 

void BSP_UART2_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	//Enable GPIO and USART clock
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_USART2_CLK_ENABLE();
	
	//UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = GPIO_PIN_2 | GPIO_PIN_3;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	UART2_Handler.Instance        = USART2;
	UART2_Handler.Init.BaudRate   = 115200;
	UART2_Handler.Init.WordLength = UART_WORDLENGTH_8B;
	UART2_Handler.Init.StopBits   = UART_STOPBITS_1;
	UART2_Handler.Init.Parity     = UART_PARITY_NONE;
	UART2_Handler.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UART2_Handler.Init.Mode       = UART_MODE_TX_RX;
	HAL_UART_Init(&UART2_Handler);
	
	__HAL_UART_ENABLE_IT(&UART2_Handler, UART_IT_RXNE);
	
	HAL_NVIC_EnableIRQ(USART2_IRQn);
	HAL_NVIC_SetPriority(USART2_IRQn,4,0);
}

void BSP_UART2_SendStr(uint8_t *str,uint32_t len)
{
	HAL_UART_Transmit(&UART2_Handler, str, len, HAL_MAX_DELAY);
	while(__HAL_UART_GET_FLAG(&UART2_Handler,UART_FLAG_TC)!=SET);
}

short BSP_UART2_RecvChar(void)
{
	uint8_t ch;
	
	if(G_uart2_num>0)
	{	
		__set_PRIMASK(1);
		// roll buffer
		ch = G_uart2_buf[ (G_uart2_pos+UART_BUF_LEN-G_uart2_num)%UART_BUF_LEN ];
		G_uart2_num--;
		if(G_uart2_num==0)
		{
			G_uart2_pos = 0;
		}
		__set_PRIMASK(0);
		return ch;
	}
	return -1;	
}

void BSP_UART2_FlushFIFO(void)
{
	__set_PRIMASK(1);
	G_uart2_num = 0;
	G_uart2_pos = 0;	
	__set_PRIMASK(0);
}

void USART6_IRQHandler(void)
{
	HAL_UART_IRQHandler(&UART6_Handler);
}
/*
void WIFI_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)	
{
	uint8_t Res;
	
	if((__HAL_UART_GET_FLAG(&UART6_Handler,UART_FLAG_RXNE)!=RESET))
	{
		HAL_UART_Receive(&UART6_Handler,&Res,1,1000);
		G_uart6_buf[G_uart6_pos] = Res;
		
		G_uart6_num++;
		if(G_uart6_num > UART_BUF_LEN)
		{
			G_uart6_num = UART_BUF_LEN;							
		}
		
		G_uart6_pos++;
		if(G_uart6_pos == UART_BUF_LEN)
		{
			G_uart6_pos = 0;
		}
	}
} */

void BSP_UART6_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	//Enable GPIO and USART clock
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_USART6_CLK_ENABLE();
	
	//init variables
	WifiRxBuffQCnt = 0;
	WifiRxBuffQIn = 0;
	WifiRxBuffQOut = 0;
	WifiRxWaitDataTimeOut = 0;
	WifiTxBuffCnt = 0;
	WifiTxBuffQIn = 0;
	WifiTxBuffQOut = 0;
	WifiTxBusyFlag = 0;
	WifiTxWaitDataTimeOut = 0;
	
	//UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = GPIO_PIN_11 | GPIO_PIN_12;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	UART6_Handler.Instance        = USART6;
	UART6_Handler.Init.BaudRate   = 115200;
	UART6_Handler.Init.WordLength = UART_WORDLENGTH_8B;
	UART6_Handler.Init.StopBits   = UART_STOPBITS_1;
	UART6_Handler.Init.Parity     = UART_PARITY_NONE;
	UART6_Handler.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UART6_Handler.Init.Mode       = UART_MODE_TX_RX;
	HAL_UART_Init(&UART6_Handler);
	
	//__HAL_UART_ENABLE_IT(&UART6_Handler, UART_IT_RXNE|UART_IT_TXE);
	HAL_UART_Receive_IT(&UART6_Handler, (uint8_t *)&WifiRxBuff[0], 1);
	
	HAL_NVIC_EnableIRQ(USART6_IRQn);
	HAL_NVIC_SetPriority(USART6_IRQn,0,0);
}

void BSP_UART6_SendStr(uint8_t *str,uint32_t len)
{
	HAL_UART_Transmit(&UART6_Handler, str, len, HAL_MAX_DELAY);
	while(__HAL_UART_GET_FLAG(&UART6_Handler,UART_FLAG_TC)!=SET);
}


/**
  * @brief  Rx Transfer completed callback
  */
void WIFI_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	//buffer not full
	if(WifiRxBuffQCnt < WIFI_UART_BUFF_SIZE){
		WifiRxBuffQCnt++;
		WifiRxBuffQIn++;
		WifiRxBuffQIn %= WIFI_UART_BUFF_SIZE;
	}else{
		//just stop in current receive position
		WifiRxBuffQCnt;
		WifiRxBuffQIn;
	}
	HAL_UART_Receive_IT(UartHandle, (uint8_t *)&WifiRxBuff[WifiRxBuffQIn], 1);
}

/**
  * @brief  Tx Transfer completed callback
  */
void WIFI_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	uint32_t  Size;
	
	//Transmission Process
	if(WifiTxBuffCnt){
		if(WifiTxBuffQOut < WifiTxBuffQIn){
			Size = WifiTxBuffQIn - WifiTxBuffQOut;
		}else{
			Size = WIFI_UART_BUFF_SIZE - WifiTxBuffQOut;
		}
		HAL_UART_Transmit_IT(UartHandle, (uint8_t *)&WifiTxBuff[WifiTxBuffQOut], Size);
		
		WifiTxBuffQOut += Size;
		if(WifiTxBuffQOut >= WIFI_UART_BUFF_SIZE)  WifiTxBuffQOut = 0;
		
		WifiTxBuffCnt = WifiTxBuffCnt - Size;
		WifiTxBusyFlag = 1;
	}else{
		WifiTxBusyFlag = 0;
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	//rx process
	if(&UART6_Handler == UartHandle){
		WIFI_UART_RxCpltCallback(UartHandle);
	}
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *UartHandle)
{
//	uint32_t  Size;			// Frank comment
	//tx process
	if(&UART6_Handler == UartHandle){
		WIFI_UART_TxCpltCallback(UartHandle);
	}
}


/**
  * @brief  Send wifi Data thru UART
  * @param  pdata : pointer to data
  * @param  len : Data length
  * @param  timeout : send timeout in mS
  * @retval Length of sent data
  */
int16_t UART_WIFI_SendData(uint8_t *pdata,  uint16_t len, uint32_t timeout)
{
	uint32_t  Size, i;
	
	//test-code
	//static uint32_t MaxLen=0;
	//if(len > MaxLen)  MaxLen = len;
	
	//space not enough//wait data ready
	WifiTxWaitDataTimeOut = timeout;
	while( ((len + WifiTxBuffCnt) > WIFI_UART_BUFF_SIZE) && (WifiTxWaitDataTimeOut) );
	if(0==WifiTxWaitDataTimeOut){
		return -1;
	}
	
	//copy to ComTxBuff
	__set_PRIMASK(1);//CPU_CRITICAL_ENTER();
	i = 0;
	while(i < len){
		WifiTxBuff[WifiTxBuffQIn++] = pdata[i++];
		WifiTxBuffQIn %= WIFI_UART_BUFF_SIZE;
	}
	
	WifiTxBuffCnt += len;
	if(0 == WifiTxBusyFlag){
		if(WifiTxBuffQOut < WifiTxBuffQIn){
			Size = WifiTxBuffQIn - WifiTxBuffQOut;
		}else{
			Size = WIFI_UART_BUFF_SIZE - WifiTxBuffQOut;
		}
		HAL_UART_Transmit_IT(&UART6_Handler, (uint8_t *)&WifiTxBuff[WifiTxBuffQOut], Size);
		
		WifiTxBuffQOut = WifiTxBuffQOut + Size;
		if(WifiTxBuffQOut >= WIFI_UART_BUFF_SIZE)  WifiTxBuffQOut = 0;
		
		WifiTxBuffCnt -= Size;
		WifiTxBusyFlag = 1;
	}
	__set_PRIMASK(0);//CPU_CRITICAL_EXIT();
	
	return len;
}


short BSP_UART6_RecvChar(void)
{
	uint16_t ch;
	/*
	if(G_uart6_num>0)
	{	
		__set_PRIMASK(1);
		// roll buffer
		ch = G_uart6_buf[ (G_uart6_pos+UART_BUF_LEN-G_uart6_num)%UART_BUF_LEN ];
		G_uart6_num--;
		if(G_uart6_num==0)
		{
			G_uart6_pos = 0;
		}
		__set_PRIMASK(0);
		return ch;
	}
	return -1;
	*/
	if(WifiRxBuffQCnt){
		__set_PRIMASK(1);
		ch = WifiRxBuff[WifiRxBuffQOut++];
		WifiRxBuffQOut %= WIFI_UART_BUFF_SIZE;
		WifiRxBuffQCnt--;
		__set_PRIMASK(0);
		return ch;
	}else{
		return -1;
	}
}


void BSP_UART6_FlushFIFO(void)
{
	__set_PRIMASK(1);
	G_uart6_num = 0;
	G_uart6_pos = 0;
	WifiRxBuffQCnt = 0;
	WifiRxBuffQOut = WifiRxBuffQIn;
	__set_PRIMASK(0);
}


