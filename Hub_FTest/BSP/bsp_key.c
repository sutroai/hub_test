#include "api.h"

uint8_t G_key = 0;

void EXTI4_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4);
}

void EXTI9_5_IRQHandler(void)
{
    HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5);
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
    switch(GPIO_Pin)
    {
        case GPIO_PIN_0:

            break;
        case GPIO_PIN_1:

            break;
        case GPIO_PIN_2:

            break;
		case GPIO_PIN_4:
			G_key = USER_BUTTON2;
			break;
        case GPIO_PIN_5:
			G_key = USER_BUTTON1;
            break;
    }
}

void BSP_KEY_Init(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();


	GPIO_InitStruct.Pin   = GPIO_PIN_4;					// BUTTON2|SW5
	GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	GPIO_InitStruct.Pin   = GPIO_PIN_5;					// BUTTON1|SW2
	GPIO_InitStruct.Mode  = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
    HAL_NVIC_SetPriority(EXTI4_IRQn,6,0);
    HAL_NVIC_EnableIRQ(EXTI4_IRQn);	
	
    HAL_NVIC_SetPriority(EXTI9_5_IRQn,7,0);
    HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

uint8_t BSP_KEY_Read(void)
{
	uint8_t key;
	
	HAL_NVIC_DisableIRQ(EXTI4_IRQn);
	HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	key = G_key;
	G_key = 0;
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
	
	return key;
}

void Key_Flush_FIFO(void)
{
	G_key = 0;
}



