#ifndef __ESP8266_H
#define __ESP8266_H

#include "stm32f4xx.h"

#define		ESP_MODE_STA		1
#define		ESP_MODE_SAP		2
#define		ESP_MODE_SAPSTA		3

#define		ESP_LINK_SINGlE		0
#define		ESP_LINK_MUX		1

#define		ESP_SERVER_ON		1
#define		ESP_SERVER_OFF		0

int ESP8266_Module_Init(void);
void ESP8266_Module_Close(void);

void ESP8266_Get_Ver(uint8_t *ver);
int ESP8266_Set_CWMODE(uint8_t mode);
int ESP8266_Join_AP(uint8_t *ssid,uint8_t *pwd);
int ESP8266_Quit_AP(void);
void ESP8266_Query_AP(uint8_t *rssi);
void ESP8266_Get_IP(uint8_t *ip,uint8_t *mac);

void ESP8266_Get_APMAC(uint8_t *mac);
int ESP8266_Set_CWSAP(uint8_t *mac,uint8_t *pwd);
int ESP8266_Set_CIPAP(uint8_t *ip);
int ESP8266_Set_CIPMUX(uint8_t mux);
int ESP8266_Set_CIPSERVER(uint8_t on);
int ESP8266_List_AP(uint8_t *aps, uint16_t timeout);

int ESP8266_TCP_Ping(uint8_t *ip);
int ESP8266_TCP_Link(uint8_t *ip,uint16_t port);
int ESP8266_TCP_Unlink(void);
int ESP8266_TCP_Send(uint8_t *data,uint16_t len);
int ESP8266_TCP_MuxSend(uint8_t linkID,uint8_t *data,uint16_t len);
int ESP8266_TCP_Recv(uint8_t *data,uint16_t *len,uint16_t time);

int ESP8266_TS_Enter(void);
int ESP8266_TS_Exit(void);
int ESP8266_TS_EnSend(void);
int ESP8266_TS_ExSend(void);
int ESP8266_TS_Send(uint8_t *data,uint16_t len);
int ESP8266_TS_Recv(uint8_t *data,uint16_t reqlen,uint16_t *len,uint16_t time);

void ESP8266_OTA_Recv(uint8_t *data,uint16_t *len,uint16_t time);

//int ESP8266_UDP_Link(u8 *ip,u16 port);
//int ESP8266_UDP_Unlink(void);
//int ESP8266_UDP_Send(u8 *data,u16 len);
//int ESP8266_UDP_Recv(u8 *data,u16 *len,u16 time);


#endif

