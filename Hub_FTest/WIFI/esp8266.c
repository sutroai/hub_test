#include "api.h"

extern volatile uint32_t  WifiRxBuffQCnt;

static void UART_Send_CMD(uint8_t *at)
{
	BSP_UART6_FlushFIFO();
	//BSP_UART6_SendStr(at,strlen((char *)at));
	//BSP_UART6_SendStr((uint8_t *)"\r\n",2);
	UART_WIFI_SendData(at,strlen((char *)at),100);
	UART_WIFI_SendData((uint8_t *)"\r\n",2,100);
}

static void UART_Send_Data(uint8_t *data,uint16_t len)
{
	//BSP_UART6_FlushFIFO();
	//BSP_UART6_SendStr(data,len);
	UART_WIFI_SendData(data,len,100);
}

static void UART_Recv_Data(uint8_t *data,uint16_t *len,uint16_t time)
{	
	uint16_t ch;
	*len = 0;
	BSP_Timer_Start(SYS_TIMER1,time);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(WifiRxBuffQCnt)
		{
			ch = BSP_UART6_RecvChar();
			*data = ch;
			data++;
			(*len)++;
			BSP_Timer_Start(SYS_TIMER1,QUARTER_SECOND);
		}
	}
}

static void UART_Recv_DataEx(uint8_t *data,uint16_t *len,uint16_t time)
{
	char ch;
	
	*len = 0;
	BSP_Timer_Start(SYS_TIMER1,time);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(WifiRxBuffQCnt)
		{
			ch = BSP_UART6_RecvChar();
			if(ch == '+')		break;
			else
			{									
				*data = ch;
				data++;
				(*len)++;
				BSP_Timer_Start(SYS_TIMER1,QUARTER_SECOND);
			}
		}
	}
}

static void UART_Recv_Data_AWS(uint8_t *data,uint16_t reqlen,uint16_t *len,uint16_t time)
{	
	*len = 0;
	BSP_Timer_Start(SYS_TIMER1,1000);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(WifiRxBuffQCnt)
		{
			*data = BSP_UART6_RecvChar();
			data++;
			(*len)++;
			BSP_Timer_Start(SYS_TIMER1,1000);
			if((*len) >= reqlen)	break;
		}
	}
}

void ESP8266_Mondule_Enable(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin   = GPIO_PIN_2;
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);	
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_SET);
}

void ESP8266_Module_RST(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOA_CLK_ENABLE();
	
	GPIO_InitStruct.Pin   = GPIO_PIN_10;
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_10, GPIO_PIN_SET);
}

int ESP8266_Module_Init(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	ESP8266_Mondule_Enable();
	BSP_UART6_Init();
	HAL_Delay(200);
	ESP8266_Module_RST();
	HAL_Delay(2000);
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"ATE0");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	
	return 0;
}

void ESP8266_Module_Close(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOC_CLK_ENABLE();
	
	GPIO_InitStruct.Pin   = GPIO_PIN_2;
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_2, GPIO_PIN_RESET);
}

void ESP8266_Get_Ver(uint8_t *ver)
{
	uint8_t rbuf[200];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((unsigned char *)"AT+GMR");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	pres = strtok((char *)rbuf,":");	
	pres = strtok((char *)'\0',"\r");
	strcpy((char *)ver,pres);
}

int ESP8266_Set_CWMODE(uint8_t mode)
{
	uint8_t cmd[100];
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	memset(rbuf,0x00,sizeof(rbuf));
	sprintf((char *)cmd,"AT+CWMODE_CUR=%d",mode);
	UART_Send_CMD(cmd);
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int ESP8266_Join_AP(uint8_t *ssid,uint8_t *pwd)
{
	int ret;
	uint8_t rbuf[1024];
	uint16_t rlen;
	uint8_t cmd[100];
	char *pres;
	
	sprintf((char *)cmd,"AT+CWJAP_CUR=\"%s\",\"%s\"",ssid,pwd);
	UART_Send_CMD(cmd);
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Recv_Data(rbuf,&rlen,TEN_SECOND*2);
	if(strstr((char *)rbuf,"+CWJAP"))
	{
		pres = strtok((char *)rbuf,":");	
		pres = strtok((char *)'\0',"\r");
		ret = atoi(pres);
		return ret;
	}
	else if(strstr((char *)rbuf,"CONNECTED"))
	{
		memset(rbuf,0x00,sizeof(rbuf));
		UART_Recv_Data(rbuf,&rlen,TEN_SECOND);
		if(!strstr((char *)rbuf,"GOT IP"))
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		memset(rbuf,0x00,sizeof(rbuf));
		UART_Recv_Data(rbuf,&rlen,TEN_SECOND*2);
		if(strstr((char *)rbuf,"+CWJAP"))
		{
			pres = strtok((char *)rbuf,":");	
			pres = strtok((char *)'\0',"\r");
			ret = atoi(pres);
			return ret;
		}
		else if(strstr((char *)rbuf,"CONNECTED"))
		{
			memset(rbuf,0x00,sizeof(rbuf));
			UART_Recv_Data(rbuf,&rlen,TEN_SECOND);
			if(!strstr((char *)rbuf,"GOT IP"))
			{
				return -2;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			return -3;
		}
	}
}

int ESP8266_Quit_AP(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CWQAP");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"DISCONNECT"))
	{
		return -1;
	}
	
	return 0;
}

void ESP8266_Query_AP(uint8_t *rssi)
{
	uint8_t rbuf[100];
	uint16_t rlen;
    char *pres;

	UART_Send_CMD("AT+CWJAP_CUR?");
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Recv_Data(rbuf,&rlen, HALF_SECOND);
	pres = strtok((char *)rbuf,",");	
	pres = strtok('\0',",");
    pres = strtok('\0',",");
    pres = strtok('\0',"\r");
    strcpy((char *)rssi,pres);
}


void ESP8266_Get_IP(uint8_t *ip,uint8_t *mac)
{
	uint8_t rbuf[200];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CIFSR");
	UART_Recv_Data(rbuf,&rlen,TWO_SECOND);
	pres = strtok((char *)rbuf,"\"");	
	pres = strtok('\0',"\"");
	strcpy((char *)ip,pres);
	pres = strtok('\0',"\"");
	pres = strtok('\0',"\"");
	strcpy((char *)mac,pres);
}

void ESP8266_Get_APMAC(uint8_t *mac)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	char *pres;
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CIPAPMAC_CUR?");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	pres = strtok((char *)rbuf,"\"");	
	pres = strtok('\0',"\"");
	strcpy((char *)mac,pres);
}

int ESP8266_Set_CWSAP(uint8_t *mac,uint8_t *pwd)
{
	uint8_t cmd[100];
	uint8_t rbuf[100];
	uint16_t rlen;

    if(pwd == NULL)
    {
        sprintf((char *)cmd,"AT+CWSAP_CUR=\"Sutro_%.1s%.2s%.02s\",\"\",6,0",mac+10,mac+12,mac+15);
    }
    else
    {
        sprintf((char *)cmd,"AT+CWSAP_CUR=\"Sutro_%.1s%.2s%.02s\",\"%s\",6,4",mac+10,mac+12,mac+15,pwd);
    }
	UART_Send_CMD(cmd);	
	UART_Recv_Data(rbuf,&rlen,TWO_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int ESP8266_Set_CIPAP(uint8_t *ip)
{
	uint8_t cmd[100];
	uint8_t rbuf[100];
	uint16_t rlen;
	
	
	sprintf((char *)cmd,"AT+CIPAP_CUR=\"%s\"",ip);
	UART_Send_CMD(cmd);
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int ESP8266_Set_CIPMUX(uint8_t mux)
{
	uint8_t cmd[100];
	uint8_t rbuf[100];
	uint16_t rlen;
	
	sprintf((char *)cmd,"AT+CIPMUX=%d",mux);
	UART_Send_CMD(cmd);
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int ESP8266_Set_CIPSERVER(uint8_t on)
{
	uint8_t cmd[100];
	uint8_t rbuf[100];
	uint16_t rlen;
	
	sprintf((char *)cmd,"AT+CIPSERVER=%d",on);
	UART_Send_CMD(cmd);
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	return 0;
}

int ESP8266_List_AP(uint8_t *aps, uint16_t timeout)
{
	uint8_t rbuf[2048];
	uint16_t rlen;
	char *pres;
		
	UART_Send_CMD("AT+CWLAPOPT=1,6");
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Recv_Data(rbuf,&rlen,QUARTER_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	
	UART_Send_CMD("AT+CWLAP");
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Recv_Data(rbuf,&rlen, timeout * 1000);
	if(!strstr((char *)rbuf,"+CWLAP:"))
	{
		return -2;
	}
	
	sprintf((char *)aps,"{\"listAP\":{");
	pres = strtok((char *)rbuf,"(");
	pres = strtok((char *)'\0',",");
	sprintf((char *)aps+strlen((char *)aps),"%s:",pres);
	pres = strtok((char *)'\0',")");
	sprintf((char *)aps+strlen((char *)aps),"\"%s\",",pres);
	while(strstr(pres+strlen(pres)+1,"+CWLAP:"))
	{
		pres = strtok((char *)'\0',"(");
		pres = strtok((char *)'\0',",");
		if(strstr((char *)aps,pres) || strstr(pres,"\"\""))			continue;				// ignore repeat ssid and ""
		sprintf((char *)aps+strlen((char *)aps),"%s:",pres);
		pres = strtok((char *)'\0',")");
		sprintf((char *)aps+strlen((char *)aps),"\"%s\",",pres);
	}
	sprintf((char *)aps + strlen((char *)aps) - 1,"}}");				// -1 for cover last ","
	return 0;
}

int ESP8266_TCP_Ping(uint8_t *ip)
{
	uint8_t cmd[100];
	uint8_t rbuf[100];
	uint16_t rlen;
	
	sprintf((char *)cmd,"AT+PING=\"%s\"",ip);
	UART_Send_CMD(cmd);	
	UART_Recv_Data(rbuf,&rlen,THREE_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		UART_Recv_Data(rbuf,&rlen,THREE_SECOND);
		if(!strstr((char *)rbuf,"OK"))
		{
			return -1;
		}
	}
	return 0;
}

int ESP8266_TCP_Link(uint8_t *ip,uint16_t port)
{
	uint8_t rbuf[100];
	uint16_t rlen;
	uint8_t cmd[100];
	
	sprintf((char *)cmd,"AT+CIPSTART=\"TCP\",\"%s\",%d",ip,port);
	UART_Send_CMD(cmd);
	UART_Recv_Data(rbuf,&rlen,TEN_SECOND);
	if(!strstr((char *)rbuf,"CONNECT"))
	{
		return -1;
	}
	return 0;
}

int ESP8266_TCP_Unlink(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CIPCLOSE");
	UART_Recv_Data(rbuf,&rlen,THREE_SECOND);
	if(!strstr((char *)rbuf,"CLOSED"))
	{
		return -1;
	}
	
	return 0;
}

int ESP8266_TCP_Send(uint8_t *data,uint16_t len)
{
	uint8_t rbuf[1024];
	uint16_t rlen;
	uint8_t cmd[100];
	
	sprintf((char *)cmd,"AT+CIPSEND=%d",len);
	UART_Send_CMD(cmd);
	UART_Recv_Data(rbuf,&rlen,TWO_SECOND);
	if(strstr((char *)rbuf,">"))
	{
		memset(rbuf,0,sizeof(rbuf));
		UART_Send_Data(data,len);
		UART_Recv_DataEx(rbuf,&rlen,TWO_SECOND);
		if(strstr((char*)rbuf,"SEND OK"))
		{
			return 0;
		}
		else
		{
			UART_Recv_DataEx(rbuf,&rlen,TWO_SECOND);
			if(strstr((char*)rbuf,"SEND OK"))
			{
				return 0;
			}
			else
			{
				return -2;
			}
		}
	}
	else
	{
		return -1;
	}
}

int ESP8266_TCP_MuxSend(uint8_t linkID,uint8_t *data,uint16_t len)
{
	uint8_t rbuf[1024];
	uint16_t rlen;
	uint8_t cmd[100];
	
	sprintf((char *)cmd,"AT+CIPSEND=%d,%d",linkID,len);
	UART_Send_CMD(cmd);
	UART_Recv_Data(rbuf,&rlen,TWO_SECOND);
	if(strstr((char *)rbuf,">"))
	{
		memset(rbuf,0,sizeof(rbuf));
		UART_Send_Data(data,len);
		UART_Recv_DataEx(rbuf,&rlen,TWO_SECOND);
		if(strstr((char*)rbuf,"SEND OK"))
		{
			return 0;
		}
		else
		{
			UART_Recv_DataEx(rbuf,&rlen,TWO_SECOND);
			if(strstr((char*)rbuf,"SEND OK"))
			{
				return 0;
			}
			else
			{
				return -2;
			}
		}
	}
	else
	{
		return -1;
	}
}

int ESP8266_TCP_Recv(uint8_t *data,uint16_t *len,uint16_t time)
{
	UART_Recv_Data(data,len,time);
	if(*len)		return 0;
	else			return -1;
}

int ESP8266_TS_Enter(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CIPMODE=1");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	
	return 0;
}

int ESP8266_TS_Exit(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD((uint8_t *)"AT+CIPMODE=0");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,"OK"))
	{
		return -1;
	}
	
	return 0;
}

int ESP8266_TS_EnSend(void)
{
	uint8_t rbuf[100];
	uint16_t rlen;	
	
	memset(rbuf,0x00,sizeof(rbuf));
	UART_Send_CMD("AT+CIPSEND");
	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
	if(!strstr((char *)rbuf,">"))
	{
		return -1;
	}
	
	return 0;
}

int ESP8266_TS_ExSend(void)
{
	HAL_Delay(2000);		// must dealy,otherwise the "+++" will join the previous send data.
	UART_Send_Data("+++",3);
	HAL_Delay(2000);		// must delay, then send cmd.
	return 0;
}

int ESP8266_TS_Send(uint8_t *data,uint16_t len)
{
	UART_Send_Data(data,len);
	return 0;
}

int ESP8266_TS_Recv(uint8_t *data,uint16_t reqlen,uint16_t *len,uint16_t time)
{
	UART_Recv_Data_AWS(data,reqlen,len,time);
//	if(*len)		return 0;
//	else			return -1;
	return 0;
}

void ESP8266_OTA_Recv(uint8_t *data,uint16_t *len,uint16_t time)
{	
	*len = 0;
	BSP_Timer_Start(SYS_TIMER1,time);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(WifiRxBuffQCnt)
		{
			*data = BSP_UART6_RecvChar();
			data++;
			(*len)++;
			if( *len < 500)
			{
				BSP_Timer_Start(SYS_TIMER1,FIVE_SECOND);
			}
			else
			{
				BSP_Timer_Start(SYS_TIMER1,QUARTER_SECOND);
			}
			if((*len)==1550)		break;
		}
	}
}

//int ESP8266_UDP_Link(u8 *ip,u16 port)
//{
//	u8 rbuf[1024];
//	u16 rlen;
//	u8 cmd[100];
//	
//	sprintf((char *)cmd,"AT+CIPSTART=\"UDP\",\"%s\",%d,,1",ip,port);
//	LCD_Write_Str(cmd);
//	UART_Send_CMD(cmd);
//	UART_Recv_Data(rbuf,&rlen,FIVE_SECOND);
//	if(!strstr((char *)rbuf,"CONNECT"))
//	{
//		return -1;
//	}
//	return 0;
//}
//
//int ESP8266_UDP_Unlink(void)
//{
//	u8 rbuf[1024];
//	u16 rlen;	
//	
//	memset(rbuf,0x00,sizeof(rbuf));
//	UART_Send_CMD("AT+CIPCLOSE");
//	UART_Recv_Data(rbuf,&rlen,HALF_SECOND);
//	if(!strstr((char *)rbuf,"CLOSED"))
//	{
//		return -1;
//	}
//	
//	return 0;
//}
//
//
//int ESP8266_UDP_Send(u8 *data,u16 len)
//{
//	u8 rbuf[1024];
//	u16 rlen;
//	u8 cmd[100];
//	
//	sprintf((char *)cmd,"AT+CIPSEND=%d",len);
//	UART_Send_CMD(cmd);
//	UART_Recv_Data(rbuf,&rlen,TWO_SECOND);
//	if(strstr((char *)rbuf,">"))
//	{
//		memset(rbuf,0,sizeof(rbuf));
//		UART_Send_Data(data,len);
//		UART_Recv_DataEx(rbuf,&rlen,TWO_SECOND);
//		if(strstr((char*)rbuf,"SEND OK"))
//		{
//			return 0;
//		}
//		else
//		{
//			UART_Recv_DataEx(rbuf,&rlen,TWO_SECOND);
//			if(strstr((char*)rbuf,"SEND OK"))
//			{
//				return 0;
//			}
//			else
//			{
//				return -2;
//			}
//		}
//	}
//	else
//	{
//		return -1;
//	}
//}
//
//int ESP8266_UDP_Recv(u8 *data,u16 *len,u16 time)
//{
//	UART_Recv_Data(data,len,time);
//	if(*len)		return 0;
//	else			return -1;
//}
