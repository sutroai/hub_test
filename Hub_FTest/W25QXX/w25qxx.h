#ifndef __W25QXX_H
#define __W25QXX_H

#include "stm32f4xx.h"
	
#define	ADDR_INIT_CHECKWORD		0x00000
#define	ADDR_WIFI_SSID			0x00020
#define	ADDR_WIFI_PSK			0x00040
#define	ADDR_DEV_CNT			0x00060
#define	ADDR_DEV_TOKEN			0x00080
#define	ADDR_CH_MASK			0x00240
#define	ADDR_HUB_SN				0x00260

#define	ADDR_DEV_IPTAB			0x20000

   
//W25Q80  ID  0XEF13
//W25Q16  ID  0XEF14
//W25Q32  ID  0XEF15
//W25Q64  ID  0XEF16	
//W25Q128 ID  0XEF17	
//W25Q256 ID  0XEF18
#define W25Q80 	0XEF13 	
#define W25Q16 	0XEF14
#define W25Q32 	0XEF15
#define W25Q64 	0XEF16
#define W25Q128	0XEF17
#define W25Q256 0XEF18

extern uint16_t W25QXX_TYPE;	 

#define	W25QXX_CS 		PBout(14)  		

////////////////////////////////////////////////////////////////////////////////// 
#define W25X_WriteEnable		0x06 
#define W25X_WriteDisable		0x04 
#define W25X_ReadStatusReg1		0x05 
#define W25X_ReadStatusReg2		0x35 
#define W25X_ReadStatusReg3		0x15 
#define W25X_WriteStatusReg1    0x01 
#define W25X_WriteStatusReg2    0x31 
#define W25X_WriteStatusReg3    0x11 
#define W25X_ReadData			0x03 
#define W25X_FastReadData		0x0B 
#define W25X_FastReadDual		0x3B 
#define W25X_PageProgram		0x02 
#define W25X_BlockErase			0xD8 
#define W25X_SectorErase		0x20 
#define W25X_ChipErase			0xC7 
#define W25X_PowerDown			0xB9 
#define W25X_ReleasePowerDown	0xAB 
#define W25X_DeviceID			0xAB 
#define W25X_ManufactDeviceID	0x90 
#define W25X_JedecDeviceID		0x9F 
#define W25X_Enable4ByteAddr    0xB7
#define W25X_Exit4ByteAddr      0xE9

void W25QXX_Init(void);
uint16_t  W25QXX_ReadID(void);  	    	
uint8_t W25QXX_ReadSR(uint8_t regno);             
void W25QXX_4ByteAddr_Enable(void);     
void W25QXX_Write_SR(uint8_t regno,uint8_t sr);   
void W25QXX_Write_Enable(void);  		
void W25QXX_Write_Disable(void);		
void W25QXX_Write_NoCheck(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);
void W25QXX_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t NumByteToRead);   
void W25QXX_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);
void W25QXX_Erase_Chip(void);    	  	
void W25QXX_Erase_Sector(uint32_t Dst_Addr);	
void W25QXX_Wait_Busy(void);           
void W25QXX_PowerDown(void);        	
void W25QXX_WAKEUP(void);

#endif
