#ifndef __FUNC_H
#define __FUNC_H

#include "stm32f4xx.h"
#include "net/ip/uip.h"
#include "aws_iot_config.h"

#define SYS_HW          "1.3A"
#define SYS_SW          "0.3.01"
#define SYS_OS          "contiki-3.0"
#define SYS_AWS         ""

#define FLASH_INIT_CHECKWORD		"CCDD"			
#define DEV_MAX_CNT		20                  			

extern uint8_t g_subg_pair;
extern uint8_t g_token_set;

extern uint8_t g_wifiSSID[32];
extern uint8_t g_wifiPSK[32];
extern uint8_t g_wifiRSSI[10];
extern uint8_t g_token[384];
extern uint32_t g_onBoardingTim;
extern uint8_t g_ch_mask[4];
extern uint8_t g_dev_cnt;
extern int16_t g_dev_index;
extern uip_ipaddr_t g_dev_group[DEV_MAX_CNT];
extern uint8_t g_dev_sleep[DEV_MAX_CNT];
extern uint32_t g_dev_binLen;


void WIFI_AP_Init(void);

int16_t Delay_Wait_Key(void);
int16_t Param_Get(void);
void Param_Config(void);
void Param_Printf(void);
void Param_SetDef(void);
void Param_Reset(void);
int Check_Monitor_ID(const uip_ipaddr_t *sender_addr);
int16_t Search_Monitor_ID(char *rbuf);

void Sys_Reset(void);

#endif

