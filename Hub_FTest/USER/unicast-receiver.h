#ifndef __UNICAST_H
#define __UNICAST_H

#include "stm32f4xx.h"
#include "public.h"
#include "uip.h"

extern struct simple_udp_connection unicast_connection;

extern uint8_t sub1g_recieve_buffer[1024*2];
extern PACKET_STR_TYPEDEF	sub1g_recieve_packet[50];
extern BUFFER_CTRL_PARA_TYPEDEF sub1g_packet_para;

extern uint8_t mqtt_recieve_buffer[1024];
extern PACKET_STR_TYPEDEF	mqtt_recieve_packet[10];
extern BUFFER_CTRL_PARA_TYPEDEF mqtt_packet_para;

extern uint8_t g_ota_state;
extern uint8_t g_pause_aws;

extern uip_ipaddr_t g_dev_addr;

extern uip_ipaddr_t mcast_addr;

extern uint8_t g_resend_flag;
extern uint8_t g_wakeup_flag;
extern uint8_t g_resend_num;
extern uint8_t g_resend_loop;

void Monitor_Quit_OTA(bool result);
void Monitor_Ping_False(void);

#endif

