//*******************************************************************************
//* Company		: svv.io
//* Copyright	: 2018-2028 SVV. All Rights Reserved.
//* File		: debug.c
//* Product		: Sutro Hub
//* Version		: 0.2.6
//* Author		: Frank Wu
//* Date		: 2019/10/10
//* Description	: Common function.
//*
//*******************************************************************************
#include "api.h"
#include "public.h"
#include "main.h"

#define SETENA0		(*((volatile unsigned long *)0xE000E100))
#define CLRENA0		(*((volatile unsigned long *)0xE000E180))

void ShowSETENARegister(void)
{
	printf("SETENA0: 0x%lx \n", SETENA0);
	printf("CLRENA0: 0x%lx \n", CLRENA0);
}

void ShowCpuRegister(void)
{
	uint32_t  *pInt;
	uint32_t i;
	pInt = (uint32_t *)__get_MSP();
	
	printf("StackPointer-0x%x\n\r", pInt);
	printf("Stacked_R0-0x%x\n\r", pInt[0]);
	printf("Stacked_R1-0x%x\n\r", pInt[1]);
	printf("Stacked_R2-0x%x\n\r", pInt[2]);
	printf("Stacked_R3-0x%x\n\r", pInt[3]);
	printf("Stacked_R12-0x%x\n\r", pInt[4]);
	printf("Stacked_LR-0x%x\n\r", pInt[5]);
	printf("Stacked_PC-0x%x\n\r", pInt[6]);
	printf("Stacked_PSR-0x%x\n\r", pInt[7]);
	printf("SCB->SHCSR-0x%x\n\r", SCB->SHCSR);
	printf("SCB->HFSR-0x%x\n\r", SCB->HFSR);
	printf("SCB->DFSR-0x%x\n\r", SCB->DFSR);
	printf("SCB->MMFAR-0x%x\n\r", SCB->MMFAR);
	printf("SCB->BFAR-0x%x\n\r", SCB->BFAR);
	printf("SCB->AFSR-0x%x\n\r\n\r", SCB->AFSR);
	
	for(i=0;i<32;i++){
		printf("Stacked_S%d-0x%x\n\r",8+i, pInt[8+i]);
	}
}

extern void *__initial_sp;
extern void *__heap_base;
extern void *__heap_limit;
unsigned char *stackInit = (unsigned char*) &__initial_sp;
unsigned char *stackStart;
unsigned char *heapBase = (unsigned char*) &__heap_base;
unsigned char *heapLimit = (unsigned char*) &__heap_limit;

void Stack_Heap_stat(void)
{
	unsigned char p;
	stackStart = &p;
	
	printf("stackInit: 0x%X  \n", stackInit);
	printf("stackStart: 0x%X  \n", stackStart);
	printf("heapLimit: 0x%X  \n", heapLimit);
	printf("heapBase: 0x%X  \n", heapBase);
	printf("heapSize: 0x%X  \n", heapLimit - heapBase);
	printf("stackSize: 0x%X  \n", stackInit - heapLimit);
	printf("stackLeft: 0x%X  \n", (stackInit - heapLimit) - (stackInit - stackStart));
}

