#include "api.h"
#include "public.h"
#include "func.h"
#include "esp8266.h"
#include "w25qxx.h" 
#include "ota.h"
#include "factory.h"
#include "net.h"
#include "cloud.h"
#include "R410M.h"
#include "unicast-receiver.h"
#include "radio-config.h"
#include "radio_gpio.h"
#include "process.h"

#include "contiki.h"
#include "net/ip/uip.h"
#include "simple-udp.h"
#include "net/ip/uip-debug.h"

#include "main.h"
//#include "aws_iot_log.h"
//#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"

#include "netstack.h"

#include "aws_iot_version.h"
#include "mbedtls/version.h"

void Stack_6LoWPAN_Init(void);

extern uip_ipaddr_t ipaddr;				// SubG IP
extern unsigned char node_mac[8];		// SubG MAC

RNG_HandleTypeDef hrng;					// RNG Handle definition
net_hnd_t         hnet;					// Network interface handle.

/**
  * @brief  System Clock Configuration
  *         The system Clock is configured as follow : 
  *            System Clock source            = PLL (HSE)
  *            SYSCLK(Hz)                     = 100000000
  *            HCLK(Hz)                       = 100000000
  *            AHB Prescaler                  = 1
  *            APB1 Prescaler                 = 2
  *            APB2 Prescaler                 = 1
  *            HSE Frequency(Hz)              = 8000000
  *            PLL_M                          = 8
  *            PLL_N                          = 200
  *            PLL_P                          = 2
  *            PLL_Q                          = 7
  *            PLL_R                          = 2
  *            VDD(V)                         = 3.3
  *            Main regulator output voltage  = Scale1 mode
  *            Flash Latency(WS)              = 3
  * @param  None
  * @retval None
  */
static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 200;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  RCC_OscInitStruct.PLL.PLLR = 2;
  ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
  
  if(ret != HAL_OK)
  {
    while(1) { ; } 
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3);
  if(ret != HAL_OK)
  {
    while(1) { ; }  
  }
}

/**
  * @brief  Peripheral Clock Configuration. 
  * @param  None
  * @retval None
  */
void Periph_Config(void)
{
	RCC_PeriphCLKInitTypeDef PeriphClkInit;
	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_CLK48;
	PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_HSE_DIV8;
	PeriphClkInit.Clk48ClockSelection = RCC_CLK48CLKSOURCE_PLLQ;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
	{
		Error_Handler();
	}
}

/**
  * @brief  Receives an amount of data on UART1.
  * @param  [out] data: Pointer to data buffer.  
  * @param  [out] len: Amount of data to be received.
  * @retval None
  */
void UART1_Recv_Data(uint8_t *data,uint16_t *len)
{	
	*len = 0;
	
	BSP_Timer_Start(SYS_TIMER1,2);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(G_uart1_num)
		{
			*data = BSP_UART1_RecvChar();
			data++;
			(*len)++;
			BSP_Timer_Start(SYS_TIMER1,2);
		}
	}
}


/**
  * @brief  Get Hub's reset status. 
  * @param  None
  * @retval None
  */
void Get_RST_Status(void)
{
	if(__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST) != RESET)
	{
		printf("RCC_FLAG_PORRST\n");					// Power on reset
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST)!= RESET)
	{
		printf("RCC_FLAG_SFTRST\n");					// Software reset
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST)!= RESET)
	{
		printf("RCC_FLAG_IWDGRST\n");					// IWDG reset
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST)!= RESET)
	{
		printf("RCC_FLAG_WWDGRST\n");					// WWDG reset
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST)!= RESET)
	{
		printf("RCC_FLAG_LPWRRST\n");					// Low power reset.
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST) != RESET)
	{
		printf("RCC_FLAG_PINRST\n");					// Pin reset.
	}	
	__HAL_RCC_CLEAR_RESET_FLAGS();
}



/**
  * @brief  Main program
  * @param  None
  * @retval Status
  */
int main(void)
{
	SCB->VTOR = FLASH_BASE | 0x68000;		// Sets the offset of the interrupt vector
    HAL_Init();
    SystemClock_Config();
	Periph_Config();
	
    BSP_LED_Init();
    BSP_KEY_Init();
    BSP_CHG_Init();
	BSP_UART1_Init();
	W25QXX_Init();
	ESP8266_Module_Close();					// Close ESP
	xxRecieveBuffer_Module_register(&sub1g_packet_para,sub1g_recieve_packet,sizeof(sub1g_recieve_packet)/sizeof(PACKET_STR_TYPEDEF),sub1g_recieve_buffer,sizeof(sub1g_recieve_buffer));
	xxRecieveBuffer_Module_register(&mqtt_packet_para,mqtt_recieve_packet,sizeof(mqtt_recieve_packet)/sizeof(PACKET_STR_TYPEDEF),mqtt_recieve_buffer,sizeof(mqtt_recieve_buffer));
	
	Factory_Test();
}	



