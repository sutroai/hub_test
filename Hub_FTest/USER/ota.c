//*******************************************************************************
//* Company		: svv.io
//* Copyright	: 2018-2028 SVV. All Rights Reserved.
//* File		: ota.c
//* Product		: Sutro Hub
//* Version		: 0.2.6
//* Author		: Frank Wu
//* Date		: 18-September-2018
//* Description	: OTA function.
//*
//*******************************************************************************

#include "api.h"
#include "ota.h"
#include "func.h"
#include "public.h"
#include "esp8266.h"
#include "w25qxx.h"

uint32_t g_file_len;
uint16_t g_head_len;
uint16_t g_cont_len;

/**
  * @brief  Format HTTP send packet.
  * @param  [out] pkt: A pointer to a buffer which will send by HTTP.
  * @param  [in] head: A pointer to a buffer which contain package head info.
  * @param  [in] range: A pointer to a buffer which contain GET range info.
  * @retval packet length.
  */
uint32_t HTTP_Format_Packet(char *pkt,char *head,char *range)
{
	strcat(pkt,head);
	strcat(pkt, "Host: s3-us-west-2.amazonaws.com:80\r\n");
//	strcat(pkt, "Connection: Keep-Alive\r\n");
//	strcat(pkt, "Cache-Control: no-cache\r\n");
//	strcat(pkt, "User-Agent: test-123\r\n");
//	strcat(pkt, "Accept: */*\r\n");
	
	if(range)
	{
		strcat(pkt, range);
	}
	strcat(pkt, "\r\n");
	
	return strlen(pkt);
}

/**
  * @brief  Parse HTTP receive packet.
  * @param  [in] data: Pointer to data buffer.
  * @param  [in] type: Indicate command type.
  * @retval Result of parse packet.
  *   @val  -1:  fail.
  *   @val   0:  success.
  */
int HTTP_Parse_Packet(uint8_t *data,uint8_t type)
{
	char pfile_head_buf[500+2];
	char pfile_clen_buf[40];
	char *pres;
	
	memset(pfile_head_buf,0x00,sizeof(pfile_head_buf));
	memcpy(pfile_head_buf,data,500);
	if(!strstr((char *)data,"\r\n\r\n"))	return -1;
	
	pres = strstr((char *)data,"\r\n\r\n");
	g_head_len = pres - (char *)data + 4;
	printf("head len: %d \n",g_head_len);
	memset(pfile_head_buf,0x00,sizeof(pfile_head_buf));
	memcpy(pfile_head_buf,data,g_head_len);
	
	pres = strtok((char *)pfile_head_buf,"\r\n");
	if((strstr(pres,"HTTP/1.1 200") == NULL) && (strstr(pres,"HTTP/1.1 206") == NULL))		return -1;
	
	while( pres != NULL )
	{	
		//printf("%s \n",pres);
		if(strstr(pres,"Content-Length") != NULL )
		{
			memset(pfile_clen_buf,0x00,sizeof(pfile_clen_buf));
			strcpy(pfile_clen_buf, pres);
		}

		pres = strtok( NULL, "\r\n");
	}
	
	pres = strtok(pfile_clen_buf," ");
	pres = strtok( NULL, "\0");
	
	if(type == HTTP_HEAD)
	{
		g_file_len = atol(pres);
		printf("file len: %ld \n\n",(long)g_file_len);
	}
	else if(type == HTTP_GET)
	{
		g_cont_len = atol(pres);
		printf("cont len: %ld \n\n",(long)g_cont_len);
	}
	return 0;
}

/**
  * @brief  OTA updata via HTTP.
  * @param  [in] bin: file type.
  *   @val  OTA_HTTP_HUB:  Hub's bin file.
  *   @val  OTA_HTTP_DEV:  Monitor's bin file.
  * @param  [in] fileName: file name.
  * @param  [in] crc: file crc code.
  * @retval Result of parse packet.
  *   @val  -1:  fail.
  *   @val   0:  success.
  */
int HTTP_OTA_Update(uint8_t bin,uint8_t *fileName,uint16_t crc)
{
	uint8_t range[30];
	uint8_t sbuf[200];
	uint8_t head[100];
	uint16_t slen;
	uint8_t rbuf[1600];
	uint16_t rlen;
	int ret = 0;
	uint16_t blocks = 0;
	uint16_t remain = 0;
		
	printf("\n*** HTTP OTA Update Start ***\n");
	printf("Connect to HTTP Server... ");
	ret = ESP8266_TCP_Link((uint8_t *)"s3-us-west-2.amazonaws.com",80);
	printf("%d \n",ret);

	HAL_Delay(1000);
	ESP8266_TS_Enter();
	ESP8266_TS_EnSend();
	
	memset(sbuf,0x00,sizeof(sbuf));
	sprintf((char *)head,"HEAD /sutro-firmware/%s HTTP/1.1\r\n",fileName);
	slen = HTTP_Format_Packet((char *)sbuf, (char *)head, NULL);
	
	ESP8266_TS_Send(sbuf,slen);
	printf("Send CMD... %d \n", slen);
	memset(rbuf,0x00,sizeof(rbuf));
	ESP8266_OTA_Recv(rbuf,&rlen,EIGHT_SECOND);
	printf("Recv Data... %d \n",rlen);
//	printf("%s",rbuf);
	if(HTTP_Parse_Packet(rbuf,HTTP_HEAD))		
	{
		ESP8266_TS_ExSend();
		ESP8266_TS_Exit();
		ESP8266_TCP_Unlink();
		printf("*** HTTP OTA Update Fault ***\n\n");
		return -1;
	}
	
	blocks = g_file_len/HTTP_BLOCK_SIZE;
	remain = g_file_len%HTTP_BLOCK_SIZE;
	if(remain)		blocks++;
	for(uint16_t i = 0; i < blocks; i++)
	{
		IWDG_Feed();
		
		if(i == blocks - 1)
		{
			sprintf((char *)range,"Range: bytes=%d-%d\r\n", i*HTTP_BLOCK_SIZE, i*HTTP_BLOCK_SIZE + remain - 1);
		}
		else
		{
			sprintf((char *)range,"Range: bytes=%d-%d\r\n",i*HTTP_BLOCK_SIZE,(i+1)*HTTP_BLOCK_SIZE - 1);
		}
		memset(sbuf,0x00,sizeof(sbuf));
		sprintf((char *)head,"GET /sutro-firmware/%s HTTP/1.1\r\n",fileName);
		slen = HTTP_Format_Packet((char *)sbuf, (char *)head, (char *)range);	
		
		for(uint8_t j = 0; j < 6; j++)
		{
			IWDG_Feed();
			
			if(j == 5)
			{
				ESP8266_TS_ExSend();
				ESP8266_TS_Exit();
				ESP8266_TCP_Unlink();
				printf("*** HTTP OTA Update Fault ***\n\n");
				return -1;
			}
			
			BSP_UART6_FlushFIFO();
			printf("send block: %d/%d \n", i + 1, blocks);
			ESP8266_TS_Send(sbuf,slen);
			
			memset(rbuf,0x00,sizeof(rbuf));
			ESP8266_OTA_Recv(rbuf,&rlen,EIGHT_SECOND);
			printf("recv data: %d \n",rlen);
			if(rlen == 0)
			{
				HAL_Delay(3000);
				continue;
			}
			else if(((rlen < 1480) || (rlen > 1520) ) && (i!=blocks - 1) )
			{
				HAL_Delay(3000);
				continue;
			}
			if(HTTP_Parse_Packet(rbuf,HTTP_GET))
			{
				ESP8266_TS_ExSend();
				ESP8266_TS_Exit();
				ESP8266_TCP_Unlink();
				printf("*** HTTP OTA Update Fault ***\n\n");
				return -1;
			}
			if(rlen != g_head_len + g_cont_len)
			{
				HAL_Delay(3000);
				continue;
			}
			else
			{
				break;
			}
		}
		
		//printf("%s\n\n",rbuf);
		if(bin == OTA_HTTP_HUB)
		{
			W25QXX_Write(rbuf + g_head_len, OTA_HUBBIN_ADDR + i * HTTP_BLOCK_SIZE, g_cont_len);
		}
		else if(bin == OTA_HTTP_DEV)
		{
			W25QXX_Write(rbuf + g_head_len, OTA_DEVBIN_ADDR + i * HTTP_BLOCK_SIZE, g_cont_len);
		}
	}
	
	// Check CRC
	unsigned short crc_val = 0;
	for(uint16_t i = 0; i < blocks; i++)
	{
		memset(rbuf,0x00,sizeof(rbuf));
		if(bin == OTA_HTTP_HUB)
		{
			if(i == blocks - 1)
			{
				W25QXX_Read(rbuf, OTA_HUBBIN_ADDR + i * HTTP_BLOCK_SIZE, remain);
				crc_val = do_crc16(crc_val,rbuf,remain);
			}
			else
			{
				W25QXX_Read(rbuf, OTA_HUBBIN_ADDR + i * HTTP_BLOCK_SIZE, HTTP_BLOCK_SIZE);
				crc_val = do_crc16(crc_val,rbuf,HTTP_BLOCK_SIZE);
			}
		}
		else if(bin == OTA_HTTP_DEV)
		{
			if(i == blocks - 1)
			{
				W25QXX_Read(rbuf, OTA_DEVBIN_ADDR + i * HTTP_BLOCK_SIZE, remain);
				crc_val = do_crc16(crc_val,rbuf,remain);
			}
			else
			{
				W25QXX_Read(rbuf, OTA_DEVBIN_ADDR + i * HTTP_BLOCK_SIZE, HTTP_BLOCK_SIZE);
				crc_val = do_crc16(crc_val,rbuf,HTTP_BLOCK_SIZE);
			}
		}
	}
	printf("crc: %04X \n",crc_val);
	if(crc != crc_val)
	{
		ESP8266_TS_ExSend();
		ESP8266_TS_Exit();
		ESP8266_TCP_Unlink();
		printf("*** HTTP OTA Update Fault ***\n\n");
		return -1;
	}
	
	if(bin == OTA_HTTP_HUB)
	{
		W25QXX_Write((uint8_t*)&g_file_len, OTA_HUBLEN_ADDR, 4);
	}
	else if(bin == OTA_HTTP_DEV)
	{
		W25QXX_Write((uint8_t*)&g_file_len, OTA_DEVLEN_ADDR, 4);
		g_dev_binLen = g_file_len;
	}
	
	ESP8266_TS_ExSend();
	ESP8266_TS_Exit();
	ret = ESP8266_TCP_Unlink();
	printf("HTTP Disconnect... %d\n",ret);
	printf("*** HTTP OTA Update Complete ***\n\n");
	return 0;
}

/**
  * @brief  Upload bin file in flash to UART1. Used for test to check downloaded file.
  * @param  [in] bin: file type.
  *   @val  OTA_HTTP_HUB:  Hub's bin file.
  *   @val  OTA_HTTP_DEV:  Monitor's bin file.
  * @retval None
  */
void check_with_flash(uint8_t bin)
{
	uint8_t rbuf[1200];
	uint16_t blocks = 0;
	uint16_t remain = 0;
	
	blocks = g_file_len/HTTP_BLOCK_SIZE;
	remain = g_file_len%HTTP_BLOCK_SIZE;
	if(remain)		blocks++;
	for(uint16_t i = 0; i < blocks; i++)
	{
		memset(rbuf,0x00,sizeof(rbuf));
		if(i == blocks - 1)
		{
			if(bin == OTA_HTTP_HUB)
			{
				W25QXX_Read(rbuf, OTA_HUBBIN_ADDR + i * HTTP_BLOCK_SIZE, remain);
			}
			else if(bin == OTA_HTTP_DEV)
			{
				W25QXX_Read(rbuf, OTA_DEVBIN_ADDR + i * HTTP_BLOCK_SIZE, remain);
			}
			BSP_UART1_SendStr(rbuf,remain);
		}
		else
		{
			if(bin == OTA_HTTP_HUB)
			{
				W25QXX_Read(rbuf, OTA_HUBBIN_ADDR + i * HTTP_BLOCK_SIZE, HTTP_BLOCK_SIZE);
			}
			else if(bin == OTA_HTTP_DEV)
			{
				W25QXX_Read(rbuf, OTA_DEVBIN_ADDR + i * HTTP_BLOCK_SIZE, HTTP_BLOCK_SIZE);
			}
			BSP_UART1_SendStr(rbuf,HTTP_BLOCK_SIZE);
		}
	}
}

