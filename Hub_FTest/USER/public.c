//*******************************************************************************
//* Company		: svv.io
//* Copyright	: 2018-2028 SVV. All Rights Reserved.
//* File		: public.c
//* Product		: Sutro Hub
//* Version		: 0.2.6
//* Author		: Frank Wu
//* Date		: 14-September-2018
//* Description	: Common function.
//*
//*******************************************************************************

#include "api.h"
#include "public.h"
#include "main.h"
#include "w25qxx.h"
#include "aws_iot_mqtt_client_interface.h"

/**
  * @brief  Letter transform between upper and lower case
  * @param  [io] str: Input lower output upper and input upper output lower.
  * @retval None
  */
void Letter_Toggle(uint8_t *str)
{
	int i;
	
    for(i=0;str[i]!='\0';i++)
    {
        if((str[i]>='A'&&str[i]<='Z')||(str[i]>='a'&&str[i]<='z'))
        str[i]^=32;
    }
}

/**
  * @brief  Calculate CRC code.
  * @param  [in] crc_val: Initial value.
  * @param  [in] message: Pointer to a message to be CRC calculate.
  * @param  [in] len: Message length.
  * @retval [out] Result value.
  */
unsigned short do_crc16(unsigned short crc_val,unsigned char *message, unsigned int len)
{
    unsigned int i, j;
    unsigned short crc_reg = crc_val;
    unsigned short current = 0;
       
    for (i = 0; i < len; i++)
    {
        current = message[i] << 8;
        for (j = 0; j < 8; j++)
        {
            if ((short)(crc_reg ^ current) < 0)
                crc_reg = (crc_reg << 1) ^ 0x1021;
            else
                crc_reg <<= 1;
            current <<= 1;           
        }
    }
    return crc_reg;
}



/**
  * @brief  Receive buffer register(Initial chair table parameter).
  * @param  [out] pBuffer_para: Pointer to a BUFFER_CTRL_PARA_TYPEDEF structure that contains the configuration information for the chair table.
  * @param  [in] pRecievePacket: Pointer to a BUFFER_CTRL_PARA_TYPEDEF structure that contains the packet information.
  * @param  [in] recievePacket_size: Amount of pRecievePacket.
  * @param  [in] pBuffer: Pointer to a data buffer.
  * @param  [in] buffer_size: Size of data buffer.
  * @retval None
  */
void xxRecieveBuffer_Module_register(BUFFER_CTRL_PARA_TYPEDEF *pBuffer_para,PACKET_STR_TYPEDEF *pRecievePacket,uint8_t recievePacket_size,uint8_t *pBuffer,uint16_t buffer_size)
{
	uint8_t i;

//	serialPort_RxBuffer = pu8Test_Memory_Allocation(SERIALPORT_BUFFER_SIZE);//Sub1gSendBuf;

	(*pBuffer_para).rxBuf_shadow.ldle_sta = (uint8_t *)pBuffer;
	(*pBuffer_para).rxBuf_shadow.ldle_end = (uint8_t *)&pBuffer[buffer_size-1];
	(*pBuffer_para).rxBuf.ldle_sta = (uint8_t *)pBuffer;
	(*pBuffer_para).rxBuf.ldle_end = (uint8_t *)&pBuffer[buffer_size-1];	

	(*pBuffer_para).rxBuf_border.sta = (uint8_t *)pBuffer;
	(*pBuffer_para).rxBuf_border.end = (uint8_t *)&pBuffer[buffer_size-1];

	(*pBuffer_para).packet_head = NULL;
	
	(*pBuffer_para).pPacket = pRecievePacket;
	(*pBuffer_para).recievePacket_max = recievePacket_size;

	for(i = 0; i < (*pBuffer_para).recievePacket_max; i++)
	{
		(*pBuffer_para).pPacket[i].sta = NULL;
		(*pBuffer_para).pPacket[i].end = NULL;
		(*pBuffer_para).pPacket[i].next= NULL; 
	}
}

/**
  * @brief  Fetch idle packet pointer in recieve packets.
  * @param  [io] pBuffer_para: Pointer to a BUFFER_CTRL_PARA_TYPEDEF structure that contains the configuration information for the chair table.
  * @retval Idle packet pointer.
  */
static PACKET_STR_TYPEDEF *pxxRecieveBuffer_Fetch_Idle_Packet_Pointer(BUFFER_CTRL_PARA_TYPEDEF *pBuffer_para)
{
	uint8_t i;

	for(i = 0; i < (*pBuffer_para).recievePacket_max; i++)
	{
		if((*pBuffer_para).pPacket[i].sta == NULL)
		{
			return (&(*pBuffer_para).pPacket[i]);
		}
	}	
	
	return NULL;
}

/**
  * @brief  Read one packet form chain table.
  * @param  [io] pBuffer_para: Pointer to a BUFFER_CTRL_PARA_TYPEDEF structure that contains the configuration information for the chair table.
  * @param  [out] pDataOut: Pointer to a data buffer.
  * @param  [in] dataOutBufMax: Limits maximum of bytes received.
  * @param  [out] pSize: The number of bytes actully received.
  * @retval Result of read chain.
  *   @val  -1:  fail.
  *   @val   0:  success.
  */
int8_t i8xxRecieveBuffer_Read_Period(BUFFER_CTRL_PARA_TYPEDEF *pBuffer_para,uint8_t *pDataOut, uint16_t dataOutBufMax, uint16_t *pSize)
{
	PACKET_STR_TYPEDEF *packet_next;
	uint16_t size = 0;

	packet_next = (*pBuffer_para).packet_head;

	if(((*pBuffer_para).packet_head == NULL) || (packet_next->sta == NULL))
	{
		return (-1);
	}
	else
	{
		while(1)
		{
			size++;
		
			*pDataOut = *packet_next->sta;
			
			if(packet_next->sta == packet_next->end)
			{
				break;
			}
			else
			{
				pDataOut++;
				packet_next->sta++;
			
				if(packet_next->sta > (*pBuffer_para).rxBuf_border.end)
				{
					packet_next->sta = (*pBuffer_para).rxBuf_border.sta;
				}
			
				dataOutBufMax--;
				if(dataOutBufMax == 0)
				{
					break;
				}
			}
		}			

		*pSize = size;
						
		(*pBuffer_para).rxBuf.ldle_end = packet_next->end;
		(*pBuffer_para).rxBuf_shadow.ldle_end = (*pBuffer_para).rxBuf.ldle_end;
		(*pBuffer_para).packet_head = packet_next->next;
		
		packet_next->sta = NULL;
		packet_next->end = NULL;
		packet_next->next= NULL;
						
		return (0);
	}
}

/**
  * @brief  Push one packet to chain table.
  * @param  [io] pBuffer_para: Pointer to a BUFFER_CTRL_PARA_TYPEDEF structure that contains the configuration information for the chair table.
  * @param  [in] data: Pointer to a data buffer.
  * @param  [in] size: The number of bytes to be pushed.
  * @retval None
  */
void i8SxxRecieveBuffer_Push_Data(BUFFER_CTRL_PARA_TYPEDEF *pBuffer_para, uint8_t const *data, uint16_t size)
{				
	uint8_t 	i = 0;
	uint16_t 	j = 0;

	PACKET_STR_TYPEDEF *packet_next;
	PACKET_STR_TYPEDEF *packet_pre;

	packet_next = (*pBuffer_para).packet_head;
	packet_pre  = packet_next;

	for(i = 0; i < (*pBuffer_para).recievePacket_max; i++)
	{	
		if(packet_next == NULL)
		{
			packet_next = pxxRecieveBuffer_Fetch_Idle_Packet_Pointer(pBuffer_para);
			if(packet_next)
			{
				if((*pBuffer_para).rxBuf.ldle_sta != (*pBuffer_para).rxBuf.ldle_end)
				{
					if(packet_pre == NULL)
					{
						(*pBuffer_para).packet_head = packet_next;
					}
					else
					{
						packet_pre->next = packet_next;
					}
			
					packet_next->sta = (*pBuffer_para).rxBuf.ldle_sta;
				}
				else
				{
					break;
				}
			
				for(j = 0; j < size; j++)
				{
					if((*pBuffer_para).rxBuf.ldle_sta != (*pBuffer_para).rxBuf.ldle_end)
					{
						*(*pBuffer_para).rxBuf.ldle_sta = *(data + j);
						packet_next->end	= (*pBuffer_para).rxBuf.ldle_sta;

						(*pBuffer_para).rxBuf.ldle_sta = ((*pBuffer_para).rxBuf.ldle_sta + 1);
						
						if((*pBuffer_para).rxBuf.ldle_sta > (*pBuffer_para).rxBuf_border.end)
						{
							(*pBuffer_para).rxBuf.ldle_sta = (*pBuffer_para).rxBuf_border.sta;
						}
					}
					else
					{
						break;
					}
				}
				
				(*pBuffer_para).rxBuf_shadow.ldle_sta = (*pBuffer_para).rxBuf.ldle_sta;
				(*pBuffer_para).rxBuf_shadow.ldle_end = (*pBuffer_para).rxBuf.ldle_end;
				
				break;
			}
		}
		else
		{
			packet_pre 	= packet_next;
			packet_next = packet_next->next;
		}
	}
}

