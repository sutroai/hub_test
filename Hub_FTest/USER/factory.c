//*******************************************************************************
//* Company		: svv.io
//* Copyright	: 2018-2028 SVV. All Rights Reserved.
//* File		: factory.c
//* Product		: Sutro Hub
//* Version		: 0.2.6
//* Author		: Frank Wu
//* Date		: 19-September-2018
//* Description	: Factory test function.
//*
//*******************************************************************************
#include "api.h"
#include "func.h"
#include "public.h"
#include "esp8266.h"
#include "w25qxx.h" 
#include "ota.h"
#include "net.h"
#include "cloud.h"
#include "R410M.h"
#include "unicast-receiver.h"
#include "radio-config.h"
#include "radio_gpio.h"
#include "process.h"

#include "contiki.h"
#include "net/ip/uip.h"
#include "simple-udp.h"
#include "net/ip/uip-debug.h"

#include "st-lib.h"

void Stack_6LoWPAN_Init(void);

uint8_t g_ft = 0;
uint8_t g_tkey_pair = 0;
uint8_t g_tkey_config = 0;
uint8_t g_subG_acPair = 0;

#define		APP_VER_ADDR		0x08067C00

extern uip_ipaddr_t ipaddr;

uint8_t F412_FLASH_Read(uint8_t *src, uint8_t *dest, uint32_t Len)
{
    /* Return a valid address to avoid HardFault */
	uint32_t i = 0;
    uint8_t *psrc = src;
 
    for(i = 0; i < Len; i++)
    {
        dest[i] = *psrc++;
    }
    return HAL_OK;
}

void ft_receiver(struct simple_udp_connection *c,
				const uip_ipaddr_t *sender_addr,
				uint16_t sender_port,
				const uip_ipaddr_t *receiver_addr,
				uint16_t receiver_port,
				const uint8_t *data,
				uint16_t datalen)
{
	int rssi;
	char *pres;
	uint8_t hubSN[32];
	uint16_t id;
	uint8_t ack[60];
	
	if(strstr((char *)data,"\"CMD\":PAIRING"))
	{
		if(g_subg_pair)
		{
			printf("Data received from ");
			uip_debug_ipaddr_print(sender_addr);
			printf("\r\n%.*s \n", datalen, data);
			
			sprintf((char *)ack, "{\"ACK\":PAIRING}");
			printf("--> %s \n",ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
	}
	else if(strstr((char *)data,"\"ACK\":PAIRING"))
	{
		if(g_subG_acPair)
		{
			g_subG_acPair = 0;
			printf("Data received from ");
			uip_debug_ipaddr_print(sender_addr);
			printf("\n%.*s\n", datalen, data);
			memcpy(&g_dev_addr, sender_addr, sizeof(uip_ipaddr_t));
			BSP_LED_ON(BSP_LED_ID_BLUE);
			HAL_Delay(1000);
			BSP_LED_OFF(BSP_LED_ID_BLUE);
		}
	}
	else if(!memcmp(&g_dev_addr, sender_addr, sizeof(uip_ipaddr_t)))
	{
		printf("Data received from ");
		uip_debug_ipaddr_print(sender_addr);
		printf("\n%.*s\n", datalen, data);	
		
		if(strstr((char *)data,"WSN"))
		{
			pres = strtok((char *)data,"\"");
			pres = strtok((char *)'\0',"\"");
			memset(hubSN,0x00,sizeof(hubSN));
			strcat((char *)hubSN,pres);
			W25QXX_Write(hubSN, ADDR_HUB_SN, 32);
			sprintf((char *)ack,"TACK+WSN");
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);		
		}
		else if(strstr((char *)data,"RSN"))
		{
			memset(hubSN,0x00,sizeof(hubSN));
			W25QXX_Read(hubSN, ADDR_HUB_SN, 32);
			sprintf((char *)ack,"TACK+RSN=\"%s\"",hubSN);
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"FWVER"))
		{
			uint8_t ver[20];
			
			F412_FLASH_Read((uint8_t*)(APP_VER_ADDR),ver,10);
			sprintf((char *)ack,"TACK+FWVER=\"V%s\",\"V%s\"",ver,SYS_SW);
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"ONLED"))
		{
			if(strstr((char *)data,"=R"))
			{
				BSP_LED_ON(BSP_LED_ID_RED);
			}
			else if(strstr((char *)data,"=G"))
			{
				BSP_LED_ON(BSP_LED_ID_GREEN);
			}
			else if(strstr((char *)data,"=B"))
			{
				BSP_LED_ON(BSP_LED_ID_BLUE);
			}
			sprintf((char *)ack,"TACK+ONLED");
			printf("-->  %s \n\n", ack);	
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);	
		}
		else if(strstr((char *)data,"OFFLED"))
		{
			BSP_LED_OFF(BSP_LED_ID_ALL);
			sprintf((char *)ack, "TACK+OFFLED");
			printf("-->  %s \n\n", ack);	
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);			
		}
		else if(strstr((char *)data,"FLASH"))
		{
			id = W25QXX_ReadID();
			if(id == 0xEF13)
			{
				sprintf((char *)ack, "TACK+FLASH=\"W25Q80\"");
			}
			else
			{
				sprintf((char *)ack, "TACK+FLASH=\"UNKNOWN\"");
			}
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"SUBG"))
		{
			rssi = st_lib_radio_qi_get_rssi_dbm();
			sprintf((char *)ack, "TACK+SUBG=\"%d\"",rssi);
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"HUBID"))
		{
			sprintf((char *)ack, "TACK+HUBID=\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"",
									ipaddr.u8[8],ipaddr.u8[9],ipaddr.u8[10],ipaddr.u8[11],
									ipaddr.u8[12],ipaddr.u8[13],ipaddr.u8[14],ipaddr.u8[15]);
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"KEY=PAIR"))
		{
			g_tkey_pair = 1;
		}
		else if(strstr((char *)data,"KEY=CONFIG"))
		{
			g_tkey_config = 1;
		}
		else if(strstr((char *)data,"CHG"))
		{
			if(g_CHG_STAT == 0)
			{
				sprintf((char *)ack, "TACK+CHG=\"noBattery\"");
			}
			else if(g_CHG_STAT == 1)
			{
				sprintf((char *)ack, "TACK+CHG=\"charging\"");
			}
			else if(g_CHG_STAT == 2)
			{
				sprintf((char *)ack, "TACK+CHG=\"complete\"");
			}
			
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"WIFI=ON"))
		{
			uint8_t mac[20];

			ESP8266_Module_Init();
			ESP8266_Set_CWMODE(ESP_MODE_SAPSTA);
			HAL_Delay(200);
			ESP8266_Get_APMAC(mac);
			Letter_Toggle(mac);
			sprintf((char *)ack, "TACK+WIFI=\"%s\"",mac);
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
			ESP8266_Set_CWSAP(mac, NULL);
			ESP8266_Set_CIPAP((uint8_t *)"192.168.1.1");
		}
		else if(strstr((char *)data,"WIFI=OFF"))
		{
			ESP8266_Module_Close();
			sprintf((char *)ack, "TACK+WIFI");
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"LTE=SIM"))
		{
			uint8_t ccid[20];
			
			if(R410M_Module_Init())
			{
				sprintf((char *)ack, "TACK+LTE=\"initError\"");
			}
			else 
			{
				if(R410M_Get_CCID(ccid))
				{
					sprintf((char *)ack, "TACK+LTE=\"simError\"");
				}
				else
				{
					sprintf((char *)ack, "TACK+LTE=\"%s\"",ccid);
				}
			}
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"LTE=CSQ"))
		{
			uint8_t ccid[20];
			uint8_t csq[20];
			
			if(R410M_Module_Init())
			{
				sprintf((char *)ack, "TACK+LTE=\"initError\"");
			}
			else 
			{
				if(R410M_Get_CCID(ccid))
				{
					sprintf((char *)ack, "TACK+LTE=\"simError\"");
				}
				else
				{
					sprintf((char *)ack, "TACK+LTE=\"%s\"",ccid);
					for(uint8_t i = 0; i < 60; i++)
					{
						HAL_Delay(3000);
						if(!R410M_Get_CGATT())
						{
							break;
						}
					}
					R410M_Get_CSQ(csq);
					sprintf((char *)ack+strlen((char *)ack), ",\"%s\"", csq);
				}
			}
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
		}
		else if(strstr((char *)data,"LTE=OFF"))
		{
			sprintf((char *)ack, "TACK+LTE");
			printf("-->  %s \n\n", ack);
			simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
			R410M_Module_PWROFF();
		}
	}
}

/*---------------------------------------------------------------------------*/
PROCESS(ft_process, "Factory test process");

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(ft_process, ev, data)
{
	static struct etimer et;
	static uint8_t key;
	uint8_t ack[60];
	
	PROCESS_BEGIN();
	
	while(1)
	{
		key = BSP_KEY_Read();
		if(key == USER_BUTTON1)
		{
			if(g_tkey_config)
			{
				g_tkey_config = 0;
				sprintf((char *)ack, "TACK+KEY=CONFIG");
				printf("-->  %s \n\n", ack);
				simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
			}
			else
			{
				BSP_LED_ON(BSP_LED_ID_BLUE);
				g_subg_pair = 1;
				G_subg_pair_time = HAL_GetTick() + 15000;
			}
		}
		else if(key == USER_BUTTON2)
		{
			if(g_tkey_pair)
			{
				g_tkey_pair = 0;
				sprintf((char *)ack, "TACK+KEY=PAIR");
				printf("-->  %s \n\n", ack);
				simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
			}
			else
			{
				g_subG_acPair = 1;
				sprintf((char *)ack, "{\"CMD\":PAIRING}");
				printf("-->  %s \n\n", ack);
				simple_udp_sendto(&unicast_connection, ack, strlen((char *)ack), &mcast_addr);
			}
		}
		
		etimer_set(&et, CLOCK_SECOND/100);
		PROCESS_WAIT_EVENT_UNTIL( ev == PROCESS_EVENT_TIMER);
	}

	PROCESS_END();
}


void Factory_Test(void)
{
	printf("*************************************************************\n");
	printf("***   Sutro Hub Factory Test                           \n");
	printf("***   FW Version %s    %s  %s  \n",SYS_SW,__DATE__,__TIME__);
	printf("*************************************************************\n");
	BSP_LED_ON(BSP_LED_ID_GREEN);
	HAL_Delay(5000);
	BSP_LED_OFF(BSP_LED_ID_GREEN);
	
	g_ft = 1;
    Stack_6LoWPAN_Init();
	// +TI-PA
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_0, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_VDD});
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_1, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_RX_STATE});
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_2, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_TX_STATE});
	
	process_start(&ft_process, NULL);
	
	while(1) {
      int r = 0;
      do {
        r = process_run();
      } while(r > 0);
    }	
}

void S2LPInterfaceInit(void)
{ 
  /* Initialize the SDN pin micro side */
  RadioGpioInit(RADIO_GPIO_SDN,RADIO_MODE_GPIO_OUT);
  
  S2LPSpiInit();
  
  //EepromSpiInitialization();
  
  /* S2LP ON */
  S2LPEnterShutdown();
  S2LPExitShutdown();  
  
  //S2LPManagementIdentificationRFBoard();
  
  /* if the board has eeprom, we can compensate the offset calling S2LPManagementGetOffset
  (if eeprom is not present this fcn will return 0) */
  //xRadioInit.lFrequencyBase = xRadioInit.lFrequencyBase + S2LPManagementGetOffset();
  
  /* if needed this will set the range extender pins */
  //S2LPManagementRangeExtInit();
  
#if 0 //indar
  /* if needed this will set the EXT_REF bit of the S2-LP */
  S2LPManagementTcxoInit();
#endif  
  /* uC IRQ config */       
  RadioGpioInit(RADIO_GPIO_3,RADIO_MODE_EXTI_IN);
  // M2S_GPIO_PIN_IRQ=SdkEvalGpioGetPin(M2S_GPIO_3);
  
  /* S2LP IRQ config */
  // S2LPGpioInit(&xGpioIRQ); 
  
  /* uC IRQ enable */
  RadioGpioInterruptCmd(RADIO_GPIO_3,0x03,0,ENABLE);  
  
}

void P2P_Init(void)
{
	SRadioInit xRadioInit = {
							  BASE_FREQUENCY, 
							  MOD_NO_MOD,
							  DATARATE,
							  FREQ_DEVIATION,
							  BANDWIDTH
							};	
	
	/* S2LP Radio config */    
	S2LPRadioInit(&xRadioInit);

	/* S2LP Radio set power */
	S2LPRadioSetMaxPALevel(S_DISABLE);  

	// Set PA
	{ 
		/* in case we are using the PA board, the S2LPRadioSetPALeveldBm will be not functioning because
		the output power is affected by the amplification of this external component.
		Set the raw register. */
		
		uint8_t paLevelValue=0x27; /* for example, this value will give 23dBm about */
		S2LPSpiWriteRegisters(PA_POWER8_ADDR, 1, &paLevelValue);
	}

	S2LPRadioSetPALevelMaxIndex(POWER_INDEX);
}

void RF_Test(void)
{
	BSP_Timer_Start(SYS_TIMER1,TWO_SECOND);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(!(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_4) == GPIO_PIN_RESET))
		{
			return;
		}
	}
	
	printf("*************************************************************\n");
	printf("***   Sutro Hub RF Test                           \n");
	printf("***   FW Version %s    %s  %s  \n",SYS_SW,__DATE__,__TIME__);
	printf("*************************************************************\n");
	BSP_LED_ON(BSP_LED_ID_BLUE);
	
	S2LPInterfaceInit();
	// +TI-PA
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_0, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_VDD});
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_1, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_RX_STATE});
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_2, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_TX_STATE});
	P2P_Init();
	
	S2LPCmdStrobeTx();
	while(1);
}
