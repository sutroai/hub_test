//*******************************************************************************
//* Company		: svv.io
//* Copyright	: 2018-2028 SVV. All Rights Reserved.
//* File		: func.c
//* Product		: Sutro Hub
//* Version		: 0.2.6
//* Author		: Frank Wu
//* Date		: 12-September-2018
//* Description	: Application function.
//*
//*******************************************************************************
#include "api.h"
#include "public.h"
#include "func.h"
#include "w25qxx.h"
#include "net/ip/uip-debug.h"
#include "esp8266.h"

#include "main.h"
#include "aws_iot_mqtt_client_interface.h"

#include "ota.h"

uint8_t g_subg_pair = 0;                    // Pairing mode flag
uint8_t g_token_set = 0;					// Token configured flag

uint8_t g_wifiSSID[32];						// WIFI SSID
uint8_t g_wifiPSK[32];						// WIFI password
uint8_t g_wifiRSSI[10];						// WIFI RSSI
uint8_t g_token[384];						// Token
uint32_t g_onBoardingTim;					// Onboarding time
uint8_t g_ch_mask[4];						// Channel selection mask
uint8_t g_dev_cnt;							// Number of monitors managed by Hub
int16_t g_dev_index;						// Current Moninor index in the list
uip_ipaddr_t g_dev_group[DEV_MAX_CNT];		// Monitor list
uint8_t g_dev_sleep[DEV_MAX_CNT];			// Monitor sleep list
uint32_t g_dev_binLen;						// Monitor's bin file size

/**
  * @brief  Create a "Sutro_xxxxx" AP for TCP connect.
  * @param  None
  * @retval None
  */
void WIFI_AP_Init(void)
{
    uint8_t mac[20];

	ESP8266_Module_Init();
	ESP8266_Set_CWMODE(ESP_MODE_SAPSTA);
	ESP8266_Get_APMAC(mac);
	Letter_Toggle(mac);
	ESP8266_Set_CWSAP(mac, NULL);
	printf("MAC: %s \n",mac);
}

/**
  * @brief  Waiting for button to be pushed or timeout.
  * @param  None
  * @retval Return type.
  *   @val  1:  key return.
  *   @val  0:  timeout return.
  */
int16_t Delay_Wait_Key(void)
{
	printf("Push User button[SW2] within the next 5 seconds to enter parameter configuration mode. \n\n");
	BSP_Timer_Start(SYS_TIMER1,FIVE_SECOND);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(BSP_KEY_Read() == USER_BUTTON1)
		{
			return 1;
		}
	}
	return 0;
}


/**
  * @brief  Print current parameter.
  * @param  None
  * @retval None
  */
void Param_Printf(void)
{
	printf("SSID: %s \n",g_wifiSSID);
	printf("PSK:  %s \n",g_wifiPSK);
    printf("Token: %s \n",g_token);
	printf("DevCN: %d \n",g_dev_cnt);
	for(uint8_t i=0;i<g_dev_cnt;i++)
	{
        printf("DevIP: %02x%02x:%02x%02x:%02x%02x:%02x%02x\n", 
        g_dev_group[i].u8[8],g_dev_group[i].u8[9],g_dev_group[i].u8[10],g_dev_group[i].u8[11],
        g_dev_group[i].u8[12],g_dev_group[i].u8[13],g_dev_group[i].u8[14],g_dev_group[i].u8[15]);
	}
	printf("CH ON/OFF:  subG = %d, uart = %d,  wifi = %d, lte = %d \n\n",g_ch_mask[0],g_ch_mask[1],g_ch_mask[2],g_ch_mask[3]);
}

/**
  * @brief  Set default parameter.
  * @param  None
  * @retval None
  */
void Param_SetDef(void)
{
	memset(g_wifiSSID, 0x00, sizeof(g_wifiSSID));
	memset(g_wifiPSK, 0x00, sizeof(g_wifiPSK));
    memset(g_token, 0x00, sizeof(g_token));
	g_ch_mask[0] = 0x01;		// subG
	g_ch_mask[1] = 0x00;		// uart
	g_ch_mask[2] = 0x01;		// wifi
	g_ch_mask[3] = 0x00;		// lte
	g_dev_cnt = 0x00;

	W25QXX_Write((uint8_t *)FLASH_INIT_CHECKWORD, ADDR_INIT_CHECKWORD, 4);
	W25QXX_Write(g_wifiSSID, ADDR_WIFI_SSID, 32);
	W25QXX_Write(g_wifiPSK, ADDR_WIFI_PSK, 32);
	W25QXX_Write(&g_dev_cnt, ADDR_DEV_CNT, 1);
    W25QXX_Write(g_token, ADDR_DEV_TOKEN, 384);
	W25QXX_Write(g_ch_mask, ADDR_CH_MASK, 4);
}

/**
  * @brief  Determine set default parameter.
  * @param  None
  * @retval None
  */
void Param_Reset(void)
{
	BSP_Timer_Start(SYS_TIMER1,TWO_SECOND);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(!(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_5) == GPIO_PIN_RESET))
		{
			return;
		}
	}
	Param_SetDef();
	printf("Setting default parameters! \n");
}

/**
  * @brief  Check Monitor ID in list.
  * @param  [in] sender_addr: Pointer to a uip_ip6addr_t union that indicate Monitor's ID.
  * @retval index
  */
int Check_Monitor_ID(const uip_ipaddr_t *sender_addr)
{
	for(uint8_t i=0;i<g_dev_cnt;i++)
	{
		if(!memcmp(&g_dev_group[i],sender_addr,sizeof(uip_ipaddr_t)))
		{
			return i;
		}
	}
	return -1;
}

/**
  * @brief  Searching index of the ID in Monitor list.
  * @param  [in] rbuf: Pointer to data buffer.
  * @retval index
  */
int16_t Search_Monitor_ID(char *rbuf)
{
	char buf[30]; 
	
	for(uint8_t i=0; i<g_dev_cnt; i++)
	{
		sprintf(buf, "%02x%02x:%02x%02x:%02x%02x:%02x%02x",
				g_dev_group[i].u8[8],g_dev_group[i].u8[9],g_dev_group[i].u8[10],g_dev_group[i].u8[11],
				g_dev_group[i].u8[12],g_dev_group[i].u8[13],g_dev_group[i].u8[14],g_dev_group[i].u8[15]);
		if(strstr(rbuf,buf))
		{
			return i;
		}
	}
	
	return -1;
}

/**
  * @brief  Software system reset.
  * @param  None
  * @retval None
  */
void Sys_Reset(void)
{
	printf("\n*** System Reset ***\n\n");
	__set_FAULTMASK(1);
	NVIC_SystemReset();
}


