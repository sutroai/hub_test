#ifndef __OTA_H
#define __OTA_H

#include "stm32f4xx.h"

#define		HTTP_BLOCK_SIZE		1024

#define		HTTP_HEAD			0
#define 	HTTP_GET			1
#define		OTA_HTTP_HUB		0
#define 	OTA_HTTP_DEV		1
#define		OTA_HUBBIN_ADDR		0x80000
#define		OTA_DEVBIN_ADDR		0x40000
#define		OTA_HUBLEN_ADDR		0x00200
#define		OTA_DEVLEN_ADDR		0x00220

int HTTP_OTA_Update(uint8_t bin,uint8_t *fileName,uint16_t crc);
void check_with_flash(uint8_t bin);

#endif
