#ifndef __PUBLIC_H
#define __PUBLIC_H

#include "stm32f4xx.h"

void Letter_Toggle(uint8_t *str);
void Crypt_AES(uint8_t *data, int len, uint8_t method);
unsigned short do_crc16(unsigned short crc_val,unsigned char *message, unsigned int len);

typedef struct{
	uint8_t *sta;
	uint8_t *end;
}RECIBUF_BORDER_STR;

typedef struct{
	uint8_t *ldle_sta;
	uint8_t *ldle_end;
}RECIBUF_STR;

typedef struct PACKET_STR{
	uint8_t *sta;
	uint8_t *end;
	struct PACKET_STR *next;
}PACKET_STR_TYPEDEF;

typedef struct{
	PACKET_STR_TYPEDEF	*packet_head;
	RECIBUF_STR			rxBuf;
	RECIBUF_STR			rxBuf_shadow;
	RECIBUF_BORDER_STR	rxBuf_border;
	uint8_t				recievePacket_max;
	PACKET_STR_TYPEDEF	*pPacket;
}BUFFER_CTRL_PARA_TYPEDEF;

void xxRecieveBuffer_Module_register(BUFFER_CTRL_PARA_TYPEDEF *pBuffer_para,PACKET_STR_TYPEDEF *pRecievePacket,uint8_t recievePacket_size,uint8_t *pBuffer,uint16_t buffer_size);
int8_t i8xxRecieveBuffer_Read_Period(BUFFER_CTRL_PARA_TYPEDEF *pBuffer_para,uint8_t *pDataOut, uint16_t dataOutBufMax, uint16_t *pSize);
void i8SxxRecieveBuffer_Push_Data(BUFFER_CTRL_PARA_TYPEDEF *pBuffer_para, uint8_t const *data, uint16_t size);

#endif
