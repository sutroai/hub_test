/*
 * Copyright (c) 2011, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "w25qxx.h"
#include "func.h"
#include "factory.h"
#include "public.h"
#include "api.h"

#include "contiki.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-debug.h"

#include "simple-udp.h"
#include "servreg-hack.h"

#include "net/rpl/rpl.h"

#include "dev/button-sensor.h"

#include "main.h"
#include "aws_iot_mqtt_client_interface.h"
#include "ota.h"

#include <stdio.h>
#include <string.h>
#include "st-lib.h"

#include "esp8266.h"
#include "R410M.h"
#include "public.h"

#define UDP_PORT 1234
#define SERVICE_ID 190

#define LOOP_INTERVAL		20

struct simple_udp_connection unicast_connection;	// UDP connection parameter structure

uip_ipaddr_t mcast_addr;							// SubG broadcast address
uint8_t g_subG_ack[60];								// ACK data for SubG
uint8_t g_subG_last[150];							// The last data reported by SubG
uint8_t g_subG_sleep = 0;							// SubG sleep response number

#define TFTP_BLOCK_SIZE		512						// OTA packet size
uint8_t g_ota_state = 0;							// OTA state flag
uint16_t g_ota_index = 0;							// OTA package index
uint16_t g_ota_block = 0;							// total OTA packages
uint8_t g_pause_aws = 0;							// AWS thread pause flag
uip_ipaddr_t g_dev_addr;							// current Monitor address

uint8_t sub1g_recieve_buffer[1024*2];				// SubG receive cache
PACKET_STR_TYPEDEF	sub1g_recieve_packet[50];		// SubG receives an array of packages
BUFFER_CTRL_PARA_TYPEDEF sub1g_packet_para;			// SubG list queue parameter structure

uint8_t mqtt_recieve_buffer[1024];					// MQTT receive cache
PACKET_STR_TYPEDEF	mqtt_recieve_packet[10];		// MQTT receives an array of packets
BUFFER_CTRL_PARA_TYPEDEF mqtt_packet_para;			// MQTT list queue parameter structure

uint8_t g_cmd_data[200];							// Instructions from the server
uint8_t g_cmd_session[10];							// Instruction session
uint8_t g_cmd_sign[30];								// Instruction sign
uint16_t g_cmd_len;									// Instruction length
uint8_t g_resend_flag = 0;							// Resend flag
uint8_t g_wakeup_flag = 0;							// Wakeup flag
uint8_t g_resend_num = 0;							// Number of resend
uint8_t g_resend_loop = 0;							// Number of resend loop

void ft_receiver(struct simple_udp_connection *c,
				const uip_ipaddr_t *sender_addr,
				uint16_t sender_port,
				const uip_ipaddr_t *receiver_addr,
				uint16_t receiver_port,
				const uint8_t *data,
				uint16_t datalen);

/**
  * @brief  Hub acknowledge Monitor's reported data.
  * @param  [in] data: A pointer to a buffer which stored data from Monitor.  
  * @param  [in] sender_addr: Current Monitor's addr.
  * @retval None
  */
void Hub_ACK_Monitor(const uint8_t *data,const uip_ipaddr_t *sender_addr)
{
	if(strstr((char *)data,"\"badCmd\""))	return;
	if(strstr((char *)data,"\"ack\""))		return;
	if(strstr((char *)data,"\"fullReading\""))
	{
		sprintf((char *)g_subG_ack,"{\"fullReading\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"fullReadingVerbose\""))
	{
		sprintf((char *)g_subG_ack,"{\"fullReadingVerbose\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"partialReading\""))
	{
		sprintf((char *)g_subG_ack,"{\"partialReading\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"battery\""))
	{
		sprintf((char *)g_subG_ack,"{\"battery\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"waterTemp\""))
	{
		sprintf((char *)g_subG_ack,"{\"waterTemp\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"lidStatus\""))
	{
		sprintf((char *)g_subG_ack,"{\"lidStatus\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"coreStatus\""))
	{
		sprintf((char *)g_subG_ack,"{\"coreStatus\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"cartridgeStatus\""))
	{
		sprintf((char *)g_subG_ack,"{\"cartridgeStatus\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"submergedInWater\""))
	{
		sprintf((char *)g_subG_ack,"{\"submergedInWater\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"standingUpright\""))
	{
		sprintf((char *)g_subG_ack,"{\"standingUpright\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"cartridgeCount\":\"90\""))
	{
		sprintf((char *)g_subG_ack,"{\"cartridgeCount\":\"90ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}	
	else if(strstr((char *)data,"\"cartridgeCount\""))
	{
		sprintf((char *)g_subG_ack,"{\"cartridgeCount\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"mixBallBottom\""))
	{
		sprintf((char *)g_subG_ack,"{\"mixBallBottom\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"screwRetract\""))
	{
		sprintf((char *)g_subG_ack,"{\"screwRetract\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"shortFlush\""))
	{
		sprintf((char *)g_subG_ack,"{\"shortFlush\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"calibrateLedError\""))
	{
		sprintf((char *)g_subG_ack,"{\"calibrateLedError\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"primeErrorCode\""))
	{
		sprintf((char *)g_subG_ack,"{\"primeErrorCode\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"pcbTemp\""))
	{
		sprintf((char *)g_subG_ack,"{\"pcbTemp\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
	else if(strstr((char *)data,"\"errorCode\""))
	{
		sprintf((char *)g_subG_ack,"{\"errorCode\":\"ack\",\"id\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}",
				sender_addr->u8[8],sender_addr->u8[9],sender_addr->u8[10],sender_addr->u8[11],
				sender_addr->u8[12],sender_addr->u8[13],sender_addr->u8[14],sender_addr->u8[15]);
		simple_udp_sendto(&unicast_connection, g_subG_ack, strlen((char *)g_subG_ack), &mcast_addr);
	}
}




/*---------------------------------------------------------------------------*/
PROCESS(unicast_receiver_process, "Unicast receiver example process");
AUTOSTART_PROCESSES(&unicast_receiver_process);
/*---------------------------------------------------------------------------*/
static void
receiver(struct simple_udp_connection *c,
         const uip_ipaddr_t *sender_addr,
         uint16_t sender_port,
         const uip_ipaddr_t *receiver_addr,
         uint16_t receiver_port,
         const uint8_t *data,
         uint16_t datalen)
{
	if(!g_ch_mask[0])	return;
}

/**
  * @brief  Send data to Monitor via subG. 
  * @param  None
  * @retval None
  */
void SubG_Send_Data(void)
{
	printf("Data send to " );
	uip_debug_ipaddr_print(&g_dev_group[g_dev_index]);
	msg_info("\n%.*s\n\n", g_cmd_len, (char *)g_cmd_data);
	simple_udp_sendto(&unicast_connection, g_cmd_data, g_cmd_len, &mcast_addr);
}

/**
  * @brief  Pick out session field from command data.
  * @param  [in] cmd: A pointer to a buffer which stored command data.  
  * @param  [in] len: Amount of data to be remove.
  * @retval None
  */
void Get_Session_From_CMD(uint8_t *cmd, uint16_t len)
{
	char buf[200];
	char *pres;
	
	memcpy(buf, cmd, len);
	if(strstr(buf,"\"devOtaUpdate\"") && strstr(buf,"\"session\""))
	{
		pres = strtok((char *)buf,",");
		pres = strtok((char *)'\0',",");
		pres = strtok((char *)'\0',",");
		pres = strtok((char *)'\0',",");
		pres = strtok((char *)'\0',":");
		pres = strtok((char *)'\0',"\"");
		memset(g_cmd_session,0x00,sizeof(g_cmd_session));
		strcpy((char *)g_cmd_session,pres);
	}	
}

/**
  * @brief  Pick out sign field from command data.
  * @param  [in] cmd: A pointer to a buffer which stored command data.  
  * @param  [in] len: Amount of data to be remove.
  * @retval None
  */
void Get_Sign_From_CMD(uint8_t *cmd, uint16_t len)
{
	char buf[200];
	char *pres;
	
	memcpy(buf, cmd, len);
	strtok((char*)buf, "\"");
	strtok((char *)'\0', "\"");
	strtok((char *)'\0', "\"");
	pres = strtok((char *)'\0',"\"");
	strcpy((char *)g_cmd_sign,pres);
	if(strstr((char *)g_cmd_sign, ":"))		strtok((char*)g_cmd_sign, ":");
	if(strstr((char *)g_cmd_sign, "sysInfo"))		strcpy((char *)g_cmd_sign,"sysHW");
	if(strstr((char *)g_cmd_sign, "subGInfo"))		strcpy((char *)g_cmd_sign,"subGIP");	
}

/**
  * @brief  Determine remote Monitor to be wakeup via subG. 
  * @param  None
  * @retval None
  */
void SubG_Wakeup_Monitor(void)
{
	char sbuf[50];
	
	if(g_dev_sleep[g_dev_index])
	{
		uint32_t rt = 5000;
		sprintf(sbuf,"\"wakup\":\"%02x%02x:%02x%02x:%02x%02x:%02x%02x\"}", 
						g_dev_group[g_dev_index].u8[8],g_dev_group[g_dev_index].u8[9],g_dev_group[g_dev_index].u8[10],g_dev_group[g_dev_index].u8[11],
						g_dev_group[g_dev_index].u8[12],g_dev_group[g_dev_index].u8[13],g_dev_group[g_dev_index].u8[14],g_dev_group[g_dev_index].u8[15]);
		
		printf("Wakeup Monitor \n");
		BSP_Timer_Start(SYS_TIMER2,rt);
		while(rt)
		{
			rt = BSP_Timer_Read(SYS_TIMER2);
			if(!(rt%5))
			{
				NETSTACK_RADIO.send(sbuf, strlen(sbuf));
			}
		}
	}
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(unicast_receiver_process, ev, data)
{
	static struct etimer periodic_timer;

	PROCESS_BEGIN();
	
	if(g_ft)
	{
		simple_udp_register(&unicast_connection, UDP_PORT, NULL, UDP_PORT, ft_receiver);
	}
	else
	{
		simple_udp_register(&unicast_connection, UDP_PORT, NULL, UDP_PORT, receiver);
	}
//	etimer_set(&periodic_timer, LOOP_INTERVAL);
	uip_create_linklocal_allnodes_mcast(&mcast_addr);
	
	while(1)
	{
		IWDG_Feed();
//		PROCESS_WAIT_EVENT();
//		if(ev == PROCESS_EVENT_TIMER && data == &periodic_timer) 
//		{
//			etimer_set(&periodic_timer, LOOP_INTERVAL);
//		}
		etimer_set(&periodic_timer, LOOP_INTERVAL);
		PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
	}
	PROCESS_END();
}
/*---------------------------------------------------------------------------*/

/**
  * @}
  */
