/*
 * Copyright (c) 2011, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 */

#include "w25qxx.h"
#include "func.h"
#include "api.h"

#include "contiki.h"
#include "lib/random.h"
#include "sys/ctimer.h"
#include "sys/etimer.h"
#include "net/ip/uip.h"
#include "net/ipv6/uip-ds6.h"
#include "net/ip/uip-debug.h"

#include "simple-udp.h"
#include "servreg-hack.h"

#include "net/rpl/rpl.h"

#include "dev/button-sensor.h"

#include "main.h"
#include "aws_iot_mqtt_client_interface.h"

#include <stdio.h>
#include <string.h>
#include "st-lib.h"
/** @addtogroup Udp_receiver
  * @{
  */

#define UDP_PORT 1234
#define SERVICE_ID 190

#define LOOP_INTERVAL		5
//#define LOOP_INTERVAL		(10 * CLOCK_SECOND)
//#define SEND_TIME		(random_rand() % (SEND_INTERVAL))


uint8_t g_ack[60];
uip_ipaddr_t mcast_addr;


struct simple_udp_connection unicast_connection;

uip_ipaddr_t g_dev_addr;
/*---------------------------------------------------------------------------*/
PROCESS(unicast_receiver_process, "Unicast receiver example process");
AUTOSTART_PROCESSES(&unicast_receiver_process);
/*---------------------------------------------------------------------------*/
static void
receiver(struct simple_udp_connection *c,
         const uip_ipaddr_t *sender_addr,
         uint16_t sender_port,
         const uip_ipaddr_t *receiver_addr,
         uint16_t receiver_port,
         const uint8_t *data,
         uint16_t datalen)
{
	if(strstr((char *)data,"\"CMD\":PAIRING"))
	{
		if(g_subg_pair)
		{
			printf("Data received from ");
			uip_debug_ipaddr_print(sender_addr);
			printf("\r\n%.*s \n", datalen, data);
			
			memcpy(&g_dev_addr, sender_addr, sizeof(uip_ipaddr_t));
			sprintf((char *)g_ack, "{\"ACK\":PAIRING}");
			printf("--> %s \n\n",g_ack);
			simple_udp_sendto(&unicast_connection, g_ack, strlen((char *)g_ack), &mcast_addr);
		}
	}
	else if(!memcmp(&g_dev_addr, sender_addr, sizeof(uip_ipaddr_t)))
	{
		printf("Data received from ");
		uip_debug_ipaddr_print(sender_addr);
		printf("\r\n%.*s \n\n", datalen, data);
	}
}

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(unicast_receiver_process, ev, data)
{
	static struct etimer periodic_timer;

	PROCESS_BEGIN();
	
	simple_udp_register(&unicast_connection, UDP_PORT, NULL, UDP_PORT, receiver);
	etimer_set(&periodic_timer, LOOP_INTERVAL);
	uip_create_linklocal_allnodes_mcast(&mcast_addr);
	
	while(1)
	{
		PROCESS_WAIT_EVENT();
		if(ev == PROCESS_EVENT_TIMER && data == &periodic_timer) 
		{
			etimer_set(&periodic_timer, LOOP_INTERVAL);
			
		}
	}
	PROCESS_END();
}
/*---------------------------------------------------------------------------*/

/**
  * @}
  */
