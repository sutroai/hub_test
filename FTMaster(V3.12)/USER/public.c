#include "api.h"
#include "public.h"
#include "main.h"
#include "aws_iot_mqtt_client_interface.h"

//======================================================================
// 函数: Letter_Toggle
// 功能: 大小写转换
//======================================================================
void Letter_Toggle(uint8_t *str)
{
	int i;
	
    for(i=0;str[i]!='\0';i++)
    {
        if((str[i]>='A'&&str[i]<='Z')||(str[i]>='a'&&str[i]<='z'))
        str[i]^=32;		//核心语句，异或实现大写减32,小写加32.
    }
}

//======================================================================
// 函数: Crypt_AES
// 功能: 128位AES加密、解密(ECB模式)
// 参数: method:
//          MBEDTLS_AES_ENCRYPT
//          MBEDTLS_AES_DECRYPT
//======================================================================
void Crypt_AES(uint8_t *data, int len, uint8_t method)
{
    mbedtls_aes_context aes;
    uint8_t key[16] = "svvfrank20180820";       // 128位(16字节)密钥
    
    // 如果文件长度不是128位(16字节)的整数倍，则补齐
    int size = len;
    if (len % 16 != 0) {
        size = (len / 16 + 1) * 16;
    }
    
    // 明文内容赋值给content
    unsigned char content[size];
    memcpy(content, data, len);
    for (int j = len; j < size; ++j) {
        content[j] = 0;
    }

    // 分组加密、解密并存储结果
    if(method == MBEDTLS_AES_ENCRYPT)
    {
        mbedtls_aes_setkey_enc(&aes, key, 128);
    }
    else if(method == MBEDTLS_AES_DECRYPT)
    {
        mbedtls_aes_setkey_dec(&aes, key, 128);
    }
    for (int i = 0; i < size / 16; ++i)
    {
        mbedtls_aes_crypt_ecb(&aes, method, content + i * 16, data + i * 16);
    }
}


