/**
  ******************************************************************************
  * @file    wifi.c
  * @author  MCD Application Team
  * @brief   WIFI interface file.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "wifi.h"
#include "esp8266.h"

/* Private define ------------------------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
/**
  * @brief  Initialiaze the WIFI core
  * @param  None
  * @retval Operation status
  */
WIFI_Status_t WIFI_Init(void)
{
	WIFI_Status_t ret = WIFI_STATUS_ERROR;

	if(ESP8266_Module_Init() == 0)
	{
		ret = WIFI_STATUS_OK;
		ESP8266_Set_CWMODE(ESP_MODE_STA);
	}

	return ret;
}


/**
  * @brief  Join an Access Point
  * @param  SSID : SSID string
  * @param  Password : Password string
  * @param  ecn : Encryption type
  * @param  IP_Addr : Got IP Address
  * @param  IP_Mask : Network IP mask
  * @param  Gateway_Addr : Gateway IP address
  * @param  MAC : pointer to MAC Address
  * @retval Operation status
  */
WIFI_Status_t WIFI_Connect(
                             const char* SSID, 
                             const char* Password,
                             WIFI_Ecn_t ecn)
{
	WIFI_Status_t ret = WIFI_STATUS_ERROR;  

	if(ESP8266_Join_AP((uint8_t *)SSID,(uint8_t *)Password) == 0)
	{
		ret = WIFI_STATUS_OK;
	}

	return ret;
}



/**
  * @brief  Disconnect from a network
  * @param  None
  * @retval Operation status
  */
WIFI_Status_t WIFI_Disconnect(void)
{
	WIFI_Status_t ret = WIFI_STATUS_ERROR;    
	if( ESP8266_Quit_AP() == 0)
	{
		ret = WIFI_STATUS_OK; 
	}

	return ret;
}



/**
  * @brief  Configure and start a client connection
  * @param  type : Connection type TCP/UDP
  * @param  name : name of the connection
  * @param  ipaddr : Client IP address
  * @param  port : Remote port
  * @param  local_port : Local port
  * @retval Operation status
  */
WIFI_Status_t WIFI_OpenClientConnection(uint32_t socket, WIFI_Protocol_t type, const char *name, uint8_t *ipaddr, uint16_t port, uint16_t local_port)
{
  WIFI_Status_t ret = WIFI_STATUS_ERROR;
//  ES_WIFI_Conn_t conn;
//  
//  conn.Number = socket;
//  conn.RemotePort = port;
//  conn.LocalPort = local_port;
//  conn.Type = (type == WIFI_TCP_PROTOCOL)? ES_WIFI_TCP_CONNECTION : ES_WIFI_UDP_CONNECTION;
//  conn.RemoteIP[0] = ipaddr[0];
//  conn.RemoteIP[1] = ipaddr[1];
//  conn.RemoteIP[2] = ipaddr[2];
//  conn.RemoteIP[3] = ipaddr[3];
//  if(ES_WIFI_StartClientConnection(&EsWifiObj, &conn)== ES_WIFI_STATUS_OK)
//  {
//    ret = WIFI_STATUS_OK;
//  }
	
	if(ESP8266_TCP_Link(ipaddr,port) == 0)
	//if(ESP8266_TCP_Link((uint8_t *)"18.191.24.253",8883) == 0)
	//if(ESP8266_TCP_Link((uint8_t *)"a1d8rh9ogf1efh.iot.us-east-2.amazonaws.com",8883) == 0)
	{
		ret = WIFI_STATUS_OK;
	}
	
	ESP8266_TS_Enter();
	ESP8266_TS_EnSend();
	
  return ret;
}

/**
  * @brief  Close client connection
  * @param  type : Connection type TCP/UDP
  * @param  name : name of the connection
  * @param  location : Client address
  * @param  port : Remote port
  * @param  local_port : Local port
  * @retval Operation status
  */
WIFI_Status_t WIFI_CloseClientConnection(uint32_t socket)
{
  WIFI_Status_t ret = WIFI_STATUS_ERROR;  
//  ES_WIFI_Conn_t conn;
//  conn.Number = socket;
//  
//  if(ES_WIFI_StopClientConnection(&EsWifiObj, &conn)== ES_WIFI_STATUS_OK)
//  {
//    ret = WIFI_STATUS_OK;
//  }
	
	ESP8266_TS_ExSend();
	ESP8266_TS_Exit();
	
	if(ESP8266_TCP_Unlink() == 0)
	{
		ret = WIFI_STATUS_OK;
	}
	return ret; 
}


/**
  * @brief  Send Data on a socket
  * @param  pdata : pointer to data to be sent
  * @param  len : length of data to be sent
  * @retval Operation status
  */
WIFI_Status_t WIFI_SendData(uint8_t socket, uint8_t *pdata, uint16_t Reqlen, uint16_t *SentDatalen, uint32_t Timeout)
{
	WIFI_Status_t ret = WIFI_STATUS_ERROR;
	
	if(ESP8266_TS_Send(pdata,Reqlen) == 0)
	{
//		printf("SendData:  %d \n",Reqlen);
		ret = WIFI_STATUS_OK;
	}
	*SentDatalen = Reqlen;
	return ret;
}



WIFI_Status_t WIFI_SendDataTo(uint8_t socket, uint8_t *pdata, uint16_t Reqlen, uint16_t *SentDatalen, uint32_t Timeout, uint8_t *ipaddr, uint16_t port)
{
  WIFI_Status_t ret = WIFI_STATUS_ERROR;

  return ret;
}

/**
  * @brief  Receive Data from a socket
  * @param  pdata : pointer to Rx buffer
  * @param  *len :  pointer to length of data
  * @retval Operation status
  */
WIFI_Status_t WIFI_ReceiveData(uint8_t socket, uint8_t *pdata, uint16_t Reqlen, uint16_t *RcvDatalen, uint32_t Timeout)
{
	WIFI_Status_t ret = WIFI_STATUS_ERROR; 
	
	if(ESP8266_TS_Recv(pdata,Reqlen,RcvDatalen,Timeout) == 0)
	{
//		if(*RcvDatalen)
//		{
//			printf("RecvData:  %d/%d \n",*RcvDatalen,Reqlen);
//		}
		ret = WIFI_STATUS_OK; 
	}

	return ret;
}


WIFI_Status_t WIFI_ReceiveDataFrom(uint8_t socket, uint8_t *pdata, uint16_t Reqlen, uint16_t *RcvDatalen, uint32_t Timeout, uint8_t *ipaddr, uint16_t *port)
{
  WIFI_Status_t ret = WIFI_STATUS_ERROR; 

  return ret;
}




/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

