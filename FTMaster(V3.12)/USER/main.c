#include "api.h"
#include "public.h"
#include "func.h"
#include "esp8266.h"
#include "w25qxx.h" 
#include "ota.h"
#include "net.h"
#include "cloud.h"
#include "R410M.h"
#include "radio-config.h"
#include "process.h"

#include "contiki.h"
#include "net/ip/uip.h"
#include "simple-udp.h"
#include "net/ip/uip-debug.h"

#include "main.h"
//#include "aws_iot_log.h"
//#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"

#include "netstack.h"

void Stack_6LoWPAN_Init(void);


extern uip_ipaddr_t mcast_addr;

extern uip_ipaddr_t ipaddr;
extern unsigned char node_mac[8];
extern uint8_t g_token_set;


uint8_t cmdBuf[200];
uint16_t cmdLen;
extern struct simple_udp_connection unicast_connection;

RNG_HandleTypeDef hrng;
net_hnd_t         hnet;

static void SystemClock_Config(void)
{
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_OscInitTypeDef RCC_OscInitStruct;
  HAL_StatusTypeDef ret = HAL_OK;

  /* Enable Power Control clock */
  __HAL_RCC_PWR_CLK_ENABLE();

  /* The voltage scaling allows optimizing the power consumption when the device is 
     clocked below the maximum system frequency, to update the voltage scaling value 
     regarding system frequency refer to product datasheet.  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /* Enable HSE Oscillator and activate PLL with HSE as source */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 200;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 7;
  RCC_OscInitStruct.PLL.PLLR = 2;
  ret = HAL_RCC_OscConfig(&RCC_OscInitStruct);
  
  if(ret != HAL_OK)
  {
    while(1) { ; } 
  }

  /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2 
     clocks dividers */
  RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;  
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;  
  ret = HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3);
  if(ret != HAL_OK)
  {
    while(1) { ; }  
  }
}

void UART1_Recv_Data(uint8_t *data,uint16_t *len)
{	
	*len = 0;
	
	BSP_Timer_Start(SYS_TIMER1,2);
	while(BSP_Timer_Read(SYS_TIMER1))
	{
		if(G_uart1_num)
		{
			*data = BSP_UART1_RecvChar();
			data++;
			(*len)++;
			BSP_Timer_Start(SYS_TIMER1,2);
		}
	}
}


/*---------------------------------------------------------------------------*/
PROCESS(main_process, "main process");

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(main_process, ev, data)
{
	static struct etimer et;
	static uint8_t key;
	
	PROCESS_BEGIN();
	
	while(1)
	{
		key = BSP_KEY_Read();
		if(key == USER_BUTTON2)
		{
			BSP_LED_ON(BSP_LED_ID_BLUE);
			g_subg_pair = 1;
			G_subg_pair_time = HAL_GetTick() + 30000;				// 配对时间持续30秒
		}		
		
		if(G_uart1_num)
		{
			memset(cmdBuf,0x00,sizeof(cmdBuf));
			UART1_Recv_Data(cmdBuf,&cmdLen);
			//if(strstr((char *)cmdBuf,"\r"))						// 通过串口发送指令给device
			if(cmdLen)
			{
				printf("Send %s to ",cmdBuf);
				uip_debug_ipaddr_print(&mcast_addr);
				printf("\r\n");
				simple_udp_sendto(&unicast_connection, cmdBuf, strlen((char *)cmdBuf) + 1, &mcast_addr);
			}
		}
		
		etimer_set(&et, CLOCK_SECOND/100);                    		// 溢出周期 10ms
		PROCESS_WAIT_EVENT_UNTIL( ev == PROCESS_EVENT_TIMER);
	}

	PROCESS_END();
}

int main(void)
{
    HAL_Init();
    SystemClock_Config();
	
    BSP_LED_Init();
    BSP_KEY_Init();
    BSP_CHG_Init();
	BSP_UART1_Init();
	W25QXX_Init();
    
	printf("*************************************************************\n");
	printf("***   Sutro Hub for STM32F412 MCU                            \n");
	printf("***   Hub Factory Test Master                   \n");
	printf("***   FW Version: %s    %s  %s \n", SYS_SW, __DATE__, __TIME__);
	printf("*************************************************************\n");

    Stack_6LoWPAN_Init();
	// +TI-PA
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_0, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_VDD});
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_1, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_RX_STATE});
    S2LPGpioInit(&(SGpioInit){S2LP_GPIO_2, S2LP_GPIO_MODE_DIGITAL_OUTPUT_HP, S2LP_GPIO_DIG_OUT_TX_STATE});
	
	process_start(&main_process, NULL);

    while(1) {
      int r = 0;
      do {
        r = process_run();
      } while(r > 0);
    }

}	



