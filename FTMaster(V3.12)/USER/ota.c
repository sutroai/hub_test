#include "api.h"
#include "ota.h"
#include "esp8266.h"
#include "w25qxx.h"

uint32_t g_file_len;
uint16_t g_head_len;
uint16_t g_cont_len;

uint32_t HTTP_Form_Packet(char *pkt,uint8_t type,char *range)
{
	if(type == HTTP_HEAD)
	{
		sprintf(pkt,"HEAD /sutro-firmware/HubV3.bin HTTP/1.1\r\n");
	}
	else if(type == HTTP_GET)
	{
		sprintf(pkt,"GET /sutro-firmware/HubV3.bin HTTP/1.1\r\n");
	}
	strcat(pkt, "Host: s3-us-west-2.amazonaws.com:80\r\n");
//	strcat(pkt, "Connection: Keep-Alive\r\n");				// 保持连接
//	strcat(pkt, "Cache-Control: no-cache\r\n");				// 不使用缓存
//	strcat(pkt, "User-Agent: test-123\r\n");
//	strcat(pkt, "Accept: */*\r\n");
	
	if((type==HTTP_GET) && range)
	{
		strcat(pkt, range);
	}
	strcat(pkt, "\r\n");
	
	return strlen(pkt);
}

int HTTP_Parse_Packet(uint8_t *data,uint8_t type)
{
	char pfile_head_buf[500];
	char pfile_clen_buf[40];
	char *pres;
	
	// 计算包头长度
	pres = strstr((char *)data,"\r\n\r\n");
	g_head_len = pres - (char *)data + 4;
	printf("head len: %d \n",g_head_len);
	memset(pfile_head_buf,0x00,sizeof(pfile_head_buf));
	memcpy(pfile_head_buf,data,g_head_len);
	
	// 判断返回结果
	pres = strtok((char *)pfile_head_buf,"\r\n");
	if((strstr(pres,"HTTP/1.1 200") == NULL) && (strstr(pres,"HTTP/1.1 206") == NULL))		return -1;
		
	// 循环分割包头
	while( pres != NULL )
	{	
		//printf("%s \n",pres);
		if(strstr(pres,"Content-Length") != NULL )
		{
			memset(pfile_clen_buf,0x00,sizeof(pfile_clen_buf));
			strcpy(pfile_clen_buf, pres);
		}

		pres = strtok( NULL, "\r\n");
	}
	
	// 解析文件/接收长度
	pres = strtok(pfile_clen_buf," ");
	pres = strtok( NULL, "\0");
	if(type == HTTP_HEAD)
	{
		g_file_len = atol(pres);
		printf("file len: %ld \n\n",(long)g_file_len);
	}
	else if(type == HTTP_GET)
	{
		g_cont_len = atol(pres);
		printf("cont len: %ld \n\n",(long)g_cont_len);
	}
	
	return 0;
}

void HTTP_OTA_Update(void)
{
	uint8_t range[30];
	uint8_t sbuf[200];
	uint16_t slen;
	uint8_t rbuf[2000];
	uint16_t rlen;
	int ret = 0;
	uint16_t blocks = 0;
	uint16_t remain = 0;
	
	ESP8266_TS_ExSend();
	ESP8266_TS_Exit();
	ESP8266_TCP_Unlink();	
	
	HAL_Delay(2000);
	printf("Link to Server... ");
	ret = ESP8266_TCP_Link((uint8_t *)"s3-us-west-2.amazonaws.com",80);
	printf("Ret:%d \n\n",ret);

	HAL_Delay(1000);
	ESP8266_TS_Enter();
	ESP8266_TS_EnSend();
	
	memset(sbuf,0x00,sizeof(sbuf));
	slen = HTTP_Form_Packet((char *)sbuf, HTTP_HEAD, NULL);
	ESP8266_TS_Send(sbuf,slen);
	printf("Send Command... slen:%d \n", slen);
	memset(rbuf,0x00,sizeof(rbuf));
	ESP8266_OTA_Recv(rbuf,&rlen,EIGHT_SECOND);
	printf("Recv Data... rlen:%d \n",rlen);
	//printf("%s",rbuf);
	HTTP_Parse_Packet(rbuf,HTTP_HEAD);
	
	blocks = g_file_len/HTTP_BLOCK_SIZE;
	remain = g_file_len%HTTP_BLOCK_SIZE;
	if(remain)		blocks++;
	for(uint16_t i = 0; i < blocks; i++)
	{
		memset(sbuf,0x00,sizeof(sbuf));
		if(i == blocks - 1)
		{
			sprintf((char *)range,"Range: bytes=%d-%d\r\n", i*HTTP_BLOCK_SIZE, i*HTTP_BLOCK_SIZE + remain - 1);
		}
		else
		{
			sprintf((char *)range,"Range: bytes=%d-%d\r\n",i*HTTP_BLOCK_SIZE,(i+1)*HTTP_BLOCK_SIZE - 1);
		}


		slen = HTTP_Form_Packet((char *)sbuf, HTTP_GET, (char *)range);
		for(uint8_t j = 0; j < 5; j++)			// 若接收失败,重试5次
		{
			ESP8266_TS_Send(sbuf,slen);
			printf("send block: %d/%d \n", i + 1, blocks);
			
			memset(rbuf,0x00,sizeof(rbuf));
			ESP8266_OTA_Recv(rbuf,&rlen,EIGHT_SECOND);
			printf("recv data: %d \n",rlen);
			if(rlen == 0)
			{
				continue;
			}
			else if((rlen<1280) && (i!=blocks - 1) )
			{
				HAL_Delay(3000);
				continue;
			}
			HTTP_Parse_Packet(rbuf,HTTP_GET);
			if(rlen != g_head_len + g_cont_len)
			{
				HAL_Delay(3000);
				continue;
			}
			else
			{
				break;
			}
		}
		//printf("%s\n\n",rbuf);
		W25QXX_Write(rbuf + g_head_len, OTA_START_ADDR + i * HTTP_BLOCK_SIZE, g_cont_len);
	}
	W25QXX_Write((uint8_t*)&g_file_len, OTA_HUBLEN_ADDR, 4);		// 写bin文件长度
	
	ESP8266_TS_ExSend();
	ESP8266_TS_Exit();
	ESP8266_TCP_Unlink();
	
	printf("\n*** HTTP OTA Update complete ***\n\n");
	return;
}

// 检查flash中的bin文件
void check_with_flash(void)
{
	uint8_t rbuf[1200];
	uint16_t blocks = 0;
	uint16_t remain = 0;
	
	blocks = g_file_len/HTTP_BLOCK_SIZE;
	remain = g_file_len%HTTP_BLOCK_SIZE;
	if(remain)		blocks++;
	for(uint16_t i = 0; i < blocks; i++)
	{
		memset(rbuf,0x00,sizeof(rbuf));
		if(i == blocks - 1)
		{
			W25QXX_Read(rbuf, OTA_START_ADDR + i * HTTP_BLOCK_SIZE, remain);
			BSP_UART1_SendStr(rbuf,remain);
		}
		else
		{
			W25QXX_Read(rbuf, OTA_START_ADDR + i * HTTP_BLOCK_SIZE, HTTP_BLOCK_SIZE);
			BSP_UART1_SendStr(rbuf,HTTP_BLOCK_SIZE);
		}
	}
}

