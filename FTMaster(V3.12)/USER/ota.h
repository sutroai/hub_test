#ifndef __OTA_H
#define __OTA_H

#include "stm32f4xx.h"

#define		HTTP_BLOCK_SIZE		1024
#define		HTTP_HEAD			0
#define 	HTTP_GET			1
#define		OTA_START_ADDR		0x80000
#define		OTA_HUBLEN_ADDR		0x00200
#define		OTA_DEVLEN_ADDR		0x00220

void HTTP_OTA_Update(void);
void check_with_flash(void);

#endif
