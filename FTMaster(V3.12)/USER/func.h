#ifndef __FUNC_H
#define __FUNC_H

#include "stm32f4xx.h"
#include "net/ip/uip.h"

#define SYS_HW          "2.00"
#define SYS_SW          "3.12"
#define SYS_OS          "contiki-3.0"
#define SYS_AWS         ""


#define DEV_MAX_CNT		20                  // 支持Device的最大数量
#define DEV_IP_TAB      0x20000             // Device IP地址表起始地址
 

extern uint8_t g_subg_pair;

extern uint8_t g_wifiSSID[32];
extern uint8_t g_wifiPSK[32];
extern uint8_t g_token[384];
extern uint8_t g_wifiRSSI[10];
extern uint32_t g_onBoardingTim;
extern uint8_t g_dev_cnt;
extern uip_ipaddr_t g_dev_group[DEV_MAX_CNT];

void WIFI_AP_Init(void);
void WIFI_AP_Config(void);

int Param_Get(void);
void Param_SetDef(void);
int check_dev(const uip_ipaddr_t *sender_addr);


void Sys_Reset(void);

#endif

