#include "api.h"
#include "public.h"
#include "func.h"
#include "w25qxx.h"
#include "net/ip/uip-debug.h"
#include "esp8266.h"

#include "main.h"
#include "aws_iot_mqtt_client_interface.h"


uint8_t g_subg_pair = 0;                    // 配对标志
uint8_t g_token_set = 0;					// 配置徽标标志

uint8_t g_wifiSSID[32];
uint8_t g_wifiPSK[32];
uint8_t g_token[384];
uint8_t g_wifiRSSI[10];
uint32_t g_onBoardingTim;
uint8_t g_dev_cnt = 0;						// 设备数量
uip_ipaddr_t g_dev_group[DEV_MAX_CNT];		// 设备地址


void WIFI_AP_Init(void)
{
    uint8_t mac[20];


	if(!ESP8266_Module_Init())
	{
		printf("ESP8266_Module_Init  OK \n");
	}
	else
	{
		printf("ESP8266_Module_Init  ERR \n");
	}

	ESP8266_Set_CWMODE(ESP_MODE_SAPSTA);

	ESP8266_Get_APMAC(mac);
	Letter_Toggle(mac);
	//ESP8266_Set_CWSAP(mac,(uint8_t *)"svv_2018");
	ESP8266_Set_CWSAP(mac, NULL);
}

void WIFI_AP_Config(void)
{
	uint8_t rbuf[500];
	uint16_t rlen;
	uint8_t sbuf[100];
    char *pres;
	
	printf("\n*** wifi network configuration  ***\n\n");

    ESP8266_Set_CIPAP((uint8_t *)"192.168.1.1");
    ESP8266_Set_CIPMUX(ESP_LINK_MUX);
	if(!ESP8266_Set_CIPSERVER(ESP_SERVER_ON))
	{
		printf("ESP8266_Set_CIPSERVER  OK \n\n");
	}
	else
	{
		printf("ESP8266_Set_CIPSERVER  ERR \n\n");
		goto end;
	}
    	
	printf("Waiting for phone connection \n\n");
	while(1)
	{
		memset(rbuf,0x00,sizeof(rbuf));
		if(!ESP8266_TCP_Recv(rbuf,&rlen,QUARTER_SECOND))
		{
			printf("<- %s \n",rbuf[0] == '\r'? rbuf+2:rbuf);
			
			// +IPD,0,21:{"ssid":"svv-office"}
			if(strstr((char *)rbuf,"ssid"))
			{
				pres = strtok((char *)rbuf,"\"");
				pres = strtok((char *)'\0',"\"");
				pres = strtok((char *)'\0',"\"");
				pres = strtok((char *)'\0',"\"");
				memset(g_wifiSSID,0x00,sizeof(g_wifiSSID));
				strcpy((char *)g_wifiSSID,pres);
				W25QXX_Write(g_wifiSSID, 0x00, 32);
				sprintf((char *)sbuf,"{\"ack\":\"ssid\"}");
				ESP8266_TCP_MuxSend(0,sbuf,strlen((char *)sbuf));
			}
			// +IPD,0,21:{"psk":"soaring@svv"}
			else if(strstr((char *)rbuf,"psk"))
			{
				pres = strtok((char *)rbuf,"\"");
				pres = strtok((char *)'\0',"\"");
				pres = strtok((char *)'\0',"\"");				
				pres = strtok((char *)'\0',"\"");
				memset(g_wifiPSK,0x00,sizeof(g_wifiPSK));
				strcpy((char *)g_wifiPSK,pres);
				W25QXX_Write(g_wifiPSK, 0x20, 32);
				sprintf((char *)sbuf,"{\"ack\":\"psk\"}");
				ESP8266_TCP_MuxSend(0,sbuf,strlen((char *)sbuf));
			}
			// +IPD,0,18:{"token":">=oo=<"}
			else if(strstr((char *)rbuf,"token"))
			{
				pres = strtok((char *)rbuf,"\"");
				pres = strtok((char *)'\0',"\"");
				pres = strtok((char *)'\0',"\"");				
				pres = strtok((char *)'\0',"\"");
				memset(g_token,0x00,sizeof(g_token));
				strcpy((char *)g_token,pres);
                Crypt_AES(g_token, sizeof(g_token), MBEDTLS_AES_ENCRYPT);
				W25QXX_Write(g_token, 0x80, 384);
				sprintf((char *)sbuf,"{\"ack\":\"token\"}");
				ESP8266_TCP_MuxSend(0,sbuf,strlen((char *)sbuf));
				g_token_set = 1;
			}
            // +IPD,0,25:{"wifi":"requestConnect"}
            else if(strstr((char *)rbuf,"requestConnect"))
            {
                break;
            }
		}
	}

end:
	printf("\n*** wifi network configuration complete ***\n\n");
}


int Param_Get(void)
{
	W25QXX_Read(g_wifiSSID, 0x00, 32);
	W25QXX_Read(g_wifiPSK, 0x20, 32);
    W25QXX_Read(&g_dev_cnt, 0x60, 1);
//	if(g_wifiSSID[0]==0xFF && g_wifiPSK[0]==0xFF && g_dev_cnt==0xFF)	// 第一次使用,需初始化 
//	{
//		Param_SetDef();
//        printf("parameter initialize,need to config network.\n");
//        return -1;
//	}
//    if(g_wifiSSID[0]==0x00 && g_wifiPSK[0]==0x00)                       // 无网络参数,需配网
//    {
//        printf("no network parameter,need to config network.\n");
//        return -1;
//    }

    W25QXX_Read(g_token, 0x80, 384);
    Crypt_AES(g_token, sizeof(g_token), MBEDTLS_AES_DECRYPT);
    
    if(g_dev_cnt > DEV_MAX_CNT)     g_dev_cnt = 0;              // 避免卡死
	for(uint8_t i=0;i<g_dev_cnt;i++)
	{
		W25QXX_Read((uint8_t *)&g_dev_group[i], DEV_IP_TAB + i*16, 16);
	}
	
//	printf("SSID: %s \n",g_wifiSSID);
//	printf("PSK:  %s \n",g_wifiPSK);
//    printf("Token: %s \n",g_token);
	printf("DevCN: %d \n",g_dev_cnt);
	for(uint8_t i=0;i<g_dev_cnt;i++)
	{
        printf("DevIP: %02x%02x:%02x%02x:%02x%02x:%02x%02x\n", 
        g_dev_group[i].u8[8],g_dev_group[i].u8[9],g_dev_group[i].u8[10],g_dev_group[i].u8[11],
        g_dev_group[i].u8[12],g_dev_group[i].u8[13],g_dev_group[i].u8[14],g_dev_group[i].u8[15]);
	}
    return 0;
}

void Param_SetDef(void)
{
	memset(g_wifiSSID, 0x00, sizeof(g_wifiSSID));
	memset(g_wifiPSK, 0x00, sizeof(g_wifiPSK));
    memset(g_token, 0x00, sizeof(g_token));
	g_dev_cnt = 0;

	W25QXX_Write(g_wifiSSID, 0x00, 32);
	W25QXX_Write(g_wifiPSK, 0x20, 32);
	W25QXX_Write(&g_dev_cnt, 0x60, 1);
    W25QXX_Write(g_token, 0x80, 384);
}

int check_dev(const uip_ipaddr_t *sender_addr)
{
	for(uint8_t i=0;i<g_dev_cnt;i++)
	{
		if(!memcmp(&g_dev_group[i],sender_addr,sizeof(uip_ipaddr_t)))
		{
			return 1;		// 设备存在于列表中
		}
	}
	return 0;
}

void Sys_Reset(void)
{
	printf("\n*** System Reset ***\n\n");
	__set_FAULTMASK(1);			// 关闭所有中端
	NVIC_SystemReset();			// 复位
}


