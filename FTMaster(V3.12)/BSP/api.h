#ifndef __API_H
#define __API_H

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"


//=======================================================================
// LED
//=======================================================================
#define BSP_LED_ID_RED			0
#define BSP_LED_ID_GREEN		1
#define BSP_LED_ID_BLUE			2
#define BSP_LED_ID_ALL			3


void BSP_LED_Init(void);
void BSP_LED_ON(uint32_t led);
void BSP_LED_OFF(uint32_t led);
void BSP_LED_Toggle(uint32_t led);


//=======================================================================
// MISC
//=======================================================================
void SystemClock_Config(void);


//=======================================================================
// RTC
//=======================================================================

void BSP_RTC_Init(void);
HAL_StatusTypeDef  BSP_RTC_SetDate(uint8_t year, uint8_t month, uint8_t date);
HAL_StatusTypeDef  BSP_RTC_SetTime(uint8_t hour, uint8_t min, uint8_t sec);
void BSP_RTC_GetDate(RTC_DateTypeDef *sdate);
void BSP_RTC_GetTime(RTC_TimeTypeDef *stime);
uint8_t BSP_RTC_GetWeek(uint8_t year,uint8_t month,uint8_t date);


//=======================================================================
// UART
//=======================================================================
#define UART_BUF_LEN		1024

extern uint32_t G_uart1_num;		// 暂时用这种方案
void BSP_UART1_Init(void);
void BSP_UART1_SendStr(uint8_t *str,uint32_t len);
short BSP_UART1_RecvChar(void);
void BSP_UART1_FlushFIFO(void);

extern uint32_t G_uart2_num;		// 暂时用这种方案
void BSP_UART2_Init(void);
void BSP_UART2_SendStr(uint8_t *str,uint32_t len);
short BSP_UART2_RecvChar(void);
void BSP_UART2_FlushFIFO(void);

extern uint32_t G_uart6_num;		// 暂时用这种方案
void BSP_UART6_Init(void);
void BSP_UART6_SendStr(uint8_t *str,uint32_t len);
short BSP_UART6_RecvChar(void);
void BSP_UART6_FlushFIFO(void);


//=======================================================================
// TIMER
//=======================================================================
// Tick
extern uint32_t G_batt_scan_time;
extern uint32_t G_subg_pair_time;
extern uint32_t G_subg_out_time;

// Timer
#define	SYS_TIMER1		1
#define	SYS_TIMER2		2
#define	SYS_TIMER3		3

#define TEN_SECOND			10000
#define NINE_SECOND			9000
#define EIGHT_SECOND		8000
#define SEVEN_SECOND		7000
#define SIX_SECOND			6000
#define FIVE_SECOND			5000
#define FOUR_SECOND			4000
#define THREE_SECOND		3000
#define TWO_SECOND			2000
#define ONE_SECOND			1000
#define HALF_SECOND			500
#define	QUARTER_SECOND		250

void BSP_Timer_Start(uint8_t t_no,uint32_t t_cnt);
uint32_t BSP_Timer_Read(uint8_t t_no);
void BSP_Cnt_Start(void);
uint32_t BSP_Cnt_Read(void);



//=======================================================================
// KEY
//=======================================================================
#define USER_BUTTON1 		1
#define USER_BUTTON2 		2

#define KEY_DELAY_TICK		30

void BSP_KEY_Init(void);
uint8_t BSP_KEY_Read(void);


//=======================================================================
// SPI
//=======================================================================
void SPI3_Init(void);
void SPI3_SetSpeed(uint8_t SPI_BaudRatePrescaler);
uint8_t SPI3_ReadWriteByte(uint8_t TxData);


//=======================================================================
// CHG
//=======================================================================
#define BATT_INTERVAL_TICK		1000

extern int8_t g_CHG_STAT;
extern const uint8_t g_CHG_Toast[3][20];


void BSP_CHG_Init(void);
void BSP_CHG_Check(void);


#endif


