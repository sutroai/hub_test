#include "api.h"

UART_HandleTypeDef  UART1_Handler;
volatile static uint8_t G_uart1_buf[UART_BUF_LEN];
uint32_t G_uart1_num = 0;
volatile static uint32_t G_uart1_pos = 0;

UART_HandleTypeDef  UART2_Handler;
volatile static uint8_t G_uart2_buf[UART_BUF_LEN];
uint32_t G_uart2_num = 0;
volatile static uint32_t G_uart2_pos = 0;

UART_HandleTypeDef  UART6_Handler;
volatile static uint8_t G_uart6_buf[UART_BUF_LEN];
uint32_t G_uart6_num = 0;
volatile static uint32_t G_uart6_pos = 0;

int fputc(int ch, FILE *f)
{
	HAL_UART_Transmit(&UART1_Handler, (uint8_t *)&ch, 1, HAL_MAX_DELAY); 
	while(__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_TC)!=SET);
	return ch;
}

void USART1_IRQHandler(void)                	
{
	uint8_t Res;
	
	if((__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_RXNE)!=RESET))
	{
		HAL_UART_Receive(&UART1_Handler,&Res,1,1000);
		G_uart1_buf[G_uart1_pos] = Res;
		
		G_uart1_num++;
		if(G_uart1_num > UART_BUF_LEN)
		{
			G_uart1_num = UART_BUF_LEN;							
		}
		
		G_uart1_pos++;
		if(G_uart1_pos == UART_BUF_LEN)
		{
			G_uart1_pos = 0;
		}
	}
	HAL_UART_IRQHandler(&UART1_Handler);
} 

void BSP_UART1_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	//Enable GPIO and USART clock
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_USART1_CLK_ENABLE();
	
	//UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = GPIO_PIN_6 | GPIO_PIN_7;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	UART1_Handler.Instance        = USART1;
	UART1_Handler.Init.BaudRate   = 115200;
	UART1_Handler.Init.WordLength = UART_WORDLENGTH_8B;
	UART1_Handler.Init.StopBits   = UART_STOPBITS_1;
	UART1_Handler.Init.Parity     = UART_PARITY_NONE;
	UART1_Handler.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UART1_Handler.Init.Mode       = UART_MODE_TX_RX;
	HAL_UART_Init(&UART1_Handler);
	
	__HAL_UART_ENABLE_IT(&UART1_Handler, UART_IT_RXNE);
	
	HAL_NVIC_EnableIRQ(USART1_IRQn);
	HAL_NVIC_SetPriority(USART1_IRQn,3,0);
}

//void BSP_UART1_SendChar(uint8_t ch)
//{
//	HAL_UART_Transmit(&UART1_Handler, &ch, 1, HAL_MAX_DELAY);
//	while(__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_TC)!=SET);
//}

void BSP_UART1_SendStr(uint8_t *str,uint32_t len)
{
	HAL_UART_Transmit(&UART1_Handler, str, len, HAL_MAX_DELAY);
	while(__HAL_UART_GET_FLAG(&UART1_Handler,UART_FLAG_TC)!=SET);
}

short BSP_UART1_RecvChar(void)
{
	uint8_t ch;
	
	if(G_uart1_num>0)
	{	
		__set_PRIMASK(1);
		// roll buffer
		ch = G_uart1_buf[ (G_uart1_pos+UART_BUF_LEN-G_uart1_num)%UART_BUF_LEN ];
		G_uart1_num--;
		if(G_uart1_num==0)
		{
			G_uart1_pos = 0;
		}
		__set_PRIMASK(0);
		return ch;
	}
	return -1;	
}

void BSP_UART1_FlushFIFO(void)
{
	__set_PRIMASK(1);
	G_uart1_num = 0;
	G_uart1_pos = 0;	
	__set_PRIMASK(0);
}

void USART2_IRQHandler(void)                	
{
	uint8_t Res;
	
	if((__HAL_UART_GET_FLAG(&UART2_Handler,UART_FLAG_RXNE)!=RESET))
	{
		HAL_UART_Receive(&UART2_Handler,&Res,1,1000);
		G_uart2_buf[G_uart2_pos] = Res;
		
		G_uart2_num++;
		if(G_uart2_num > UART_BUF_LEN)
		{
			G_uart2_num = UART_BUF_LEN;							
		}
		
		G_uart2_pos++;
		if(G_uart2_pos == UART_BUF_LEN)
		{
			G_uart2_pos = 0;
		}
	}
	HAL_UART_IRQHandler(&UART2_Handler);
} 

void BSP_UART2_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	//Enable GPIO and USART clock
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_USART2_CLK_ENABLE();
	
	//UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = GPIO_PIN_2 | GPIO_PIN_3;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	UART2_Handler.Instance        = USART2;
	UART2_Handler.Init.BaudRate   = 115200;
	UART2_Handler.Init.WordLength = UART_WORDLENGTH_8B;
	UART2_Handler.Init.StopBits   = UART_STOPBITS_1;
	UART2_Handler.Init.Parity     = UART_PARITY_NONE;
	UART2_Handler.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UART2_Handler.Init.Mode       = UART_MODE_TX_RX;
	HAL_UART_Init(&UART2_Handler);
	
	__HAL_UART_ENABLE_IT(&UART2_Handler, UART_IT_RXNE);
	
	HAL_NVIC_EnableIRQ(USART2_IRQn);
	HAL_NVIC_SetPriority(USART2_IRQn,4,0);
}

void BSP_UART2_SendStr(uint8_t *str,uint32_t len)
{
	HAL_UART_Transmit(&UART2_Handler, str, len, HAL_MAX_DELAY);
	while(__HAL_UART_GET_FLAG(&UART2_Handler,UART_FLAG_TC)!=SET);
}

short BSP_UART2_RecvChar(void)
{
	uint8_t ch;
	
	if(G_uart2_num>0)
	{	
		__set_PRIMASK(1);
		// roll buffer
		ch = G_uart2_buf[ (G_uart2_pos+UART_BUF_LEN-G_uart2_num)%UART_BUF_LEN ];
		G_uart2_num--;
		if(G_uart2_num==0)
		{
			G_uart2_pos = 0;
		}
		__set_PRIMASK(0);
		return ch;
	}
	return -1;	
}

void BSP_UART2_FlushFIFO(void)
{
	__set_PRIMASK(1);
	G_uart2_num = 0;
	G_uart2_pos = 0;	
	__set_PRIMASK(0);
}

void USART6_IRQHandler(void)                	
{
	uint8_t Res;
	
	if((__HAL_UART_GET_FLAG(&UART6_Handler,UART_FLAG_RXNE)!=RESET))
	{
		HAL_UART_Receive(&UART6_Handler,&Res,1,1000);
		G_uart6_buf[G_uart6_pos] = Res;
		
		G_uart6_num++;
		if(G_uart6_num > UART_BUF_LEN)
		{
			G_uart6_num = UART_BUF_LEN;							
		}
		
		G_uart6_pos++;
		if(G_uart6_pos == UART_BUF_LEN)
		{
			G_uart6_pos = 0;
		}
	}
	HAL_UART_IRQHandler(&UART6_Handler);
} 

void BSP_UART6_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStruct;
	
	//Enable GPIO and USART clock
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_USART6_CLK_ENABLE();
	
	//UART TX GPIO pin configuration
	GPIO_InitStruct.Pin       = GPIO_PIN_11 | GPIO_PIN_12;
	GPIO_InitStruct.Mode      = GPIO_MODE_AF_PP;
	GPIO_InitStruct.Pull      = GPIO_PULLUP;
	GPIO_InitStruct.Speed     = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Alternate = GPIO_AF8_USART6;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	
	UART6_Handler.Instance        = USART6;
	UART6_Handler.Init.BaudRate   = 115200;
	UART6_Handler.Init.WordLength = UART_WORDLENGTH_8B;
	UART6_Handler.Init.StopBits   = UART_STOPBITS_1;
	UART6_Handler.Init.Parity     = UART_PARITY_NONE;
	UART6_Handler.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
	UART6_Handler.Init.Mode       = UART_MODE_TX_RX;
	HAL_UART_Init(&UART6_Handler);
	
	__HAL_UART_ENABLE_IT(&UART6_Handler, UART_IT_RXNE);
	
	HAL_NVIC_EnableIRQ(USART6_IRQn);
	HAL_NVIC_SetPriority(USART6_IRQn,5,0);
}

void BSP_UART6_SendStr(uint8_t *str,uint32_t len)
{
	HAL_UART_Transmit(&UART6_Handler, str, len, HAL_MAX_DELAY);
	while(__HAL_UART_GET_FLAG(&UART6_Handler,UART_FLAG_TC)!=SET);
}

short BSP_UART6_RecvChar(void)
{
	uint8_t ch;
	
	if(G_uart6_num>0)
	{	
		__set_PRIMASK(1);
		// roll buffer
		ch = G_uart6_buf[ (G_uart6_pos+UART_BUF_LEN-G_uart6_num)%UART_BUF_LEN ];
		G_uart6_num--;
		if(G_uart6_num==0)
		{
			G_uart6_pos = 0;
		}
		__set_PRIMASK(0);
		return ch;
	}
	return -1;	
}

void BSP_UART6_FlushFIFO(void)
{
	__set_PRIMASK(1);
	G_uart6_num = 0;
	G_uart6_pos = 0;
	__set_PRIMASK(0);
}


