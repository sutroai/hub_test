#include "api.h"

void BSP_LED_Init(void)
{
	GPIO_InitTypeDef   GPIO_InitStruct;
	
	__HAL_RCC_GPIOB_CLK_ENABLE();
	
	GPIO_InitStruct.Pin   = GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
	GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull  = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_SET);
}

void BSP_LED_ON(uint32_t led)
{
	BSP_LED_OFF(BSP_LED_ID_ALL);			// �ȹر�����LED
    switch(led)
    {
		case BSP_LED_ID_ALL:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_RESET);
			break;
		case BSP_LED_ID_RED:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);
			break;
		case BSP_LED_ID_GREEN:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_RESET);
			break;
		case BSP_LED_ID_BLUE:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);
			break;
		default:
			break;
	}
}

void BSP_LED_OFF(uint32_t led)
{
	switch(led)
	{
		case BSP_LED_ID_ALL:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15, GPIO_PIN_SET);
			break;
		case BSP_LED_ID_RED:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);
			break;
		case BSP_LED_ID_GREEN:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);
			break;
		case BSP_LED_ID_BLUE:
			HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_SET);
			break;
		default:
			break;
	}
}

void BSP_LED_Toggle(uint32_t led)
{
	switch(led)
	{
		case BSP_LED_ID_ALL:
			HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15);
			break;
		case BSP_LED_ID_RED:
			HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_13);
			break;
		case BSP_LED_ID_GREEN:
			HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_14);
			break;
		case BSP_LED_ID_BLUE:
			HAL_GPIO_TogglePin(GPIOB, GPIO_PIN_15);
			break;
		default:
			break;
	}
}




