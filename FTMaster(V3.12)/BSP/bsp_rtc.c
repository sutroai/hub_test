#include "api.h"

const uint8_t table_week[12]={0,3,3,6,1,4,6,2,5,0,3,5};
RTC_HandleTypeDef  RtcHandle;

void BSP_RTC_Init(void)
{
	RCC_OscInitTypeDef        RCC_OscInitStruct;
	RCC_PeriphCLKInitTypeDef  PeriphClkInitStruct;
	
	//Enables the PWR Clock and Enables access to the backup domain
	__HAL_RCC_PWR_CLK_ENABLE();
	HAL_PWR_EnableBkUpAccess();

	RCC_OscInitStruct.OscillatorType =  RCC_OSCILLATORTYPE_LSI | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.LSIState = RCC_LSI_OFF;
	HAL_RCC_OscConfig(&RCC_OscInitStruct);
  
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct);
	
	//Enable RTC Clock
	__HAL_RCC_RTC_ENABLE(); 
  
	RtcHandle.Instance = RTC;
	RtcHandle.Init.HourFormat     = RTC_HOURFORMAT_24;
	RtcHandle.Init.AsynchPrediv   = 0x7F;
	RtcHandle.Init.SynchPrediv    = 0x00FF;
	RtcHandle.Init.OutPut         = RTC_OUTPUT_DISABLE;
	RtcHandle.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
	RtcHandle.Init.OutPutType     = RTC_OUTPUT_TYPE_OPENDRAIN;
	HAL_RTC_Init(&RtcHandle);
}

HAL_StatusTypeDef  BSP_RTC_SetDate(uint8_t year, uint8_t month, uint8_t date)
{
	HAL_StatusTypeDef  BspStatus;
	RTC_DateTypeDef sdatestructure;
	
	sdatestructure.Year = year;
	sdatestructure.Month = month;
	sdatestructure.Date = date;
	sdatestructure.WeekDay = BSP_RTC_GetWeek(year,month,date);
	BspStatus = HAL_RTC_SetDate(&RtcHandle, &sdatestructure, RTC_FORMAT_BIN);
	if(BspStatus != HAL_OK) {
		return BspStatus;
	}
	return HAL_OK;
}

HAL_StatusTypeDef  BSP_RTC_SetTime(uint8_t hour, uint8_t min, uint8_t sec)
{
	HAL_StatusTypeDef  BspStatus;
	RTC_TimeTypeDef stimestructure;
	
	stimestructure.Hours   = hour;
	stimestructure.Minutes = min;
	stimestructure.Seconds = sec;
	stimestructure.TimeFormat = RTC_HOURFORMAT12_AM;
	stimestructure.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
	stimestructure.StoreOperation = RTC_STOREOPERATION_RESET;

	BspStatus = HAL_RTC_SetTime(&RtcHandle, &stimestructure, RTC_FORMAT_BIN);
	if(BspStatus != HAL_OK) {
		return BspStatus;
	}
	return HAL_OK;
}

void BSP_RTC_GetDate(RTC_DateTypeDef *sdate)
{
	HAL_RTC_GetDate(&RtcHandle, sdate, RTC_FORMAT_BIN);
}

void BSP_RTC_GetTime(RTC_TimeTypeDef *stime)
{
	HAL_RTC_GetTime(&RtcHandle, stime, RTC_FORMAT_BIN);
}

uint8_t BSP_RTC_GetWeek(uint8_t year,uint8_t month,uint8_t date)
{	
	uint16_t temp;
	uint8_t yearH,yearL;
	uint16_t nian;
	
	nian = year + 2000;
	yearH = nian/100;	yearL = nian%100;
	if (yearH>19)yearL+=100;
	temp=yearL+yearL/4;
	temp=temp%7; 
	temp=temp+date+table_week[month-1];
	if (yearL%4==0&&month<3)temp--;
	return(temp%7);
}

