#include "api.h"

extern uint8_t g_subg_pair;
extern void Contiki_SysTick_Handler(void);

uint32_t G_timer_time1 = 0;						// 定时器1 
uint32_t G_timer_time2 = 0;						// 定时器2
uint32_t G_timer_time3 = 0;
uint32_t G_sys_cnt = 0;                         // 系统计数器


uint32_t G_batt_scan_time = 0;					// 电池扫描时间
uint32_t G_subg_pair_time = 0;					// subG配对时间
uint32_t G_subg_out_time = 0;					// subG接收超时时间
uint32_t G_sys_tick = 0;

void SysTick_Handler(void)
{
	HAL_IncTick();
	Contiki_SysTick_Handler();
	
	G_sys_tick = HAL_GetTick();
	if(G_batt_scan_time == G_sys_tick)
	{
		G_batt_scan_time += BATT_INTERVAL_TICK;
		BSP_CHG_Check();
	}
	else if(G_subg_pair_time == G_sys_tick)
	{
		g_subg_pair = 0;
		BSP_LED_OFF(BSP_LED_ID_BLUE);
	}
	else if(G_subg_out_time == G_sys_tick)
	{
		
	}
	
}

//======================================================================
// 函数：Sys_Timer_Start
// 功能：启动定时器
//	t_no	定时器编号
//			SYS_TIMER1	定时器1
//			SYS_TIMER2	定时器2
//	t_cnt	定时时间,单位(1ms)
// 返回值：无
//======================================================================
void BSP_Timer_Start(uint8_t t_no,uint32_t t_cnt)
{
	uint32_t tickstart = HAL_GetTick();
	
	if(t_no == SYS_TIMER1)
	{
		G_timer_time1 = tickstart + t_cnt;
	}
	else if(t_no == SYS_TIMER2)
	{
		G_timer_time2 = tickstart + t_cnt;
	}
	else if(t_no == SYS_TIMER3)
	{
		G_timer_time3 = tickstart + t_cnt;
	}
}

//======================================================================
// 函数：Sys_Timer_Read
// 功能：读取剩余计数值
//	t_no	定时器编号
//			SYS_TIMER1	定时器1
//			SYS_TIMER2	定时器2
// 返回值：剩余计数值
//======================================================================
uint32_t BSP_Timer_Read(uint8_t t_no)
{
	uint32_t ticknow = HAL_GetTick();
	
	if(t_no == SYS_TIMER1)
	{
		if(G_timer_time1 > ticknow)
		{
			return (G_timer_time1 - ticknow);
		}
	}
	else if(t_no == SYS_TIMER2)
	{
		if(G_timer_time2 > ticknow)
		{
			return (G_timer_time2 - ticknow);
		}
	}
	else if(t_no == SYS_TIMER3)
	{
		if(G_timer_time3 > ticknow)
		{
			return (G_timer_time3 - ticknow);
		}
	}	
	
	return 0;
}

//======================================================================
// 函数：BSP_Cnt_Start
// 功能：启动计数器
//======================================================================
void BSP_Cnt_Start(void)
{
	uint32_t G_sys_cnt = HAL_GetTick();
}

//======================================================================
// 函数：BSP_Cnt_Read
// 功能：读取计数值
// 返回值：计数值
//======================================================================
uint32_t BSP_Cnt_Read(void)
{
    
    return HAL_GetTick() - G_sys_cnt;
}



