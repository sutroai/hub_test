/**
******************************************************************************
* @file    radio-config.c
* @author  System LAB
* @version V1.0.0
* @date    17-June-2015
* @brief   Source file for SPIRIT1 and S2LP radio configuration
******************************************************************************
* @attention
*
* <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
*
* Redistribution and use in source and binary forms, with or without modification,
* are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.
*   3. Neither the name of STMicroelectronics nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
* FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
* DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
* CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
* OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
******************************************************************************
*/
/*---------------------------------------------------------------------------*/
#include "radio-config.h"
#include "radio-arch.h"
#include "contiki.h"
#include "net/mac/frame802154.h"
#include "net/netstack.h"
#include "net/packetbuf.h"
#include "net/rime/rimestats.h"
#include <stdio.h>
#include "st-lib.h"
#include "api.h"

/**
 * @defgroup ST_Radio
 * @ingroup STM32_Contiki_Library
 * @{
 */

/**
 * @addtogroup ST_Radio
 * @ingroup STM32_Contiki_Library
 * @{
 * @file Porting of the ST_Radio for the Contiki OS
 */

#ifdef USE_STM32L1XX_NUCLEO
//#include "stm32l1xx.h"
#include "stm32f4xx.h"
#include "hw-config.h" 
#endif

#ifdef USE_STM32F4XX_NUCLEO
#include "stm32f4xx.h"
#endif

#if defined(X_NUCLEO_S2868A1) || defined(X_NUCLEO_S2915A1) 
#define S2LP_RADIO
#elif defined(X_NUCLEO_IDS01A4) || defined(X_NUCLEO_IDS01A5)
#define SPIRIT1_RADIO
#endif

#if defined DATA_PACKET_127_BYTES && defined S2LP_RADIO 
#error "DATA_PACKET_127_BYTES makes sense for SPIRIT1 only." 
#endif /*DATA_PACKET_127_BYTES && S2LP_RADIO */
/*---------------------------------------------------------------------------*/
#define XXX_ACK_WORKAROUND 0
/*---------------------------------------------------------------------------*/
#ifndef DEBUG
#define DEBUG 0
#endif
#if DEBUG
#include <stdio.h>
#define PRINTF(...) printf(__VA_ARGS__)
#else
#define PRINTF(...)
#endif

/*---------------------------------------------------------------------------*/
#define BUSYWAIT_UNTIL(cond, max_time)                                  \
  do {                                                                  \
    rtimer_clock_t t0;                                                  \
    t0 = RTIMER_NOW();                                                  \
    while(!(cond) && RTIMER_CLOCK_LT(RTIMER_NOW(), t0 + (max_time)));   \
  } while(0)

/*---------------------------------------------------------------------------*/
#define CLEAR_TXBUF()           (radio_txbuf[0] = 0)
#define CLEAR_RXBUF()           (radio_rxbuf[0] = 0)
#define IS_TXBUF_EMPTY()        (radio_txbuf[0] == 0)
#define IS_RXBUF_EMPTY()        (radio_rxbuf[0] == 0)
#define IS_RXBUF_FULL()         (radio_rxbuf[0] != 0)
/*---------------------------------------------------------------------------*/
/* transceiver state. */
#define ON     0
#define OFF    1
/*---------------------------------------------------------------------------*/
static volatile unsigned int radio_on = OFF;
static volatile uint8_t receiving_packet = 0;
static packetbuf_attr_t last_rssi = 0 ;
//static packetbuf_attr_t last_lqi = 0 ;


/**
* @brief GPIO structure fitting
*/
SGpioInit xGpioIRQ={
  Radio_GPIO_IRQ,
  Radio_GPIO_MODE_DIGITAL_OUTPUT_LP,
  Radio_GPIO_DIG_OUT_IRQ
};

/**
* @brief Radio structure fitting
*/
SRadioInit xRadioInit = {
#ifdef SPIRIT1_RADIO   
  XTAL_OFFSET_PPM,
#endif  
  BASE_FREQUENCY,
#ifdef SPIRIT1_RADIO  
  CHANNEL_SPACE,
  CHANNEL_NUMBER,
#endif  
  MODULATION_SELECT,
  DATARATE,
  FREQ_DEVIATION,
  BANDWIDTH
 };



/**
* @brief Packet Basic structure fitting
*/
#ifdef S2LP_RADIO
PktBasicInit xBasicInit={
  PREAMBLE_LENGTH,
  SYNC_LENGTH,
  SYNC_WORD,
  VARIABLE_LENGTH,
  EXTENDED_LENGTH_FIELD,
  CRC_MODE,
  EN_ADDRESS,
  EN_FEC,
  EN_WHITENING
};
#else //SPIRIT1_RADIO

PktBasicInit xBasicInit={
  PREAMBLE_LENGTH,
  SYNC_LENGTH,
  SYNC_WORD,
  LENGTH_TYPE,
  LENGTH_WIDTH,
#ifdef DATA_PACKET_127_BYTES  
  PKT_NO_CRC,
#else
  CRC_MODE,
#endif  
  CONTROL_LENGTH,
  EN_ADDRESS,
  EN_FEC,
  EN_WHITENING
};
#endif

/**
* @brief Address structure fitting
*/
PktBasicAddressesInit xAddressInit={
  EN_FILT_MY_ADDRESS,
  MY_ADDRESS,
  EN_FILT_MULTICAST_ADDRESS,
  MULTICAST_ADDRESS,
  EN_FILT_BROADCAST_ADDRESS,
  BROADCAST_ADDRESS
};

/*Flags declarations*/
volatile FlagStatus rx_timeout=RESET;

uint8_t cThreholdRxFifoAF = 78; 
uint8_t cThreholdTxFifoAE = 24;
uint16_t nTxIndex, nResidualPcktLength, nPayloadLength=60;
uint8_t *radio_128_data_buffp;
uint16_t nRxIndex = 0;
uint8_t cRxDataLen = 0;
volatile FlagStatus xRxDoneFlag = RESET, xTxDoneFlag=RESET;


#ifdef CSMA_ENABLE
#define PERSISTENT_MODE_EN              S_DISABLE
#define CS_PERIOD                       CSMA_PERIOD_64TBIT
#define CS_TIMEOUT                      3
#define MAX_NB                          5
#define BU_COUNTER_SEED                 0xFA21
#define CU_PRESCALER                    32

// refer to radio config.h for RSSI Thresholds

  /* Radio CSMA config */
#ifdef S2LP_RADIO //MGR300
SCsmaInit
#else /*!S2LP_RADIO*/
CsmaInit
#endif /*S2LP_RADIO*/
 xCsmaInit={ //MGR300
  PERSISTENT_MODE_EN,
  CS_PERIOD,
  CS_TIMEOUT,
  MAX_NB,
  BU_COUNTER_SEED,
  CU_PRESCALER
};
#ifdef S2LP_RADIO
SRssiInit xSRssiInit = {
  .cRssiFlt = 14,
  .xRssiMode = RSSI_STATIC_MODE,
  .cRssiThreshdBm = RSSI_TX_THRESHOLD
};
#endif /*S2LP_RADIO*/

#endif /*CSMA_ENABLE*/
/*---------------------------------------------------------------------------*/
/* 
* The buffers which hold incoming data.
* The +1 because of the first byte, 
* which will contain the length of the packet.
*/
#ifdef S2LP_RADIO
static uint8_t radio_rxbuf[512];
static uint8_t radio_txbuf[512];

#else //SPIRIT1_RADIO
static uint8_t radio_rxbuf[MAX_PACKET_LEN+1];
static uint8_t radio_txbuf[MAX_PACKET_LEN+1-RADIO_MAX_FIFO_LEN];
#endif
/*---------------------------------------------------------------------------*/
#if XXX_ACK_WORKAROUND || NULLRDC_CONF_802154_AUTOACK
static int just_got_an_ack = 0; /* Interrupt callback just detected an ack */
#endif /* XXX_ACK_WORKAROUND */

#if NULLRDC_CONF_802154_AUTOACK
#define ACK_LEN 3
static int wants_an_ack = 0; /* The packet sent expects an ack */
//#define ACKPRINTF printf
#define ACKPRINTF(...)
#endif /* NULLRDC_CONF_802154_AUTOACK */
/*---------------------------------------------------------------------------*/
static int packet_is_prepared = 0;
/*---------------------------------------------------------------------------*/
PROCESS(subGHz_radio_process, "SPIRIT radio driver");
/*---------------------------------------------------------------------------*/
static int Radio_init(void);
static int Radio_prepare(const void *payload, unsigned short payload_len);
static int Radio_transmit(unsigned short payload_len);
static int Radio_send(const void *data, unsigned short len);
static int Radio_read(void *buf, unsigned short bufsize);
static int Radio_channel_clear(void);
static int Radio_receiving_packet(void);
static int Radio_pending_packet(void);
static int Radio_on(void);
static int Radio_off(void);
/*---------------------------------------------------------------------------*/
const struct radio_driver subGHz_radio_driver =
{
  Radio_init,
  Radio_prepare,
  Radio_transmit,
  Radio_send,
  Radio_read,
  Radio_channel_clear,
  Radio_receiving_packet,
  Radio_pending_packet,
  Radio_on,
  Radio_off,  
  NULL,
  NULL,
  NULL,
  NULL
};
/*---------------------------------------------------------------------------*/
/**
  * @brief  radio_printstatus
  * 	prints to the UART the status of the radio
  * @param  none
  * @retval None
  */
void
radio_printstatus(void)
{
  int s = RADIO_STATUS();
  if(s == RADIO_STATE_STANDBY) 
  {
    printf("radio-config: RADIO_STATE_STANDBY\n");
  } 
  else if(s == RADIO_STATE_READY) 
  {
    printf("radio-config: RADIO_STATE_READY\n");
  }
  else if(s == RADIO_STATE_TX) 
  {
    printf("radio-config: RADIO_STATE_TX\n");
  } 
  else if(s == RADIO_STATE_RX)
  {
    printf("radio-config: RADIO_STATE_RX\n");
  } 
  else 
  {
    printf("radio-config: status: %d\n", s);
  }
}
/*---------------------------------------------------------------------------*/
/* Strobe a command. The rationale for this is to clean up the messy legacy code. */
/**
  * @brief  radio_strobe
  * 	strobe a command to the Spirit1 radio
  * @param  uint8_t s
  * @retval None
  */
static void
radio_strobe(uint8_t s)
{
  st_lib_radio_cmd_strobe_command((st_lib_radio_Cmd)s);
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  radio_set_ready_state
  * 	sets the state of the Spirit1 radio to READY
  * @param  none
  * @retval None
  */
void radio_set_ready_state(void)
{
  PRINTF("READY IN\n");

  st_lib_radio_irq_clear_status();
  RADIO_IRQ_DISABLE();

  if(RADIO_STATUS() == RADIO_STATE_STANDBY) 
  {
    RadioCmdStrobeReady();
  }

  else if(RADIO_STATUS() == RADIO_STATE_RX) 
  {
    RadioCmdStrobeSabort();          
    st_lib_radio_irq_clear_status();
  }

  RADIO_IRQ_ENABLE();

  PRINTF("READY OUT\n");
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_init
  * 	initialises the Spirit1 radio
  * @param  none
  * @retval int result(0 == success)
  */
static int
Radio_init(void)
{ 

  PRINTF("RADIO INIT IN\n");

  /* Configure radio shut-down (SDN) pin and activate radio */
  st_lib_radio_gpio_init(RADIO_GPIO_SDN, RADIO_MODE_GPIO_OUT);
  st_lib_radio_spi_init();
  
//#ifdef S2LP_RADIO
//   EepromSpiInitialization();
//#endif /*S2LP_RADIO*/
  
  /* Configures the SPIRIT1 library */
  st_lib_radio_radio_set_xtal_frequency(XTAL_FREQUENCY);
  /* wake up to READY state */
  /* weirdly enough, this *should* actually *set* the pin, not clear it! The pins is declared as GPIO_pin13 == 0x2000 */
  /* Radio ON */
  st_lib_radio_enter_shutdown();
  st_lib_radio_exit_shutdown();


  /* wait minimum 1.5 ms to allow SPIRIT1 a proper boot-up sequence */
  BUSYWAIT_UNTIL(0, 3 * RTIMER_SECOND/2000);

  /* Soft reset of core */  
  radio_strobe(RADIO_STROBE_SRES);  

#ifdef SPIRIT1_RADIO
  /* Configures the SPIRIT1 radio part */
  st_lib_radio_radio_init(&xRadioInit);
  st_lib_radio_radio_set_pa_level_dbm(0,POWER_DBM);
  st_lib_radio_radio_set_pa_level_max_index(0);
#endif /*SPIRIT1_RADIO*/

#ifdef S2LP_RADIO
  //S2LPManagementIdentificationRFBoard(); 

  /* if the board has eeprom, we can compensate the offset calling S2LPManagementGetOffset
  (if eeprom is not present this fcn will return 0) */
  //xRadioInit.lFrequencyBase = xRadioInit.lFrequencyBase + S2LPManagementGetOffset();
	xRadioInit.lFrequencyBase = xRadioInit.lFrequencyBase;

  /* if needed this will set the range extender pins */
  //S2LPManagementRangeExtInit();

  /* if needed this will set the EXT_REF bit of the S2-LP */
  //S2LPManagementTcxoInit();  
    
   /* S2LP Radio config */   
  st_lib_radio_radio_init(&xRadioInit);
  
  
#if 1 
    S2LPRadioSetChannel(CHANNEL_NUMBER);
    S2LPRadioSetChannelSpace(CHANNEL_SPACE);

#endif  

  /* S2LP Radio set power */
 // S2LPRadioSetMaxPALevel(S_DISABLE);
//	S2LPRadioSetPALeveldBm(POWER_INDEX,POWER_DBM); 
	
//  if(!S2LPManagementGetRangeExtender())
//  {
//    /* if we haven't an external PA, use the library function */
//    S2LPRadioSetPALeveldBm(POWER_INDEX,POWER_DBM);
//  }
//  else
//  { 
//    /* in case we are using the PA board, the S2LPRadioSetPALeveldBm will be not functioning because
//       the output power is affected by the amplification of this external component.
//        Set the raw register. */
//    uint8_t paLevelValue=0x25; /* for example, this value will give 23dBm about */
//    S2LPSpiWriteRegisters(PA_POWER8_ADDR, 1, &paLevelValue);
//  }

	// ���ó�PA
	{ 
		/* in case we are using the PA board, the S2LPRadioSetPALeveldBm will be not functioning because
		the output power is affected by the amplification of this external component.
		Set the raw register. */
		uint8_t paLevelValue=0x23; /* for example, this value will give 23dBm about */
		S2LPSpiWriteRegisters(PA_POWER8_ADDR, 1, &paLevelValue);
	}

   S2LPRadioSetPALevelMaxIndex(POWER_INDEX); 
    
#endif /*S2LP_RADIO*/
  
  /* Configures the SPIRIT1 packet handler part*/
  st_lib_radio_pkt_basic_init(&xBasicInit);
  
#ifdef CSMA_ENABLE
  st_lib_radio_csma_init(&xCsmaInit);
  st_lib_radio_csma(S_ENABLE);
 // S2LPRadioSetRssiThreshdBm(RSSI_THR);

#ifdef S2LP_RADIO 
  S2LPRadioRssiInit(&xSRssiInit); 
#else //SPIRIT Radio
  SpiritQiSetRssiThresholddBm(RSSI_TX_THRESHOLD); 
#endif /*S2LP_RADIO*/
#endif /*CSMA_ENABLE*/

  /* Enable the following interrupt sources, routed to GPIO */
  st_lib_radio_irq_de_init(NULL);
  st_lib_radio_irq_clear_status();
  st_lib_radio_irq(TX_DATA_SENT, S_ENABLE);
  st_lib_radio_irq(RX_DATA_READY,S_ENABLE);
  st_lib_radio_irq(VALID_SYNC,S_ENABLE);
  st_lib_radio_irq(RX_DATA_DISC, S_ENABLE);

#ifdef DATA_PACKET_127_BYTES
  st_lib_radio_irq(RX_FIFO_ALMOST_FULL,S_ENABLE);
#endif /*DATA_PACKET_127_BYTES*/
  
#ifdef CSMA_ENABLE
  st_lib_radio_irq(MAX_BO_CCA_REACH , S_ENABLE);  
  st_lib_radio_csma(S_DISABLE);
#endif /*CSMA_ENABLE*/
    
  st_lib_radio_irq(TX_FIFO_ERROR, S_ENABLE);
  st_lib_radio_irq(RX_FIFO_ERROR, S_ENABLE);

  /* Configure Radio */
  st_lib_radio_radio_persisten_rx(S_ENABLE);

#ifdef SPIRIT1_RADIO
  st_lib_radio_qi_set_sqi_threshold(SQI_TH_0);
  st_lib_radio_qi_sqi_check(S_ENABLE);
  
  st_lib_radio_timer_set_rx_timeout_stop_condition(SQI_ABOVE_THRESHOLD);
#endif /*SPIRIT1_RADIO*/

#ifndef CSMA_ENABLE
  st_lib_radio_qi_set_rssi_threshold_dbm(RSSI_RX_THRESHOLD);
#endif  /*CSMA_ENABLE*/


  SET_INFINITE_RX_TIMEOUT();

#ifdef SPIRIT1_RADIO
  st_lib_radio_radio_afc_freeze_on_sync(S_ENABLE);
#endif /*SPIRIT1_RADIO*/

#ifdef DATA_PACKET_127_BYTES
    /* .. set the almost full threshold and configure the associated IRQ */
  st_lib_FifoSetAlmostFullThresholdRx(96-cThreholdRxFifoAF);

#endif /*DATA_PACKET_127_BYTES*/

  
  /* Puts the SPIRIT1 in STANDBY mode (125us -> rx/tx) */ 
#ifndef CSMA_ENABLE  
//  radio_strobe(SPIRIT1_STROBE_STANDBY);
#endif
  radio_on = OFF;
  CLEAR_RXBUF();
  CLEAR_TXBUF();
  
  /* Initializes the mcu pin as input, used for IRQ */
  st_lib_radio_gpio_init(RADIO_GPIO_IRQ, RADIO_MODE_EXTI_IN);
  
  /* Configure the radio to route the IRQ signal to its GPIO 3 */
 
  st_lib_radio_gpio_irq_init(&xGpioIRQ);

  /* uC IRQ enable */
  RadioGpioInterruptCmd(RADIO_GPIO_IRQ,3,0,ENABLE);

  process_start(&subGHz_radio_process, NULL);

  PRINTF("Radio init done\n");
  return 0;
}

/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_prepare
  * 	prepares the Spirit1 radio
  * @param  none
  * @retval int result(0 == success)
  */
static int
Radio_prepare(const void *payload, unsigned short payload_len)
{
  PRINTF("Spirit1: prep %u\n", payload_len);
  radio_128_data_buffp =  (uint8_t *) payload; 
  nResidualPcktLength = payload_len;

  packet_is_prepared = 0;

  /* Checks if the payload length is supported */
  if(payload_len > SICSLOWPAN_CONF_MAC_MAX_PAYLOAD) 
  {
    return RADIO_TX_ERR;
  }

  /* Should we delay for an ack? */
#if NULLRDC_CONF_802154_AUTOACK
  frame802154_t info154;
  wants_an_ack = 0;
  if(payload_len > ACK_LEN
      && frame802154_parse((char*)payload, payload_len, &info154) != 0) {
    if(info154.fcf.frame_type == FRAME802154_DATAFRAME
        && info154.fcf.ack_required != 0) {
      wants_an_ack = 1;
    }
  }
#endif /* NULLRDC_CONF_802154_AUTOACK */

  /* Sets the length of the packet to send */
  RADIO_IRQ_DISABLE();
  radio_strobe(RADIO_STROBE_FTX);
  st_lib_radio_pkt_basic_set_payload_length(payload_len);

 if(payload_len > MAX_PACKET_LEN)
  {
    /* ... if yes transmit data using an AE IRQ and a FIFO reloading mechanism */
    /* set the almost empty threshold */
    st_lib_FifoSetAlmostEmptyThresholdTx(cThreholdTxFifoAE);
    st_lib_radio_irq(TX_FIFO_ALMOST_EMPTY , S_ENABLE);
    
    /* write the linear fifo with the first 96 bytes of payload */
    st_lib_radio_spi_write_linear_fifo(MAX_PACKET_LEN, (uint8_t *)payload);
    
    /* store the number of transmitted bytes */
    nTxIndex = MAX_PACKET_LEN;
    
    /* update the residual number of bytes to be transmitted */
    nResidualPcktLength -= MAX_PACKET_LEN;
    
  }

  else
  {
  nTxIndex = payload_len;
  st_lib_radio_spi_write_linear_fifo(payload_len, (uint8_t *)payload);
  }

  RADIO_IRQ_ENABLE();
  
  PRINTF("PREPARE OUT\n");

  packet_is_prepared = 1;
  return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_transmit
  * 	transimts a packet with the Spirit1 radio
  * @param  unsigned short payload_len
  * @retval int result(0 == success)
  */
static int
Radio_transmit(unsigned short payload_len)
{ 
  /* This function blocks until the packet has been transmitted */

  PRINTF("TRANSMIT IN\n");
  if(!packet_is_prepared)
  {
    return RADIO_TX_ERR;
  }

#ifdef CSMA_ENABLE
   xTxDoneFlag = RESET;  
#endif /*CSMA_ENABLE*/

  /* Stores the length of the packet to send */
  /* Others Radio_prepare will be in hold */
  radio_txbuf[0] = payload_len;
  
  /* Puts the SPIRIT1 in TX state */
  receiving_packet = 0;
  radio_set_ready_state(); 

#ifdef CSMA_ENABLE
  st_lib_radio_csma(S_ENABLE);
  st_lib_radio_qi_set_rssi_threshold_dbm(RSSI_TX_THRESHOLD); //MGR300...
#endif  /*CSMA_ENABLE*/
  st_lib_CmdStrobeTx();
    
#if (XXX_ACK_WORKAROUND || NULLRDC_CONF_802154_AUTOACK) 
  just_got_an_ack = 0;
#endif /* XXX_ACK_WORKAROUND */

#ifdef CSMA_ENABLE
    uint8_t cCsState; 
    
    /* wait for TX done */
    while(!xTxDoneFlag)
    {
   //   SdkDelayMs(2);
      
      cCsState = st_lib_QiGetCs();
      
      if(cCsState)
      {
        while(!xTxDoneFlag);
      }
    }
    xTxDoneFlag = RESET;
    
  st_lib_radio_csma(S_DISABLE);
  st_lib_radio_qi_set_rssi_threshold_dbm(RSSI_RX_THRESHOLD); 
#else /*!CSMA_ENABLE*/

	// while(!xTxDoneFlag);
	// modify by Frank.
	BSP_Timer_Start(SYS_TIMER3, 100);
	while((!xTxDoneFlag) && BSP_Timer_Read(SYS_TIMER3));
    xTxDoneFlag = RESET; 
#endif /*CSMA_ENABLE*/

  /* Reset radio - needed for immediate RX of ack */
  if(!IS_TXBUF_EMPTY())
  {
	  CLEAR_TXBUF();
  }

  if(!IS_RXBUF_EMPTY())
  {
	  CLEAR_RXBUF();
  }

  RADIO_IRQ_DISABLE();
  st_lib_radio_irq_clear_status();
  RadioCmdStrobeSabort();

  BUSYWAIT_UNTIL(0, RTIMER_SECOND/2500);
  RadioCmdStrobeReady();
  BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_READY, 1 * RTIMER_SECOND/1000);
  RadioCmdStrobeRx();

  BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_RX, 1 * RTIMER_SECOND/1000);
  RADIO_IRQ_ENABLE();

#if XXX_ACK_WORKAROUND
  just_got_an_ack = 1;
#endif /* XXX_ACK_WORKAROUND */

#if NULLRDC_CONF_802154_AUTOACK

 if (wants_an_ack) 
  {

   BUSYWAIT_UNTIL(just_got_an_ack, 2 * RTIMER_SECOND/1000);
    if(just_got_an_ack) 
    {
    } 
    else 
    {
      ACKPRINTF("debug_ack: no ack received\n");
    }
  }
#endif /* NULLRDC_CONF_802154_AUTOACK */

  PRINTF("TRANSMIT OUT\n");

  if(!IS_TXBUF_EMPTY())
  {
	  CLEAR_TXBUF();
  }

  packet_is_prepared = 0;

  clock_wait(1);

  return RADIO_TX_OK;
}
/*---------------------------------------------------------------------------*/
static int Radio_send(const void *payload, unsigned short payload_len)
{
  if(Radio_prepare(payload, payload_len) == RADIO_TX_ERR) 
  {
    return RADIO_TX_ERR;
  } 
  return Radio_transmit(payload_len);
  
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_read
  * 	reads a packet received with the Spirit1 radio
  * @param  void *buf, unsigned short bufsize
  * @retval int bufsize
  */
static int Radio_read(void *buf, unsigned short bufsize)
{  
  PRINTF("READ IN\n");
  
  /* Checks if the RX buffer is empty */
  if(IS_RXBUF_EMPTY()) 
  {
    RADIO_IRQ_DISABLE();
    CLEAR_RXBUF();
    RadioCmdStrobeSabort();
    BUSYWAIT_UNTIL(0, RTIMER_SECOND/2500);
    RadioCmdStrobeReady();
    BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_READY, 1 * RTIMER_SECOND/1000);
    RadioCmdStrobeRx(); 
    BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_RX, 1 * RTIMER_SECOND/1000);
    PRINTF("READ OUT RX BUF EMPTY\n");
    RADIO_IRQ_ENABLE();
    return 0;
  }

  if(bufsize < radio_rxbuf[0]) 
  { 
    /* If buf has the correct size */
    PRINTF("TOO SMALL BUF\n");
    return 0;
  } 
  else 
  {
    /* Copies the packet received */
    memcpy(buf, radio_rxbuf + 1, radio_rxbuf[0]);

    packetbuf_set_attr(PACKETBUF_ATTR_RSSI, last_rssi);        
    //packetbuf_set_attr(PACKETBUF_ATTR_LINK_QUALITY, last_lqi); 
    bufsize = radio_rxbuf[0];
    CLEAR_RXBUF();
    
    PRINTF("READ OUT\n");
    
    return bufsize;
  }  
  
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_channel_clear
  * 	checks the channel
  * @param  none
  * @retval int result
  */
static int
Radio_channel_clear(void)
{
  float rssi_value;
  /* Local variable used to memorize the SPIRIT1 state */
  uint8_t radio_state = radio_on;//ON;// see below
  
  PRINTF("CHANNEL CLEAR IN\n");
  
  if(radio_on == OFF) 
  {
    /* Wakes up the Radio */
    Radio_on();
    //radio_state = OFF;// check this is different from original contiki
  }
  
  RADIO_IRQ_DISABLE();
  RadioCmdStrobeSabort();
  
  st_lib_radio_irq_clear_status();
  
  RADIO_IRQ_ENABLE();
  
  
  rtimer_clock_t timeout = RTIMER_NOW() + 5 * RTIMER_SECOND/1000;
  do 
  {
    st_lib_radio_refresh_status();
  } while((st_lib_g_x_status.MC_STATE != MC_STATE_READY) && (RTIMER_NOW() < timeout));
  //    if(RTIMER_NOW() < timeout) {
  /* Modified EP */
  if(st_lib_g_x_status.MC_STATE != MC_STATE_READY) 
  {
    return 0;
  }
  
  /* Stores the RSSI value */
  rssi_value = st_lib_radio_qi_get_rssi_dbm();
  //  int ret = st_lib_QiGetCs(); 
  
  /* Puts the SPIRIT1 in its previous state */
  if(radio_state==OFF) 
  {
    Radio_off();
  } 
  else 
  {
    RadioCmdStrobeRx();
    /*    SpiritCmdStrobeRx();*/
    BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_RX, 5 * RTIMER_SECOND/1000);
  }
  //PRINTF("CHANNEL CLEAR OUT (%d)\n", !ret);   
  //  return !ret; 

  if(rssi_value<(float)RSSI_TX_THRESHOLD) 
  {
    return 1;
  } 
  else 
  {
    return 0;
  }  
  
}

/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_receiving_packet
  * 	checks for receiving packet
  * @param  none
  * @retval int result
  */
static int
Radio_receiving_packet(void)
{
  return receiving_packet;
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_receiving_packet
  * 	checks for pending packet
  * @param  none
  * @retval int result
  */
static int
Radio_pending_packet(void)
{
  PRINTF("PENDING PACKET\n");
  return !IS_RXBUF_EMPTY();
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_off
  * 	turns off the Spirit1 radio
  * @param  none
  * @retval int result(0 == success)
  */
static int
Radio_off(void)
{  
  PRINTF("Spirit1: ->off\n");
  if(radio_on == ON) 
  {
    /* Disables the mcu to get IRQ from the SPIRIT1 */
    RADIO_IRQ_DISABLE();

    /* first stop rx/tx */
    
    RadioCmdStrobeSabort();

    /* Clear any pending irqs */
    st_lib_radio_irq_clear_status();
    
    BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_READY, 5 * RTIMER_SECOND/1000);
    if(RADIO_STATUS() != RADIO_STATE_READY) 
    {
      PRINTF("Spirit1: failed off->ready\n");
      return 1;
    }

    /* Puts the SPIRIT1 in STANDBY */
    RadioCmdStrobeSabort();
    BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_STANDBY, 5 * RTIMER_SECOND/1000);
    if(RADIO_STATUS() != RADIO_STATE_STANDBY) 
    {
      PRINTF("Spirit1: failed off->stdby\n");
      return 1;
    }

    radio_on = OFF;
    CLEAR_TXBUF();
    CLEAR_RXBUF(); 
  }
  PRINTF("Spirit1: off.\n");
  return 0; 
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_on
  * 	turns on the Spirit1 radio
  * @param  none
  * @retval int result(0 == success)
  */
static int Radio_on(void)
{

  PRINTF("Spirit1: on\n");
  RadioCmdStrobeSabort();          
 
  BUSYWAIT_UNTIL(0, RTIMER_SECOND/2500);
  if(radio_on == OFF) 
  {
    RADIO_IRQ_DISABLE();
    /* ensure we are in READY state as we go from there to Rx */
    RadioCmdStrobeReady();

    BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_READY, 5 * RTIMER_SECOND/1000);
    if(RADIO_STATUS() != RADIO_STATE_READY) {
      PRINTF("Spirit1: failed to turn on\n");
	  while(1);
      //return 1;
    }
 
    /* now we go to Rx */
   
    RadioCmdStrobeRx();
    BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_RX, 5 * RTIMER_SECOND/1000);
    if(RADIO_STATUS() != RADIO_STATE_RX) {
      PRINTF("Spirit1: failed to enter rx\n");
	  while(1);
      //return 1;
    }
    
    /* Enables the mcu to get IRQ from the SPIRIT1 */
    RADIO_IRQ_ENABLE();
    radio_on = ON;
  }

  return 0;
}
/*---------------------------------------------------------------------------*/
static int interrupt_callback_in_progress = 0;
static int interrupt_callback_wants_poll = 0;

/**
  * @brief  main Spirit1 contiki PROCESS
  */
PROCESS_THREAD(subGHz_radio_process, ev, data)
{
  PROCESS_BEGIN();
  PRINTF("Spirit1: process started\n");
  
  while(1) {
    int len;

    PROCESS_YIELD_UNTIL(ev == PROCESS_EVENT_POLL);    
    PRINTF("Spirit1: polled\n");

    packetbuf_clear();
    len = Radio_read(packetbuf_dataptr(), PACKETBUF_SIZE);

    if(len > 0) {

#if NULLRDC_CONF_802154_AUTOACK
      /* Check if the packet has an ACK request */
      frame802154_t info154;
      if(len > ACK_LEN &&
          frame802154_parse((char*)packetbuf_dataptr(), len, &info154) != 0) {
        if(info154.fcf.frame_type == FRAME802154_DATAFRAME &&
            info154.fcf.ack_required != 0 &&
            linkaddr_cmp((linkaddr_t *)&info154.dest_addr,
                         &linkaddr_node_addr)) {


#if !XXX_ACK_WORKAROUND
          /* Send an ACK packet */
          uint8_t ack_frame[ACK_LEN] = {
              FRAME802154_ACKFRAME,
              0x00,
              info154.seq
          };
          RADIO_IRQ_DISABLE();
          radio_strobe(RADIO_STROBE_FTX);
          st_lib_radio_pkt_basic_set_payload_length((uint16_t) ACK_LEN);
          st_lib_radio_spi_write_linear_fifo((uint16_t) ACK_LEN, (uint8_t *) ack_frame);

          radio_set_ready_state();
          RADIO_IRQ_ENABLE();
          st_lib_CmdStrobeTx();
          BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_TX, 1 * RTIMER_SECOND/1000);
          BUSYWAIT_UNTIL(RADIO_STATUS() != RADIO_STATE_TX, 1 * RTIMER_SECOND/1000);
          ACKPRINTF("debug_ack: sent ack %d\n", ack_frame[2]);

#endif /* !XXX_ACK_WORKAROUND */
        }
      }
#endif /* NULLRDC_CONF_802154_AUTOACK */

      packetbuf_set_datalen(len);   
      NETSTACK_RDC.input();
    }

    if(!IS_RXBUF_EMPTY()) {
      process_poll(&subGHz_radio_process);
    }

    if(interrupt_callback_wants_poll) {
      Radio_interrupt_callback();

      if(RADIO_STATUS() == RADIO_STATE_READY) 
      {
        RadioCmdStrobeRx();
        BUSYWAIT_UNTIL(RADIO_STATUS() == RADIO_STATE_RX, 1 * RTIMER_SECOND/1000);
      }
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/**
  * @brief  Radio_interrupt_callback
  * 	callback when an interrupt is received
  * @param  none
  * @retval none
  */
void
Radio_interrupt_callback(void)
{
#define INTPRINTF(...) // PRINTF
  st_lib_radio_irqs x_irq_status;
  if (radio_spi_busy() || interrupt_callback_in_progress) 
  {
    process_poll(&subGHz_radio_process);
    interrupt_callback_wants_poll = 1;
    return;
  }
  
  interrupt_callback_wants_poll = 0;
  interrupt_callback_in_progress = 1;
  
  /* get interrupt source from radio */
  st_lib_radio_irq_get_status(&x_irq_status);
  st_lib_radio_irq_clear_status();
  
  if(x_irq_status.IRQ_RX_FIFO_ERROR) 
  {
    receiving_packet = 0;
    interrupt_callback_in_progress = 0;
    radio_strobe(RADIO_STROBE_FRX);

#ifdef S2LP_RADIO
     RadioCmdStrobeRx(); 
#endif /*S2LP_RADIO*/
    return;
  }
  
  if(x_irq_status.IRQ_TX_FIFO_ERROR) 
  {
    receiving_packet = 0;
    interrupt_callback_in_progress = 0;
    radio_strobe(RADIO_STROBE_FTX);
    return;
  }
  
  /* The IRQ_VALID_SYNC is used to notify a new packet is coming */
  if(x_irq_status.IRQ_VALID_SYNC) 
  {
    INTPRINTF("SYNC\n");
    receiving_packet = 1;
    RadioCmdStrobeRx();
  }
  
  /* The IRQ_TX_DATA_SENT notifies the packet received. Puts the SPIRIT1 in RX */
  if(x_irq_status.IRQ_TX_DATA_SENT)
  {
    RadioCmdStrobeRx();
    
    
    INTPRINTF("SENT\n");
    CLEAR_TXBUF();
    nTxIndex = 0;
    xTxDoneFlag = SET;
    interrupt_callback_in_progress = 0;     
    return;
  }

#ifdef CSMA_ENABLE  
    if(x_irq_status.IRQ_MAX_BO_CCA_REACH)
    {
      /* Send a Tx command */
      st_lib_CmdStrobeTx();
    }
#endif
#ifdef DATA_PACKET_127_BYTES
    /* Check the SPIRIT TX_FIFO_ALMOST_EMPTY IRQ flag */
    else if(x_irq_status.IRQ_TX_FIFO_ALMOST_EMPTY)
      {
        /* read the number of elements in the Tx FIFO */
        uint8_t cNElemTxFifo = st_lib_FifoReadNumberBytesTxFifo();

        /* check if the sum of the residual payload to be transmitted and the actual bytes in FIFO are higher than 96 */
        if(nResidualPcktLength+cNElemTxFifo > NETSTACK_RADIO_MAX_PAYLOAD_LEN)
        {
          /* .. if yes another almost full IRQ has to be managed */

          /* ..so fill the Tx FIFO */
          st_lib_radio_spi_write_linear_fifo(96-cNElemTxFifo-1, &radio_128_data_buffp[nTxIndex]);

          /* update the number of bytes to be transmitted */
          nResidualPcktLength -= 96-cNElemTxFifo-1;

          /* update the number of bytes transmitted until now */
          nTxIndex += 96-cNElemTxFifo-1;
        }
        else
        {
          /* .. if not all the nResidualPcktLength bytes remaining can be written to the Tx FIFO */
          /* ..so disable the TX_FIFO_ALMOST_EMPTY IRQ */
          st_lib_radio_irq(TX_FIFO_ALMOST_EMPTY , S_DISABLE);

          /* unarm the AE threshold mechanism */
          st_lib_FifoSetAlmostEmptyThresholdTx(96);

          /* fill the Tx fifo */
          st_lib_radio_spi_write_linear_fifo(nResidualPcktLength, &radio_128_data_buffp[nTxIndex]);

          /* update the number of transmitted bytes */
          nTxIndex += nResidualPcktLength;

          /* update the number of bytes to be transmitted */
          nResidualPcktLength = 0;

        }

        /* re-read the number of elements in the Tx FIFO */
        cNElemTxFifo = st_lib_FifoReadNumberBytesTxFifo();

      }
#endif  /*DATA_PACKET_127_BYTES*/
  
  /* The IRQ_RX_DATA_READY notifies a new packet arrived */
  if(x_irq_status.IRQ_RX_DATA_READY) 
  {
  
#ifndef DATA_PACKET_127_BYTES
    st_lib_radio_spi_read_linear_fifo(st_lib_radio_linear_fifo_read_num_elements_rx_fifo(),
                                       &radio_rxbuf[1]);
#else /*!DATA_PACKET_127_BYTES*/
    cRxDataLen = st_lib_radio_linear_fifo_read_num_elements_rx_fifo();

    if(cRxDataLen!=0)
    {
    /* Read the RX FIFO */
    if(nRxIndex == 0)
      {
       st_lib_radio_spi_read_linear_fifo(cRxDataLen, &radio_rxbuf[1]);
      }
    else
      {
      st_lib_radio_spi_read_linear_fifo(cRxDataLen, &radio_rxbuf[nRxIndex]);
      }        

    }

#endif   /*DATA_PACKET_127_BYTES*/                                    
    radio_rxbuf[0] = st_lib_radio_pkt_basic_get_received_pkt_length();
    st_lib_radio_cmd_strobe_flush_rx_fifo();
    
    INTPRINTF("RECEIVED\n");
    
   // process_poll(&subGHz_radio_process);
    
    last_rssi = (packetbuf_attr_t) st_lib_radio_qi_get_rssi(); 
    //last_lqi  = (packetbuf_attr_t) st_lib_radio_qi_get_lqi(); 
    
    receiving_packet = 0;
    
#if NULLRDC_CONF_802154_AUTOACK
    if (radio_rxbuf[0] == ACK_LEN) 
    {
      /* For debugging purposes we assume this is an ack for us */
      just_got_an_ack = 1;
    }
#endif /* NULLRDC_CONF_802154_AUTOACK */
    
    /* update the number of received bytes */
    nRxIndex += cRxDataLen;

    /* set the Rx done flag */
    xRxDoneFlag = SET; 
    
     nRxIndex = 0; 
    interrupt_callback_in_progress = 0;
     process_poll(&subGHz_radio_process);
     RadioCmdStrobeRx(); 
    return;
  }
  
  if(x_irq_status.IRQ_RX_DATA_DISC) 
  {
    /* RX command - to ensure the device will be ready for the next reception */
    if(x_irq_status.IRQ_RX_TIMEOUT) 
    {
      st_lib_radio_cmd_strobe_flush_rx_fifo();
      rx_timeout = SET; 
       RadioCmdStrobeRx(); 
    }
  }
  
#ifdef DATA_PACKET_127_BYTES
  else if(x_irq_status.IRQ_RX_FIFO_ALMOST_FULL)
    {      
      nRxIndex = 0; //issue due to packet loss so reset the values
      cRxDataLen = st_lib_radio_linear_fifo_read_num_elements_rx_fifo();
       
      /* Read the RX FIFO */
      st_lib_radio_spi_read_linear_fifo(cRxDataLen, &radio_rxbuf[1]);     
      radio_rxbuf[0] = cRxDataLen;

      //   receiving_packet = 0; 

      nRxIndex+=cRxDataLen + 1; 
      

  }
#endif /*DATA_PACKET_127_BYTES*/
  interrupt_callback_in_progress = 0;
}
/*---------------------------------------------------------------------------*/
/** @} */
/** @} */
