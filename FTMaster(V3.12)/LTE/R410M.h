#ifndef __R410M_H
#define __R410M_H

#include "stm32f4xx.h"

void R410M_Module_PWRON(void);
void R410M_Module_PWROFF(void);
int R410M_Module_Reset(void);

int R410M_Module_Init(void);
int R410M_GET_CGMM(uint8_t *mid);
int R410M_Get_CPIN(void);

int R410M_Get_CGATT(void);
int R410M_Get_CEREG(void);
int R410M_Get_COPS(uint16_t *mode,uint8_t *oper,uint16_t *act);

int R410M_Create_Socket(uint16_t *soc);
int R410M_Close_Socket(uint16_t soc);
int R410M_Connect_Socket(uint16_t soc,uint8_t *ip,uint16_t port);
int R410M_Set_USODL(uint16_t soc);
int R410M_Exit_USODL(void);
int R410_DL_Send(uint8_t *data,uint16_t len);
int R410_DL_Recv(uint8_t *data,uint16_t reqlen,uint16_t *len,uint16_t time);

#endif

